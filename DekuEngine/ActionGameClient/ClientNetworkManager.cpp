#pragma once
#include "ClientNetworkManager.h"

namespace
{
	ClientNetworkManager* sInstance = nullptr;
}

void ClientNetworkManager::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[ClientNetworkManager] Manager already initialized");
	sInstance = new ClientNetworkManager();
}

void ClientNetworkManager::StaticTerminate()
{
	XASSERT(sInstance != nullptr, "[ClientNetworkManager] Manager already Terminated");
	X::SafeDelete(sInstance);
}

ClientNetworkManager* ClientNetworkManager::Get()
{
	XASSERT(sInstance != nullptr, "[ClientNetworkManager] Manager Not initialized");
	return sInstance;
}


ClientNetworkManager::ClientNetworkManager()
{

}

ClientNetworkManager::~ClientNetworkManager()
{

}

void ClientNetworkManager::Initialize()
{
	Network::Initialize();
}

void ClientNetworkManager::Terminate()
{
	Network::Terminate();
}

bool ClientNetworkManager::ConnectToServer(const char* host, uint32_t port)
{
	return;
		
}

bool ClientNetworkManager::SendMessageToServer(const Network::MemoryStream& memStream)
{

}

bool ClientNetworkManager::HandleMessage()
{
	uint8_t buffer[16384];
	int bytesReceived = mSocket.Receive(buffer, (int)std::size(buffer));
	if (bytesReceived == SOCKET_ERROR)
	{
		return false;
	}

	if (bytesReceived > 0)
	{
		Network::MemoryStream memStream(buffer, bytesReceived);
		Network::StreamReader reader(memStream);

		while (reader.GetRemainingDataSize() > 0)
		{
			uint32_t messageType;
			reader.Read(messageType);
			switch (MessageType(messageType))
			{
			case MessageType::Snapshot:
				GameObjectManager::Get()->Deserialize(reader);
				break;
			case MessageType::Assignment:
				reader.Read(mClientId);
				break;
			case MessageType::Create:
				GameObjectManager::Get()->CreateGameObject(reader);
				break;
			case MessageType::Update:
				GameObjectManager::Get()->Update(reader);
				break;
			default:
				break;
			}
		}
	}
	return true;
}






