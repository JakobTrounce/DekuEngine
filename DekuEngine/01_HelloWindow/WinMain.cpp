#include <Core.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	Deku::Core::Window myWindow;
	myWindow.Initialize(hInstance, "Hello Window",1920, 1080);

	bool done = false;
	while (!done)
	{
		done = myWindow.ProcessMessage();
	}

	
	
	myWindow.Terminate();
	return 0;
}