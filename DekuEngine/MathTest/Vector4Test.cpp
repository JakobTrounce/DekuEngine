#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Deku::Math;


namespace MathTest
{
	// macro is doing code injection for you
	TEST_CLASS(Vector4Test)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			Vector4 v0;
			Assert::AreEqual(v0.x, 0.0f);
			Assert::AreEqual(v0.y, 0.0f);
			Assert::AreEqual(v0.z, 0.0f);
			Assert::AreEqual(v0.w, 0.0f);

			Vector4 v1(1.0f);
			Assert::AreEqual(v1.x, 1.0f);
			Assert::AreEqual(v1.y, 1.0f);
			Assert::AreEqual(v1.z, 1.0f);
			Assert::AreEqual(v1.w, 1.0f);

			Vector4 v2(1.0f, 2.0f, 3.0f, 4.0f);
			Assert::AreEqual(v2.x, 1.0f);
			Assert::AreEqual(v2.y, 2.0f);
			Assert::AreEqual(v2.z, 3.0f);
			Assert::AreEqual(v2.w, 4.0f);
		}

		TEST_METHOD(TestAddition)
		{
			Vector4 v0(1.0f);
			Vector4 v1(2.0f, 3.0f, 4.0f, 5.0f);
			Vector4 v2 = v0 + v1;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);
			Assert::AreEqual(v0.z, 1.0f);
			Assert::AreEqual(v0.w, 1.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
			Assert::AreEqual(v1.z, 4.0f);
			Assert::AreEqual(v1.w, 5.0f);

			Assert::AreEqual(v2.x, 3.0f);
			Assert::AreEqual(v2.y, 4.0f);
			Assert::AreEqual(v2.z, 5.0f);
			Assert::AreEqual(v2.w, 6.0f);

		}

		TEST_METHOD(TestSubtraction)
		{
			Vector4 v0(1.0f);
			Vector4 v1(2.0f, 3.0f, 4.0f,5.0f);
			Vector4 v2 = v0 - v1;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);
			Assert::AreEqual(v0.z, 1.0f);
			Assert::AreEqual(v0.w, 1.0f);


			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
			Assert::AreEqual(v1.z, 4.0f);
			Assert::AreEqual(v1.w, 5.0f);

			Assert::AreEqual(v2.x, -1.0f);
			Assert::AreEqual(v2.y, -2.0f);
			Assert::AreEqual(v2.z, -3.0f);
			Assert::AreEqual(v2.w, -4.0f);
		}

		TEST_METHOD(TestDivision)
		{
			Vector4 v0(2.0f, 4.0f, 6.0f, 8.0f);
			Vector4 v2 = v0 / 2.0f;

			Assert::AreEqual(v0.x, 2.0f);
			Assert::AreEqual(v0.y, 4.0f);
			Assert::AreEqual(v0.z, 6.0f);
			Assert::AreEqual(v0.w, 8.0f);

			Assert::AreEqual(v2.x, 1.0f);
			Assert::AreEqual(v2.y, 2.0f);
			Assert::AreEqual(v2.z, 3.0f);
			Assert::AreEqual(v2.w, 4.0f);
		}

		TEST_METHOD(TestMultiplication)
		{
			Vector4 v0(2.0f, 4.0f, 6.0f,8.0f);
			Vector4 v2 = v0 * 2.0f;

			Assert::AreEqual(v0.x, 2.0f);
			Assert::AreEqual(v0.y, 4.0f);
			Assert::AreEqual(v0.z, 6.0f);
			Assert::AreEqual(v0.w, 8.0f);

			Assert::AreEqual(v2.x, 4.0f);
			Assert::AreEqual(v2.y, 8.0f);
			Assert::AreEqual(v2.z, 12.0f);
			Assert::AreEqual(v2.w, 16.0f);
		}

		TEST_METHOD(TestAdditionAssignment)
		{
			Vector4 v0(1.0f);
			Vector4 v1(2.0f, 3.0f, 4.0f, 5.0f);
			v0 += v1;

			Assert::AreEqual(v0.x, 3.0f);
			Assert::AreEqual(v0.y, 4.0f);
			Assert::AreEqual(v0.z, 5.0f);
			Assert::AreEqual(v0.w, 6.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
			Assert::AreEqual(v1.z, 4.0f);
			Assert::AreEqual(v1.w, 5.0f);
		}

		TEST_METHOD(TestSubtractionAssignment)
		{
			Vector4 v0(1.0f);
			Vector4 v1(2.0f, 3.0f, 4.0f, 5.0f);
			v0 -= v1;

			Assert::AreEqual(v0.x, -1.0f);
			Assert::AreEqual(v0.y, -2.0f);
			Assert::AreEqual(v0.z, -3.0f);
			Assert::AreEqual(v0.w, -4.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
			Assert::AreEqual(v1.z, 4.0f);
			Assert::AreEqual(v1.w, 5.0f);
		}

		TEST_METHOD(TestMultiplicationAssignment)
		{
			Vector4 v0(2.0f, 3.0f, 4.0f,5.0f);
			float f = 2.0f;
			v0 *= f;

			Assert::AreEqual(v0.x, 4.0f);
			Assert::AreEqual(v0.y, 6.0f);
			Assert::AreEqual(v0.z, 8.0f);
			Assert::AreEqual(v0.w, 10.0f);

			Assert::AreEqual(f, 2.0f);
		}

		TEST_METHOD(TestDivisionAssignment)
		{
			Vector4 v0(2.0f, 4.0f, 6.0f, 8.0f);
			float f = 2.0f;
			v0 /= f;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 2.0f);
			Assert::AreEqual(v0.z, 3.0f);
			Assert::AreEqual(v0.w, 4.0f);

			Assert::AreEqual(f, 2.0f);
		}

		//TEST_METHOD(TestCross)
		//{
		//	Vector4 v0(1.0f);
		//	Vector4 v1(2.0f, 5.0f, 3.0f);
		//	Vector4 v2 = Deku::Math::Cross(v0, v1);
		//
		//	Assert::AreEqual(v0.x, 1.0f);
		//	Assert::AreEqual(v0.y, 1.0f);
		//	Assert::AreEqual(v0.z, 1.0f);
		//
		//
		//	Assert::AreEqual(v1.x, 2.0f);
		//	Assert::AreEqual(v1.y, 5.0f);
		//	Assert::AreEqual(v1.z, 3.0f);
		//
		//	Assert::AreEqual(v2.x, -2.0f);
		//	Assert::AreEqual(v2.y, -1.0f);
		//	Assert::AreEqual(v2.z, 3.0f);
		//
		//}

		TEST_METHOD(TestDot)
		{
			Vector4 v0(1.0f);
			Vector4 v1(2.0f, 5.0f, 3.0f,4.0f);
			float f = Dot(v0, v1);

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);
			Assert::AreEqual(v0.z, 1.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 5.0f);
			Assert::AreEqual(v1.z, 3.0f);

			Assert::AreEqual(f, 14.0f);
		}

		TEST_METHOD(TestMagnitude)
		{
			Vector4 v1(3.0f, 4.0f, 0.0f,0.0f);
			float magnitude = Magnitude(v1);

			Assert::AreEqual(v1.x, 3.0f);
			Assert::AreEqual(v1.y, 4.0f);
			Assert::AreEqual(v1.z, 0.0f);

			Assert::AreEqual(magnitude, 5.0f);



		}

		TEST_METHOD(TestNormalize)
		{
			Vector4 v0 = (1.0f, 1.0f, 1.0f,1.0f);
			Vector4 normalized = Normalize(v0);
			float epsilon = 0.0001f;

			float diff = std::fabsf(0.5f - normalized.w);
			bool isEqual = false;

			if (diff <= epsilon)
			{
				isEqual = true;
			}

			Assert::IsTrue(isEqual);

		}
	};
}