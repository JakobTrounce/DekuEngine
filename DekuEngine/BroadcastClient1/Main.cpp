#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <vector>
#pragma comment(lib,"Ws2_32.lib")

int main(int argc, char* argv[])
{
	const char* hostAddress = "255.255.255.255";
	uint16_t port = 8888;

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	SOCKET mySocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//Resolve host using address
	//in_addr iaHost;
	//iaHost.s_addr = inet_addr(hostAddress);
	//HOSTENT* hostEntry = gethostbyaddr((const char*)&iaHost, sizeof(struct in_addr), AF_INET);

	//fill host address information and connect
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr = *((LPIN_ADDR)hostAddress);
	serverInfo.sin_port = htons(port);
	

	char buffer[1024];



	while (true)
	{
		int fromLength = sizeof(sockaddr);
		int bytesReceived = recvfrom(mySocket, buffer, std::size(buffer), 0, (LPSOCKADDR)&serverInfo, &fromLength);
		if (bytesReceived == SOCKET_ERROR)
		{
			printf("recv failed.\n");
			return -1;
		}
		if (bytesReceived == 0)
		{
		}
		buffer[bytesReceived] = '\0';
		printf("%s\n", buffer);
	}
	//close all sockets
	closesocket(mySocket);
	//Shutdown Winsock
	WSACleanup();
	return 0;
}

