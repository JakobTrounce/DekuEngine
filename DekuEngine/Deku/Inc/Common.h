#ifndef INCLUDED_DEKU_COMMON_H
#define INCLUDED_DEKU_COMMON_H

#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>
#include <Math/Inc/DekuMath.h>
#include <Physics/Inc/Physics.h>
#include <Input/Inc/Input.h>
#include <ImGui/Inc/imgui.h>



#endif // INCLUDED_DEKU_COMMON_H