#include <Deku/Inc/Deku.h>
#include <Physics/Inc/Physics.h>
using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Math;
using namespace Deku::Input;


// globals
	// fluid constansts

float viscosity = 150.0f;
float density = 10.0f;
float mass = 30.0f;


Window Init(HINSTANCE instance, int height, int width);
void Terminate(Window& myWindow);
// input
bool UserInput(Camera& camera, InputSystem* inputSystem);
// Kernels
double CalculatePoly6(float smoothingLength);
double CalculateSpikey(float smoothingLength);
double CalculateViscosity(float smoothingLength);

// momentum calculations
void CalculateDensityPressure(std::vector<Deku::Physics::Fluid>& particles);
void CalculateForces(std::vector<Deku::Physics::Fluid>& particles);


const float smoothingLength = 22.0f;

double poly = CalculatePoly6(smoothingLength);
double spikey = CalculateSpikey(smoothingLength);
double visc = CalculateViscosity(smoothingLength);

//--------------------------------------------------------------------------
void Reset(int screenW, int screenH, std::vector<Deku::Physics::Fluid>& particles);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int) {

	const int numOfParticles = 500;

	int screenW = 1280;
	int screenH = 720;

	// Setup window, initialise the graphics system
	Window myWindow = Init(hInstance, screenH, screenW);

	auto inputSystem = InputSystem::Get();
	
	float aspect = (float)screenW / (float)screenH;

	std::vector<Deku::Physics::Fluid> particles;

;

	Camera camera{};
	camera.SetPosition({ 0.0f,1.0f,0.0f });
	// setting the particle dam
	
	int i = 0;
	for (int y = 4; y < screenH - 2 * 2; y += 19)
	{
		for (int x = screenW >> 2; x < screenW >> 1; x += 19)
		{
			if (particles.size() < numOfParticles)
			{
				particles.push_back(Deku::Physics::Fluid({ static_cast<float>(x), static_cast<float>(y + 10) }, i));

				++i;
			}
		}
	}


	float* viscosityP;
	viscosityP = &viscosity;
	float* densityP;
	densityP = &density;
	float* massP;
	massP = &mass;
	
	static auto lastTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	bool done = false;


	while (!done)
	{
		currentTime = std::chrono::high_resolution_clock::now();
		done = myWindow.ProcessMessage();

		
		inputSystem->Update();
		GraphicsSystem::Get()->BeginRender(Colors::Black);

		float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastTime).count()/ 1000.0f;

		done = UserInput(camera, inputSystem);

		CalculateDensityPressure(particles);
		CalculateForces(particles);

		for (auto& particle : particles)
		{
			if (particle.Density() == 0.0)
			{
				particle.Density(1.0);
			}
			Vector2 velocity = (particle.Velocity() / particle.Density()) * deltaTime;
			particle.Position(particle.Position() + velocity);

			if (particle.Position().x - 20.0f < 250.0f)
			{
				velocity.x *= -0.5f;
				
				particle.Position({ 0.0f + 20.0f,particle.Position().y });
			}
			if (particle.Position().x + 20.0f > 600.0f)
			{
				velocity.x *= -0.5f;
				particle.Position({600.0f,particle.Position().y });
			}
			if (particle.Position().y - 20.0f < 0.0f)
			{
				velocity.y *= -0.5f;
				particle.Position({ particle.Position().x,20.0f });
			}
			if (particle.Position().y + 20.0f > 300.0f)
			{
				velocity.y *= -0.5f;
				particle.Position({ particle.Position().x,300.0f - 10.0f });
			}

			
			
			SimpleDraw::DrawScreenCircle({particle.Position()}, 10.0f, Colors::Blue);
		}
		SimpleDraw::Render(camera);

		DebugUI::BeginRender();

		if (ImGui::Button("Reset"))
		{
			Reset(screenW, screenH, particles);
		}
		ImGui::DragFloat("Viscosity",&viscosity, 1.0f, 0.0f, 80.0f);
		ImGui::DragFloat("Mass", &mass, 1.0f, 0.0f, 100.0f);


		DebugUI::EndRender();

		GraphicsSystem::Get()->EndRender();
		lastTime = currentTime;
	}

	Terminate(myWindow);
	return 0;
}

bool UserInput(Camera& camera, InputSystem* inputSystem)
{

	float moveSpeed = 0.05f;
	const float turnSpeed = 0.001f;
	if (inputSystem->IsKeyDown(KeyCode::LSHIFT))
	{
		moveSpeed *= 3;
	}
	if (inputSystem->IsKeyDown(KeyCode::SPACE))
	{
		camera.Rise(moveSpeed);
	}
	if (inputSystem->IsKeyDown(KeyCode::M))
	{
		camera.Rise(-moveSpeed);
	}
	if (inputSystem->IsKeyDown(KeyCode::W))
	{
		camera.Walk(moveSpeed);
	}
	if (inputSystem->IsKeyDown(KeyCode::S))
	{
		camera.Walk(-moveSpeed);
	}
	if (inputSystem->IsKeyDown(KeyCode::A))
	{
		camera.Strafe(moveSpeed);
	}
	if (inputSystem->IsKeyDown(KeyCode::D))
	{
		camera.Strafe(-moveSpeed);
	}
	if (inputSystem->IsMouseDown(MouseButton::RBUTTON))
	{
		camera.Yaw(inputSystem->GetMouseMoveX() * -turnSpeed);
		camera.Pitch(inputSystem->GetMouseMoveY() * turnSpeed);
	}
	moveSpeed = 0.05f;

	if (inputSystem->IsKeyPressed(KeyCode::ESCAPE))
	{
		return true;
	}

	return false;
}

Window Init(HINSTANCE hInstance, int height, int width)
{

	Window myWindow;
	myWindow.Initialize(hInstance, "Hello SpriteRenderer", width, height);

	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);
	SimpleDraw::StaticInitialize();
	InputSystem::StaticInitialize(myWindow.GetWindowHandle());
	DebugUI::StaticInitialize(myWindow.GetWindowHandle());
	SpriteRenderer::StaticInitialize();

	return myWindow;
}

void Terminate(Window& myWindow)
{
	InputSystem::StaticTerminate();
	SimpleDraw::StaticTerminate();
	SpriteRenderer::StaticTerminate();
	GraphicsSystem::StaticTerminate();

	myWindow.Terminate();
}

//mullers poly6 kernel
double CalculatePoly6(float smoothingLength)
{
	return static_cast<float>((315 / (65 * Constants::Pi * pow(smoothingLength, 9))));
}

// mullers spiky Kernel

double CalculateSpikey(float smoothingLength)
{
	return static_cast<float>(-15.0f / (Constants::Pi * pow(smoothingLength, 6)));
}

// mullers Viscosity kernel

double CalculateViscosity(float smoothingLength)
{
	return static_cast<float>(15 / (2 * Constants::Pi * pow(smoothingLength, 6)));
}

void CalculateDensityPressure(std::vector<Deku::Physics::Fluid>& particles)
{
	for (auto& particle : particles)
	{
		//particle.Pressure(0.0);
		particle.Density(0.0);
		for (auto& extParticle : particles)
		{
			Vector2 r = extParticle.Position() - particle.Position();
			float r2 = Deku::Math::MagnitudeSqr(r);
			if (r2 < 400.0f)
			{
				particle.Density(particle.Density() + particle.Mass() * poly * pow(400.0 - r2, 3.0f));
			}
		}
		particle.Pressure(20.0f * (particle.Density() - 10.0f));
	}
}

void CalculateForces(std::vector<Deku::Physics::Fluid>& particles)
{
	for (auto& particle : particles)
	{
		Vector2 pressureForce(0);
		Vector2 vicosityForce(0);
		for (auto& extParticle : particles)
		{
			if (particle.ID() == extParticle.ID())
				continue;


			Vector2 r = extParticle.Position() - particle.Position();
			float rMag = Math::Magnitude(r);
			if ( rMag < 20.0f)
			{
				pressureForce += (Math::Normalize(-r)) * particle.Mass() * (particle.Pressure() + extParticle.Pressure()) / (20.0f * extParticle.Density()) * spikey * pow((20.0f - rMag), 2.0f);

				vicosityForce += (extParticle.Velocity() - particle.Velocity()) * (particle.Viscosity()* extParticle.Mass()) / extParticle.Density() * visc * (20.0f - rMag);
			}
		}
		Vector2 gravityForce = { 0.0f,static_cast<float>(particle.Mass()) * Deku::Physics::Constants::Gravity * static_cast<float>(particle.Density()) };
		particle.Velocity(gravityForce + pressureForce + vicosityForce);
	}
}

void Reset(int screenW, int screenH,std::vector<Deku::Physics::Fluid>& particles)
{

	for (auto& particle : particles)
	{
		particle.Position(particle.startPos);
		particle.Mass(mass);
		particle.Viscosity(viscosity);
		particle.Density(density);
	}
}