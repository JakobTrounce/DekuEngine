#include "Miner.h" 

Miner::Miner()
	:
	mTexture(0)
{

}

Miner::Miner(X::Math::Vector2 pos, int energy)
	:
	mPos(pos),
	maxEnergy(energy),
	mEnergy(energy),
	mTexture(0)
{

}

void Miner::Load()
{
	mTexture = X::LoadTexture("yellow.png");

	mStateMachine = new AI::StateMachine<Miner>(*this);
	mStateMachine->AddState<RestState<Miner>>();
	mStateMachine->AddState<DepositInventoryState<Miner>>();
	mStateMachine->ChangeState(0);
}

void Miner::Unload()
{

}

void Miner::Update(float deltaTime)
{
	mStateMachine->Update(deltaTime);
}

void Miner::Render()
{
	X::DrawSprite(mTexture,mPos);
}

//void Mine::Enter(Miner& agent)
//{
//	energyTimer = 0.0f;
//}
//
//void Mine::Update(Miner& agent, float deltaTime)
//{
//	X::Math::Vector2 agentPos = agent.GetPos();
//	if (agentPos.x < mMinePos.x || agentPos.x > mMinePos.x && agentPos.y < mMinePos.y || agentPos.y > mMinePos.y)
//	{
//		// seek to mine
//	}
//	else
//	{
//		if (energyTimer >= 2.0f)
//		{
//			agent.LoseEnergy(10);
//			int stoneAmount = X::Random(1, 5);
//			for (int i = 0; i < stoneAmount; ++i)
//			{
//				agent.GetInventory().push_back((int)Items::stone);
//			}
//		}
//		if (agent.GetEnergy() <= 0)
//		{
//			agent.mStateMachine->ChangeState((int)General::RestState);
//		}
//		else if (agent.GetInventory().size() >= agent.GetMaxInvSize())
//		{
//			agent.mStateMachine->ChangeState((int)General::DepositState);
//		}
//	}
//}
//
//void Mine::Exit(Miner& agent)
//{
//
//}