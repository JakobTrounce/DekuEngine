#pragma once
#include <XEngine.h>

class CameraGame
{
public:
	static void StaticInitialize();
	static void StaticTerminate();
	static CameraGame* Get();

public:
	CameraGame();
	~CameraGame();

	void SetViewPos(X::Math::Vector2 pos);
	
	X::Math::Vector2 ConvertToWorld(X::Math::Vector2 screenPos);
	X::Math::Vector2 ConvertToScreen(X::Math::Vector2 worldPos);

	void Render(X::TextureId id, X::Math::Vector2 worldPos);

private:
	X::Math::Vector2 mViewPosition;
};

