#ifndef _INCLUDED_DEKUENGINE_CORE_DEBUG_H
#define _INCLUDED_DEKUENGINE_CORE_DEBUG_H


#if defined(_DEBUG)
	#define LOG(format,...)\
	do{\
		char buffer[4096];\
		int ret = _snprintf_s(buffer,std::size(buffer),_TRUNCATE, "%s(%d) " ##format, __FILE__, __LINE__, __VA_ARGS__);\
		OutputDebugStringA(buffer);\
		if(ret == -1)\
			OutputDebugStringA("** ,message truncated ** \n");\
		OutputDebugStringA("\n");\
	}while(false)
	#define DEKUASSERT(condition, format, ...)\
	do{\
		if(!(condition))\
		{\
			LOG(format, __VA_ARGS__);\
			DebugBreak();\
		}\
	}while(false)
#else
	#define DEKUASSERT(condition, format, ...)
	#define LOG(format,...)
#endif 

#endif // _INCLUDED_DEKUENGINE_CORE_DEBUG_H