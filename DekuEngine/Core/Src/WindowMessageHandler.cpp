#include "Precompiled.h"
#include "WindowMessageHandler.h"

#include "Debug.h"

void Deku::Core::WindowMessageHandler::Hook(HWND window, Callback cb)
{
	mWindow = window;
	mPreviousCallback = (Callback)GetWindowLongPtrA(window, GWLP_WNDPROC);
	SetWindowLongPtrA(window, GWLP_WNDPROC, (LONG_PTR)cb);
}

void Deku::Core::WindowMessageHandler::UnHook()
{
	SetWindowLongA(mWindow, GWLP_WNDPROC, (LONG_PTR)mPreviousCallback);
	mWindow = nullptr;
}

LRESULT Deku::Core::WindowMessageHandler::ForwardMessage(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	DEKUASSERT(mPreviousCallback, "[Core::WindowMessageHandler] No callback is hooked.");
	return CallWindowProc((WNDPROC)mPreviousCallback, window, message, wParam, lParam);
}