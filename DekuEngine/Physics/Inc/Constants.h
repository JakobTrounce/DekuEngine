#ifndef INCLUDED_DEKU_PHYSICS_CONSTANTS_H
#define INCLUDED_DEKU_PHYSICS_CONSTANTS_H

namespace Deku::Physics::Constants
{
	constexpr float Gravity = 9.82f;
}


#endif //INCLUDED_DEKU_PHYSICS_CONSTANTS_H