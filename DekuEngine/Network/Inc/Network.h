#ifndef INCLUDED_NETWORK_H
#define INCLUDED_NETWORK_H

#include "Common.h"

#include "StreamWriter.h"
#include "StreamReader.h"
#include "MemoryStream.h"
#include "NetworkUtil.h"
#include "SocketAddress.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "LinkingContext.h"
#include "ObjectFactory.h"
#pragma comment(lib,"Ws2_32.lib")

//255.255.255.255 broadcast ip

#endif
