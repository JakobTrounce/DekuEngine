#include "Character.h"

Character::Character()
	:GameObject(true)
	, mPosition({ 0.0f,0.0f })
	, mTextureId(0)
{

}

void Character::Load()
{
	mTextureId = X::LoadTexture("mario.png");
}

void Character::Update(float deltaTime)
{
	if (!mCommand.Empty())
	{
		Dirty();


		float commandTime = mCommand.GetCommandTime(deltaTime);

		const float kMoveSpeed = 200.0f;
		if (mCommand.MoveForward())
		{
			mPosition.y -= kMoveSpeed * commandTime;
		}
		if (mCommand.MoveBackward())
		{
			mPosition.y += kMoveSpeed * commandTime;
		}
		if (mCommand.MoveLeft())
		{
			mPosition.x -= kMoveSpeed * commandTime;
		}
		if (mCommand.MoveRight())
		{
			mPosition.x += kMoveSpeed * commandTime;
		}

		mCommand.Update(deltaTime);
	}
}

void Character::Draw()
{
	X::DrawSprite(mTextureId, mPosition);
}

void Character::Serialize(Network::StreamWriter& writer) const
{
	writer.Write(mPosition.x);
	writer.Write(mPosition.y);
}

void Character::Deserialize(Network::StreamReader& reader)
{
	reader.Read(mPosition.x);
	reader.Read(mPosition.y);
}