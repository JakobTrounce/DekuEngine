//====================================================================================================
// Filename:	DoLighting.fx
// Created by:	Jakob Trounce
// Description: Shader that applies transformation and Lighting.
//====================================================================================================

cbuffer ConstantBuffer : register(b0)
{
	matrix WVP
}

struct VSInput
{
	float4 position : POSITION;
	float normal : NORMAL;
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

VS_OUTPUT VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.position = mul(input.position, WVP);
	output.Color = float4(0,1,1,1);
	return output;
}

float4 PS(VSOutput input) : SV_Target
{
	return input.color;
}
