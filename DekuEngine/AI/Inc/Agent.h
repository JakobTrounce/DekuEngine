#ifndef INCLUDED_AI_AGENT_H
#define INCLUDED_AI_AGENT_H
#include <XEngine.h>

namespace AI {


class AIWorld;

class Agent
{
public:
	Agent(AIWorld& world);
	virtual ~Agent();
	X::Math::Matrix3 LocalToWorld() const;

	AIWorld& World() const						{ return mWorld; }
	
	X::Math::Vector2& Position()				{ return mPosition; }
	const X::Math::Vector2& Position() const	{ return mPosition; }
	
	X::Math::Vector2& Velocity()				{ return mVelocity; }
	const X::Math::Vector2& Velocity() const	{ return mVelocity; }

	X::Math::Vector2& Destination()				{ return mDestination; }
	const X::Math::Vector2& Destination() const { return mDestination; }

	X::Math::Vector2& Heading()					{ return mHeading; }
	const X::Math::Vector2& Heading() const		{ return mHeading; }

	float& MaxSpeed()							{ return mMaxSpeed; }
	const float MaxSpeed() const				{ return mMaxSpeed; }

	int& Neighbours()							{ return mNeighbours; }
	const int Neighbours() const				{ return mNeighbours; }
protected:
	AIWorld& mWorld;
	X::Math::Vector2 mPosition;
	X::Math::Vector2 mVelocity;
	X::Math::Vector2 mDestination;
	X::Math::Vector2 mHeading;

	float mMaxSpeed;
	int mNeighbours = 0;
};

} // namespace AI

#endif // #include INCLUDED_AI_AGENT_H