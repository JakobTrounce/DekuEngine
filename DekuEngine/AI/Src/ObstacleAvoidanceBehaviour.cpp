#include "Precompiled.h"
#include "ObstacleAvoidanceBehaviour.h"

using namespace AI;

X::Math::Vector2 AI::ObstacleAvoidanceBehaviour::Calculate(Agent & agent)
{
	float maxAhead = 100.0f;
	agent.Heading() = X::Math::Normalize(agent.Velocity());
	X::Math::Vector2 ahead = agent.Position() + agent.Heading() * maxAhead;

	X::Math::Circle closest = GetNearestObstacle(agent, ahead);
	X::Math::Vector2 avoid = { 0,0 };

	avoid = agent.Heading() - closest.center;
	

	avoid = X::Math::Normalize(avoid) * agent.MaxSpeed();
	
	return avoid;
}


X::Math::Circle AI::ObstacleAvoidanceBehaviour::GetNearestObstacle(Agent& agent, const X::Math::Vector2& ahead)
{
	auto agentList = agent.World().GetObstacles();
	X::Math::Circle closest;
	
	for (const auto& a : agentList)
	{
		bool collides = X::Math::Intersect({ agent.Position(), ahead }, a);
		if (collides && X::Math::Distance(agent.Position(),a.center) < X::Math::Distance(agent.Position(), closest.center))
		{
			closest = a;
		}
	}
	return closest;
}

