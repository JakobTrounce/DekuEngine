#include "Precompiled.h"
#include "SeekBehavior.h"

using namespace AI;

SeekBehaviour::SeekBehaviour()
{

}


X::Math::Vector2 SeekBehaviour::Calculate(Agent& agent)
{
	X::Math::Vector2 desiredVel = (X::Math::Normalize(agent.Destination() - agent.Position())) * agent.MaxSpeed();

	return desiredVel - agent.Velocity();
}