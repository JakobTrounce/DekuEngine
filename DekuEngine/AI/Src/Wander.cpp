#include "Precompiled.h"
#include "Wander.h"

X::Math::Vector2 AI::WanderBehaviour::Calculate(Agent & agent)
{
	float circleDistance = 400.0f;
	float radius = 200.0f;
	X::Math::Circle ai;

	if (agent.Velocity().x != 0.0f && agent.Velocity().y != 0.0f)
	{
		ai.center = agent.Position() + X::Math::Normalize(agent.Velocity()) * circleDistance;
	}
	ai.radius = radius;

	X::DrawScreenCircle(ai, X::Math::Vector4::Green());
	X::DrawScreenLine(agent.Position(), ai.center , X::Math::Vector4::LightBlue());


	agent.Destination() = ai.center + X::Math::Rotate({ radius, 0 }, X::Random(0, 360));

	X::Math::Vector2 desired = (X::Math::Normalize(agent.Destination() - agent.Position())) * agent.MaxSpeed();

	return desired - agent.Velocity();

}

