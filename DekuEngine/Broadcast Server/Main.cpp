#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <cstring>
#pragma comment(lib,"Ws2_32.lib")

int main(int argc, char* argv[])
{
	uint16_t port = 8888;
	printf("Hello World\n");

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);



	// create a socket using UDP/IP
	SOCKET listener = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//have to bind socket to an address

	// fill server address info
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET; // address family or format

	// the address format tells these how to read the ip
	serverInfo.sin_addr.s_addr = 255.255,255,255; // check for any ip
	serverInfo.sin_port = htons(port); //  connection port

	printf("Listening on port %hu...\n", port);
	char buffer[1024] = "I see you \n";
	//bind the address to our listener socket and listen for connection
	bind(listener, (sockaddr*)&serverInfo.sin_addr.s_addr, sizeof(struct sockaddr));
	setsockopt(listener, SOL_SOCKET, SO_BROADCAST, buffer, std::size(buffer));
	int bytesSent;
	// server forever...
	while (true)
	{
		//accept any incoming client connection **blocking**
		// why this is blocking
		//// the application hits a halt at this line, it must wait for a response from the network card
		// this is called a blocking call

		 bytesSent = sendto(listener, buffer, std::size(buffer), 0, (sockaddr*)&serverInfo.sin_addr.s_addr, sizeof(sockaddr));

		// we do this
		// recv
		//send
		// closesocket;

		//closesocket(client);
	}

	//close all sockets
	closesocket(listener);

	//Shutdown Winsock
	WSACleanup();
	return 0;
}