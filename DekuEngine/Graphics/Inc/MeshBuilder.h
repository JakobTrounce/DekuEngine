#ifndef INCLUDED_DEKU_GRAPHICS_MESH_BUILDER_H 
#define INCLUDED_DEKU_GRAPHICS_MESH_BUILDER_H 

#include "Mesh.h"

namespace Deku {
namespace Graphics {

class MeshBuilder
{
public:
	static MeshPC CreateCube();
	static MeshPC CreateTriangle();
	static MeshPC CreatePlane(int rows, int col,float thickness);
	//static MeshPC CreateCylender(int row, int col)
	//{
	//	for (int y = 0; y < row; ++y)
	//	{
	//		for (int x = 0; x < col; ++x)
	//		{
	//			int index = x + (y*col);
	//			VertexPC vertices[index] = { x,y,z };
				//X and Z is gonna be some value of sin and cosin
	//		}
	//	}
	//}
	static MeshPC CreateSphere(int row, int col, float radius); //apply sin and cosine to Y aswell
	//static MeshPC CreateTorus() //just bend a cylnder 
	static MeshPX CreateSphereUV(int row, int col, float radius);
	static MeshPX CreateSkyBoxUV(int height, int width, int depth);
	static MeshPN CreateSpherePN(int rows, int col, float radius);
private:


};


} //Graphics
} //Deku

#endif // defined INCLUDED_VISHV_GRAPHICS_MESH_BUILDER_H