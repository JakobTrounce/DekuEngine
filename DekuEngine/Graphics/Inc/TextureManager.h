#ifndef INCLUDED_DEKU_GRAPHICS_TEXTUREMANAGER_H
#define INCLUDED_DEKU_GRAPHICS_TEXTUREMANAGER_H

#include "Texture.h"

namespace Deku::Graphics
{
	using TextureID = size_t;

	class TextureManager
	{
	public:

		static void StaticInitialize();
		static void StaticTerminate();
		static TextureManager* Get();

	public:

		TextureManager() = default;
		~TextureManager();

		void Initialize();
		void Terminate();

		void SetRootPath(std::wstring path);

		TextureID LoadTexture(std::wstring fileName);
		const Texture* GetTexture(TextureID textureId) const;

	private:

		std::wstring mRootPath = L"../Assets/";
		std::unordered_map<TextureID, std::unique_ptr<Texture>> mInventory;
	};
}
#endif
