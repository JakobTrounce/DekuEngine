#ifndef INCLUDED_DEKU_GRAPHICS_H
#define INCLUDED_DEKU_GRAPHICS_H

namespace Deku::Graphics{


class MeshBuffer
{
public:
	enum class Topology {Points, Lines, Triangles};

	template<class MeshType>
	void Initialize(const MeshType& mesh, bool dynamic);

	void Initialize(const void* vertexData, int vertexSize, uint32_t numVertices, const uint32_t* data, uint32_t numIndices, bool dynamic = false);
	void Initialize(const void* vertexData, int vertexSize, uint32_t numVertices, bool dynamic = false);
	void Terminate();

	void SetTopology(Topology topology);

	void Update(const void* vertexData, uint32_t numVertices);

	void Render();

private:
	void CreateVertexBuffer(const void* vertexData, int vertexSize, uint32_t numVertices, bool dynamic = false);
	void CreateIndexBuffer(const uint32_t* data, uint32_t numIndices);
	ID3D11Buffer* mVertexBuffer{ nullptr };
	ID3D11Buffer* mIndexBuffer{ nullptr };

	D3D11_PRIMITIVE_TOPOLOGY mTopology;

	uint32_t mVertexSize{ 0 };
	int mVertexCount{ 0 };
	int mIndexCount{ 0 };
};

template<class MeshType>

inline void Deku::Graphics::MeshBuffer::Initialize(const MeshType & mesh, bool dynamic)

{

	Initialize(mesh.mVertices.size(), sizeof(MeshType::VertexType), mesh.mVertices.data(), mesh.mIndices.data(), mesh.mIndices.size());



}

}


#endif // INCLUDED_DEKU_GRAPHICS_H