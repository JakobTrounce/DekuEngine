#ifndef INCLUDED_DEKU_GRAPHICS_SPRITERENDERER_H
#define INCLUDED_DEKU_GRAPHICS_SPRITERENDERER_H

namespace DirectX { class SpriteBatch; }

using namespace Deku;
namespace Deku::Graphics {

	class Texture;

class SpriteRenderer
{
public:
	static void StaticInitialize();
	static void StaticTerminate();
	static SpriteRenderer* Get();

public:
	SpriteRenderer() = default;
	~SpriteRenderer();
	
	SpriteRenderer(const SpriteRenderer&) = delete;
	SpriteRenderer& operator=(const SpriteRenderer) = delete;

	void Initialize();
	void Terminate();

	void BeginRender();
	void EndRender();

	void Draw(const Texture& texture, const Deku::Math::Vector3& pos);

private:
	std::unique_ptr<DirectX::SpriteBatch> mSpriteBatch = nullptr;
};
}

#endif // defined INCLUDED_DEKU_GRAPHICS_SPRITERENDERER_H