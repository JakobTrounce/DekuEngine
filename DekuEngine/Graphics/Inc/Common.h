#ifndef INCLUDED_DEKUENGINE_GRAPHICS_COMMON_H
#define INCLUDED_DEKUENGINE_GRAPHICS_COMMON_H


//Engine headers
#include <Core/Inc/Core.h>
#include <Math/Inc/DekuMath.h>
#include <Input/Inc/Input.h>

//Directx headers
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

//common headers
#include "VertexTypes.h"

//directX libraries
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")


//utility functions to release D3D interfaces

#endif //defined INCLUDED_DEKUENGINE_GRAPHICS_COMMON_H