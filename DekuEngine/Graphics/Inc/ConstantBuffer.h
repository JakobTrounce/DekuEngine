#ifndef INCLUDED_DEKU_GRAPHICS_CONSTANTBUFFER_H
#define INCLUDED_DEKU_GRAPHICS_CONSTANTBUFFER_H
#include "Common.h"
#include "Camera.h"

namespace Deku{
namespace Graphics{
class ConstantBuffer
{
public:
	void Initialize(uint32_t constantSize,const void* initData = nullptr);
	void Bind(void* constants);

	void Set(void* constants);

	void BindPS();
	void BindVS();
	void Terminate();
private:
	ID3D11Buffer* mConstantBuffer{ nullptr };
};
}
}




#endif
