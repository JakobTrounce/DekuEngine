#ifndef INCLUDED_DEKU_GRAPHICS_TEXTURE_H
#define INCLUDED_DEKU_GRAPHICS_TEXTURE_H

namespace Deku {
namespace Graphics {
class Texture
{
public:
	Texture() = default;
	~Texture();

	void Initialize(const wchar_t* fileName);
	void Terminate();

	void BindPS(uint32_t slot) const;

private:

	std::wstring ConvertString(std::string& string);

	friend ID3D11ShaderResourceView* GetShaderResourceView(const Texture& texture);
	ID3D11ShaderResourceView* mShaderResourceView{nullptr};

};

}
}


#endif // !INCLUDED_DEKU_GRAPHICS_TEXTURE_H
