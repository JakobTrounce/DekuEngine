#ifndef INCLUDED_DEKU_GRAPHICS_PIXELSHADER_H
#define INCLUDED_DEKU_GRAPHICS_PIXELSHADER_H
namespace Deku{
namespace Graphics{

class PixelShader
{
public:
	void Initialize(std::string shaderName);
	void Terminate();

	void Bind();
	std::wstring ConvertString(std::string & s);
private:

	ID3D11PixelShader* pixelShader{nullptr};
};



}
}



#endif //INCLUDED_DEKU_GRAPHICS_PIXELSHADER_H