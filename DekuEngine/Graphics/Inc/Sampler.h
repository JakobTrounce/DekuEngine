#ifndef INCLUDED_DEKU_GRAPHICS_SAMPLER_H
#define INCLUDED_DEKU_GRAPHICS_SAMPLER_H

namespace Deku {
namespace Graphics {

class Sampler
{

public:
	enum class Filter { Point, Linear, Anisotropic };
	enum class AddressMode {Border, Clamp, Mirror, Wrap};


	Sampler() = default;
	~Sampler();

	void Initialize(Filter filter, AddressMode addressMode);
	void Terminate();

	void BindPS(uint32_t slot = 0);

private:
	ID3D11SamplerState* mSampler{ nullptr };
};
}
}

#endif // !INCLUDED_DEKU_GRAPHICS_SAMPLER_H