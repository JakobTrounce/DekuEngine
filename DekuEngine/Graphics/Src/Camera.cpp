#include "Precompiled.h"
#include "Camera.h"

#include "GraphicsSystem.h"
using namespace Deku;
using namespace Deku::Graphics;

Math::Matrix4 Deku::Graphics::ComputeViewMatrix(Math::Vector3 right, Math::Vector3 up, Math::Vector3 look, Math::Vector3 position)
{
	const float dx = - Math::Dot(right, position);
	const float dy = - Math::Dot(up, position);
	const float dz = - Math::Dot(look, position);

	return {
		right.x, up.x, look.x, 0.0f,
		right.y, up.y, look.y, 0.0f,
		right.z, up.z, look.z, 0.0f,
		dx     , dy  , dz    , 1.0f
	};
}

Math::Matrix4 Deku::Graphics::ComputePerspectiveMatrix(float n, float f, float fov, float aspectRatio)
{   
	
	const float h = 1 / tan(fov * 0.5f);
	const float w = h / aspectRatio;
	const float zf = f;
	const float zn = n;
	const float d = zf / (zf - zn);


	return {
		w,		0.0f,	0.0f,		0.0f,
		0.0f,	h,		0.0f,		0.0f,
		0.0f,	0.0f,	d,			1.0f,
		0.0f,	0.0f,	-zn*d,	     0.0f
	};
}

Math::Matrix4 Camera::GetViewMatrix() const
{
	const Math::Vector3 look = Normalize(mDirection);
	const Math::Vector3 right = Math::Normalize(Math::Cross({0.0f, 1.0f, 0.0f}, look));
	const Math::Vector3 up = Math::Normalize(Math::Cross(look, right));
	return ComputeViewMatrix(right, up, look, mPosition);
}

Math::Matrix4 Camera::GetPerspectiveMatrix(float aspectRatio) const
{
	if (aspectRatio == 0.0f)
	{
		auto width = GraphicsSystem::Get()->GetBackBufferWidth();
		auto height = GraphicsSystem::Get()->GetBackBufferHeight();
		aspectRatio = static_cast<float>(width) / static_cast<float>(height);
	}
	return ComputePerspectiveMatrix(mNearPlane, mFarPlane, mFov, aspectRatio);
}

