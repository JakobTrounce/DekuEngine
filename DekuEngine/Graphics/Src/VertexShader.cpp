#include "Precompiled.h"
#include "VertexShader.h"
#include "GraphicsSystem.h"
#include "VertexTypes.h"

using namespace Deku;
using namespace Deku::Graphics;

namespace
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> GetVertexLayout(uint32_t vertexFormat)
	{
		std::vector< D3D11_INPUT_ELEMENT_DESC> desc;
		if (vertexFormat & VE_Position)
		{
			desc.push_back({ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VE_Normal)
		{
			desc.push_back({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VE_Tangent)
		{
			desc.push_back({ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VE_Color)
		{
			desc.push_back({ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VE_Texcoord)
		{
			desc.push_back({"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0});
		}
		if (vertexFormat & VE_Blendindex)
		{
			desc.push_back({ "BLENDINDICES", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		if (vertexFormat & VE_BlendWidth)
		{
			desc.push_back({ "BLENDWIDTH", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		return desc;
	}
}

void VertexShader::Initialize(std::string shaderName, uint32_t vertexFormat)
{
	////Describes what a Vertex Struct is to the GPU
	//const D3D11_INPUT_ELEMENT_DESC elemDescs[] =
	//{
	//	{
	//		"POSITION", 0,
	//		DXGI_FORMAT_R32G32B32_FLOAT,		//Telling the GPU that we have x, y, z as float (32)
	//		0,
	//		D3D11_APPEND_ALIGNED_ELEMENT,
	//		D3D11_INPUT_PER_VERTEX_DATA,
	//		0
	//	},
	//	{
	//		"COLOR", 0,
	//		DXGI_FORMAT_R32G32B32A32_FLOAT,		//Telling the gpu that R, G, B, A as float (32)
	//		0,
	//		D3D11_APPEND_ALIGNED_ELEMENT,
	//		D3D11_INPUT_PER_VERTEX_DATA,
	//		0
	//	},
	//};
	std::vector<D3D11_INPUT_ELEMENT_DESC> elemDescs = GetVertexLayout(vertexFormat);

	HRESULT hr;

	//Compile our vertex shader code
	//ID3DBlob* shaderBlob = nullptr;
	//ID3DBlob* errorBlob = nullptr;

	std::wstring name = ConvertString(shaderName);

	hr = D3DCompileFromFile(name.c_str(), nullptr, nullptr, "VS", "vs_5_0", 0, 0, &shaderBlob, &errorBlob);
	DEKUASSERT(SUCCEEDED(hr), "Failed to compile vertex shader. Error: %s", (const char*)errorBlob->GetBufferPointer());


	hr = GraphicsSystem::Get()->GetDevice()->CreateVertexShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, &mVertexShader);
	DEKUASSERT(SUCCEEDED(hr), "Failed to create vertex shader.");

	hr = GraphicsSystem::Get()->GetDevice()->CreateInputLayout
	(elemDescs.data(), static_cast<UINT>(std::size(elemDescs)),
		shaderBlob->GetBufferPointer(), 
		shaderBlob->GetBufferSize(), 
		&mInputLayout);

	DEKUASSERT(SUCCEEDED(hr), "Failed to create input layout.");

	SafeRelease(shaderBlob);
	SafeRelease(errorBlob);
}

std::wstring VertexShader::ConvertString(std::string & s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}


void VertexShader::Bind()
{
	GraphicsSystem::Get()->GetContext()->IASetInputLayout(mInputLayout);
	GraphicsSystem::Get()->GetContext()->VSSetShader(mVertexShader, nullptr, 0);
}

VertexShader::~VertexShader()
{
	DEKUASSERT(mVertexShader == nullptr, "[Deku::Graphics::VertexShader] Didn't call terminate for vertex shader.");
	DEKUASSERT(mInputLayout == nullptr, "[Deku::Graphics::VertexShader] Didn't call terminate for vertex shader.");
}

void VertexShader::Terminate()
{
	SafeRelease(mVertexShader);
	SafeRelease(mInputLayout);
}