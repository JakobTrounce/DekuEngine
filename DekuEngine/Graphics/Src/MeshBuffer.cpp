#include "Precompiled.h"
#include "MeshBuffer.h"

#include "GraphicsSystem.h"

using namespace Deku;
using namespace Graphics;

void MeshBuffer::Initialize(const void* vertexData, int vertexSize, uint32_t numVertices, const uint32_t* indexData, uint32_t numIndices, bool dynamic)
{
	CreateVertexBuffer(vertexData, vertexSize, numVertices, dynamic);
	CreateIndexBuffer(indexData, numIndices);
}

void Deku::Graphics::MeshBuffer::Initialize(const void * vertexData, int vertexSize, uint32_t numVertices, bool dynamic)
{
	CreateVertexBuffer(vertexData, vertexSize, numVertices, dynamic);
}

void MeshBuffer::Terminate()
{
	SafeRelease(mIndexBuffer);
	SafeRelease(mVertexBuffer);
}

void Deku::Graphics::MeshBuffer::SetTopology(Topology topology)
{
	switch (topology)
	{
	case Topology::Points:
		mTopology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
		break;
	case Topology::Lines:
		mTopology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
		break;
	case Topology::Triangles:
		mTopology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	}

}

void MeshBuffer::Update(const void * vertexData, uint32_t numVertices)
{
	mVertexCount = numVertices;
	auto context = GraphicsSystem::Get()->GetContext();

	D3D11_MAPPED_SUBRESOURCE resource;
	context->Map(mVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
	memcpy(resource.pData, vertexData, (unsigned int)numVertices * mVertexSize);
	context->Unmap(mVertexBuffer, 0);
}

void MeshBuffer::Render()
{
	UINT stride = (UINT)mVertexSize;
	UINT offset = 0;

	auto context = GraphicsSystem::Get()->GetContext();
	context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);
	context->IASetPrimitiveTopology(mTopology);

	if (mIndexBuffer != nullptr)
	{
		context->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
		context->DrawIndexed(static_cast<UINT>(mIndexCount), 0, 0);
	}
	else
	{
		context->Draw(static_cast<UINT>(mVertexCount), 0);
	}
}

void Deku::Graphics::MeshBuffer::CreateVertexBuffer(const void * vertexData, int vertexSize, uint32_t numVertices, bool dynamic)
{
	mVertexSize = vertexSize;
	mVertexCount = numVertices;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = numVertices * vertexSize;
	bufferDesc.Usage = dynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = dynamic ? D3D11_CPU_ACCESS_WRITE : 0;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	
	// create initialization data
	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = vertexData;
	initData.SysMemPitch = 0;
	initData.SysMemSlicePitch = 0;
	

	auto device = GraphicsSystem::Get()->GetDevice();
	HRESULT hr = device->CreateBuffer(&bufferDesc, vertexData ? &initData : nullptr , &mVertexBuffer);
	DEKUASSERT(SUCCEEDED(hr), "[MeshBuffer] Failed to create device vertex buffer. ");
}

void Deku::Graphics::MeshBuffer::CreateIndexBuffer(const uint32_t * data, uint32_t numIndices)
{

	mIndexCount = numIndices;
	// create buffer desc
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(uint32_t) * numIndices;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	// create initialization data
	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = data;
	initData.SysMemPitch = 0;
	initData.SysMemSlicePitch = 0;

	bufferDesc.ByteWidth = sizeof(uint32_t) * numIndices;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	initData.pSysMem = data;

	auto device = GraphicsSystem::Get()->GetDevice();
	HRESULT hr = device->CreateBuffer(&bufferDesc, &initData, &mIndexBuffer);
	DEKUASSERT(SUCCEEDED(hr), "[MeshBuffer] Failed to create device index buffer. ");
}
