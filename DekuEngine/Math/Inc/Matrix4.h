#ifndef INCLUDED_DEKU_MATH_MATRIX4_H
#define INCLUDED_DEKU_MATH_MATRIX4_H
#include "Constants.h"
namespace Deku {
namespace Math {
	struct Matrix4
	{
		union
		{
			struct  // row+column subscript
			{
				float _11, _12, _13, _14;
				float _21, _22, _23, _24;
				float _31, _32, _33, _34;
				float _41, _42, _43, _44;
			};
			std::array<float, 16> v;
		};


		Matrix4() noexcept : Matrix4(0.0f) {}
		Matrix4(float f) noexcept
			: _11(f), _12(f), _13(f), _14(f),
			_21(f), _22(f), _23(f), _24(f),
			_31(f), _32(f), _33(f), _34(f),
			_41(f), _42(f), _43(f), _44(f)
		{}
		Matrix4(float _11, float _12, float _13, float _14, float _21, float _22, float _23, float _24, float _31, float _32, float _33, float _34, float _41, float _42, float _43, float _44)
			: _11(_11), _12(_12), _13(_13), _14(_14),
			_21(_21), _22(_22), _23(_23), _24(_24),
			_31(_31), _32(_32), _33(_33), _34(_34),
			_41(_41), _42(_42), _43(_43), _44(_44)
		{}

		Matrix4 operator*(const int & power)
		{
			return Matrix4(
				_11 * power, _12 * power, _13 * power, _14 * power,

				_21 * power, _22 * power, _23 * power, _24 * power,

				_31 * power, _32 * power, _33 * power, _34 * power,

				_41 * power, _42 * power, _43 * power, _44 * power);
		}
		Matrix4 operator*(float power)
		{
			return Matrix4(
				_11 * power, _12 * power, _13 * power, _14 * power,

				_21 * power, _22 * power, _23 * power, _24 * power,

				_31 * power, _32 * power, _33 * power, _34 * power,

				_41 * power, _42 * power, _43 * power, _44 * power);
		}
		Matrix4& operator=(Matrix4 other)
		{
			_11 = other._11;	_12 = other._12;	_13 = other._13;	_14 = other._14;
			_21 = other._21;	_22 = other._22;	_23 = other._23;	_24 = other._24;
			_31 = other._31;	_32 = other._32;	_33 = other._33;	_34 = other._34;
			_41 = other._41;	_42 = other._42;	_43 = other._43;	_44 = other._44;

			return *this;
		}
		Matrix4 operator*(const Matrix4 & other)
		{
			return Matrix4
			(
				(_11 * other._11 + _12 * other._21 + _13 * other._31 + _14 * other._41), (_11 * other._12 + _12 * other._22 + _13 * other._32 + _14 * other._42), (_11 * other._13 + _12 * other._23 + _13 * other._33 + _14 * other._43), (_11 * other._14 + _12 * other._24 + _13 * other._34 + _14 * other._44),

				(_21 * other._11 + _22 * other._21 + _23 * other._31 + _24 * other._41), (_21 * other._12 + _22 * other._22 + _23 * other._32 + _24 * other._42), (_21 * other._13 + _22 * other._23 + _23 * other._33 + _24 * other._43), (_21 * other._14 + _22 * other._24 + _23 * other._34 + _24 * other._44),

				(_31 * other._11 + _32 * other._21 + _33 * other._31 + _34 * other._41), (_31 * other._12 + _32 * other._22 + _33 * other._32 + _34 * other._42), (_31 * other._13 + _32 * other._23 + _33 * other._33 + _34 * other._43), (_31 * other._14 + _32 * other._24 + _33 * other._34 + _34 * other._44),

				(_41 * other._11 + _42 * other._21 + _43 * other._31 + _44 * other._41), (_41 * other._12 + _42 * other._22 + _43 * other._32 + _44 * other._42), (_41 * other._13 + _42 * other._23 + _43 * other._33 + _44 * other._43), (_41 * other._14 + _42 * other._24 + _43 * other._34 + _44 * other._44)
			);
		}
		Matrix4& operator*=(float power)
		{
			_11 *= power;	_12 *= power;	_13 *= power;	_14 *= power;
			_21 *= power;	_22 *= power;	_23 *= power;	_24 *= power;
			_31 *= power;	_32 *= power;	_33 *= power;	_34 *= power;
			_41 *= power;	_42 *= power;	_43 *= power;	_44 *= power;

			return *this;
		}
		bool operator==(const Matrix4& other) const
		{
			return(

				fabs(_11 - other._11) < Constants::Epsilon && fabs(_12 - other._12) < Math::Constants::Epsilon && fabs(_13 - other._13) < Math::Constants::Epsilon && fabs(_14 - other._14) < Math::Constants::Epsilon &&
				fabs(_21 - other._21) < Math::Constants::Epsilon && fabs(_22 - other._22) < Math::Constants::Epsilon && fabs(_23 - other._23) < Math::Constants::Epsilon && fabs(_24 - other._24) < Math::Constants::Epsilon &&
				fabs(_31 - other._31) < Math::Constants::Epsilon && fabs(_32 - other._32) < Math::Constants::Epsilon && fabs(_33 - other._33) < Math::Constants::Epsilon && fabs(_34 - other._34) < Math::Constants::Epsilon &&
				fabs(_41 - other._41) < Math::Constants::Epsilon && fabs(_42 - other._42) < Math::Constants::Epsilon && fabs(_43 - other._43) < Math::Constants::Epsilon && fabs(_44 - other._44) < Math::Constants::Epsilon

				);
		}
		bool operator!=(const Matrix4& other) const
		{
			return(

				fabs(_11 - other._11) > Math::Constants::Epsilon || fabs(_12 - other._12) > Math::Constants::Epsilon || fabs(_13 - other._13) > Math::Constants::Epsilon || fabs(_14 - other._14) > Math::Constants::Epsilon ||
				fabs(_21 - other._21) > Math::Constants::Epsilon || fabs(_22 - other._22) > Math::Constants::Epsilon || fabs(_23 - other._23) > Math::Constants::Epsilon || fabs(_24 - other._24) > Math::Constants::Epsilon ||
				fabs(_31 - other._31) > Math::Constants::Epsilon || fabs(_32 - other._32) > Math::Constants::Epsilon || fabs(_33 - other._33) > Math::Constants::Epsilon || fabs(_34 - other._34) > Math::Constants::Epsilon ||
				fabs(_41 - other._41) > Math::Constants::Epsilon || fabs(_42 - other._42) > Math::Constants::Epsilon || fabs(_43 - other._43) > Math::Constants::Epsilon || fabs(_44 - other._44) > Math::Constants::Epsilon

				);
		}

		// Returns the Identity matrix
		static Matrix4 Identity()
		{
			return Matrix4(
				(1.0f), (0.0f), (0.0f), (0.0f),
				(0.0f), (1.0f), (0.0f), (0.0f),
				(0.0f), (0.0f), (1.0f), (0.0f),
				(0.0f), (0.0f), (0.0f), (1.0f));
		}

		// Returns the transpose of this* matrix
		 Matrix4 Transpose()const 
		{
			return Matrix4(
				(_11), (_21), (_31), (_41),
				(_12), (_22), (_32), (_42),
				(_13), (_23), (_33), (_43),
				(_14), (_24), (_34), (_44)
			);
		}

		 static Matrix4 Transpose(const Matrix4& m)
		 {
			 return Matrix4(
				 (m._11), (m._21), (m._31), (m._41),
				 (m._12), (m._22), (m._32), (m._42),
				 (m._13), (m._23), (m._33), (m._43),
				 (m._14), (m._24), (m._34), (m._44)
			 );
		 }

		// Returns the determinate of this* matrix
		float Determinate() const
		{
			return (

				_11*(_22*_33*_44 + _23 * _34*_42 + _24 * _32*_43 - _24 * _33*_42 - _23 * _32*_44 - _22 * _34*_43)

				- _21 * (_12*_33*_44 + _13 * _34*_42 + _14 * _32*_43 - _14 * _33*_42 - _13 * _32*_44 - _12 * _34*_43)

				+ _31 * (_12*_23*_44 + _13 * _24*_42 + _14 * _22*_43 - _14 * _23*_42 - _13 * _22*_44 - _12 * _24*_43)

				- _41 * (_12*_23*_34 + _13 * _24*_32 + _14 * _22*_33 - _14 * _23*_32 - _13 * _22*_34 - _12 * _24*_33)

				);
		}

		// Returns the adjugate of this* matrix
		Matrix4 Adjugate() const
		{
			// https://semath.info/src/inverse-cofactor-ex4.html
			return Matrix4(

				(_22*_33*_44 + _23 * _34*_42 + _24 * _32*_43 - _24 * _33*_42 - _23 * _32*_44 - _22 * _34*_43), (-_12 * _33*_44 - _13 * _34*_42 - _14 * _32*_43 + _14 * _33*_42 + _13 * _32*_44 + _12 * _34*_43), (_12*_23*_44 + _13 * _24*_42 + _14 * _22*_43 - _14 * _23*_42 - _13 * _22*_44 - _12 * _24*_43), (-_12 * _23*_34 - _13 * _24*_32 - _14 * _22*_33 + _14 * _23*_32 + _13 * _22*_34 + _12 * _24*_33),

				(-_21 * _33*_44 - _23 * _34*_41 - _24 * _31*_43 + _24 * _33*_41 + _23 * _31*_44 + _21 * _34*_43), (_11*_33*_44 + _13 * _34*_41 + _14 * _31*_43 - _14 * _33*_41 - _13 * _31*_44 - _11 * _34*_43), (-_11 * _23*_44 - _13 * _24*_41 - _14 * _21*_43 + _14 * _23*_41 + _13 * _21*_44 + _11 * _24*_43), (_11*_23*_34 + _13 * _24*_31 + _14 * _21*_33 - _14 * _23*_31 - _13 * _21*_34 - _11 * _24*_33),

				(_21*_32*_44 + _22 * _34*_41 + _24 * _31*_42 - _24 * _32*_41 - _22 * _31*_44 - _21 * _34*_42), (-_11 * _32*_44 - _12 * _34*_41 - _14 * _31*_42 + _14 * _32*_41 + _12 * _31*_44 + _11 * _34*_42), (_11*_22*_44 + _12 * _24*_41 + _14 * _21*_42 - _14 * _22*_41 - _12 * _21*_44 - _11 * _24*_42), (-_11 * _22*_34 - _12 * _24*_31 - _14 * _21*_32 + _14 * _22*_31 + _12 * _21*_34 + _11 * _24*_32),

				(-_21 * _32*_43 - _22 * _33*_41 - _23 * _31*_42 + _23 * _32*_41 + _22 * _31*_43 + _21 * _33*_42), (_11*_32*_43 + _12 * _33*_41 + _13 * _31*_42 - _13 * _32*_41 - _12 * _31*_43 - _11 * _33*_42), (-_11 * _22*_43 - _12 * _23*_41 - _13 * _21*_42 + _13 * _22*_41 + _12 * _21*_43 + _11 * _23*_42), (_11*_22*_33 + _12 * _23*_31 + _13 * _21*_32 - _13 * _22*_31 - _12 * _21*_33 - _11 * _23*_32)

			);
		}

		// Returns the inverse of this* matrix
		Matrix4 Inverse() const
		{
			float factor = 1 / Determinate();

			Matrix4 inverse = Adjugate();

			inverse *= factor;

			return  inverse;
		}

		constexpr void Scale(const Vector3& v)
		{
			_11 *= v.x;
			_22 *= v.y;
			_33 *= v.z;
		}
		constexpr void Scale(float f)
		{
			_11 += f;
			_22 += f;
			_33 += f;
		}
		constexpr void ScaleX(float s)
		{
			_11 *= s;
		}
		constexpr void ScaleY(float s)
		{
			_22 *= s;
		}
		constexpr void ScaleZ(float s)
		{
			_33 *= s;
		}

		void Rotate(const Vector3& v)
		{
			RotateX(v.x);
			RotateY(v.y);
			RotateZ(v.z);
		}
		void Rotate(float f)
		{
			RotateX(f);
			RotateY(f);
			RotateZ(f);
		}
		void RotateX(float s)
		{
			_22 = cosf(s);		_32 = -sinf(s);
			_23 = sinf(s);		_33 = cosf(s);
		}
		void RotateY(float s)
		{
			_11 = cosf(s);		_31 =  sinf(s);
			_13 = -sinf(s);		_33 = cosf(s);
		}
		void RotateZ(float s)
		{
			_11 = cosf(s);		_21 = -sinf(s);
			_12 = sinf(s);		_22 = cosf(s);
		}

		constexpr void Translate(const Vector3& v)
		{
			_41 += v.x;
			_42 += v.y;
			_43 += v.z;
		}
		constexpr void Translate(float f)
		{
			_41 += f;
			_42 += f;
			_43 += f;
		}
		constexpr void TranslateX(float f)
		{
			_41 = f;
		}
		constexpr void TranslateY(float f)
		{
			_42 = f;
		}
		constexpr void TranslateZ(float f)
		{
			_43 = f;
		}

		//translate operator overload
		constexpr void operator++()
		{
			Translate(1.0f);
		}
		constexpr void operator--()
		{
			Translate(-1.0f);
		}

		static Matrix4 ScaleMatrix(const Vector3& v)
		{
			Matrix4 m(Identity());
			m.Scale(v);
			return m;
		}
		static Matrix4 RotateMatrixX(float a)
		{
			Matrix4 m(Identity());
			m.RotateX(a);
			return m;
		}
		static Matrix4 RotateMatrixY(float a)
		{
			Matrix4 m(Identity());
			m.RotateY(a);
			return m;
		}
		static Matrix4 RotateMatrixZ(float a)
		{
			Matrix4 m(Identity());
			m.RotateZ(a);
			return m;
		}

		static Matrix4 TranslateMatrix(const Vector3& v)
		{
			Matrix4 m(Identity());
			m.Translate(v);
			return m;
		}

	};
}
}

#endif