#ifndef INCLUDED_DEKU_MATH_VECTOR2_H
#define INCLUDED_DEKU_MATH_VECTOR2_H

namespace Deku {
	namespace Math {

		struct Vector2
		{
			union
			{
				struct { float x, y; };
				std::array<float, 2> v;
				// std::array does bound checking for you, safer version of static array
				// no downside.
			};

			constexpr Vector2() noexcept : Vector2(0.0f) {}
			constexpr Vector2(float f) noexcept : x(f), y(f) {}
			Vector2(float x, float y) noexcept : x(x), y(y) {}


			bool operator==(Vector2 other)
			{
				return (x == other.x && y == other.y);
			}
			Vector2  operator-() const { return { -x,-y }; }
			Vector2  operator+(const Vector2& v) const { return { x + v.x, y + v.y }; }
			Vector2  operator*(float f) const { return { x * f, y * f }; }
			Vector2  operator-(const Vector2& v) const { return { x - v.x, y - v.y }; }
			Vector2  operator/(float f) const { DEKUASSERT((f != 0.0f), "[Deku::Math::Vector2] Cannot divide by zero!"); float divisor = 1 / f; return { x * divisor,y * divisor }; }
			// - ,*,  /

			Vector2& operator+=(const Vector2& v)
			{
				x += v.x;
				y += v.y;
				
				return *this;
			}

			Vector2& operator-=(const Vector2& v)
			{
				x -= v.x;
				y -= v.y;
				
				return *this;
			}

			Vector2& operator*=(float f)
			{
				x *= f;
				y *= f;
				
				return *this;
			}
			Vector2& operator/=(float f)
			{
				DEKUASSERT(f != 0, "[Deku::Math::Vector2] Cannot divide by zero");
				float divisor = 1 / f;
				
				*this *= divisor;
				
				return *this;
			}
			// -=, *=. /=



			static Vector2 Zero() { return Vector2(); }

		};

	} //namespace math
} // namespace Deku

#endif //ifndef INCLUDED_DEKU_MATH_Vector2_H
