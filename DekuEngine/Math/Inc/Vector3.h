#ifndef INCLUDED_DEKU_MATH_VECTOR3_H
#define INCLUDED_DEKU_MATH_VECTOR3_H

namespace Deku {
namespace Math {

struct Vector3
{
	union
	{
		struct { float x, y, z; };
		std::array<float, 3> v;
		// std::array does bound checking for you, safer version of static array
		// no downside.
	};

	constexpr Vector3() noexcept : Vector3(0.0f) {}
	constexpr Vector3(float f) noexcept : x(f), y(f), z(f) {}
	constexpr Vector3(float x, float y, float z) noexcept : x(x), y(y), z(z) {}

	Vector3  operator-() const { return{ -x,-y,-z }; }
	Vector3  operator+(const Vector3& v) const { return { x + v.x,y + v.y,z + v.z }; }
	Vector3  operator*(float f) const { return { x * f, y * f,z * f }; }
	Vector3  operator-(const Vector3& v) const { return { x - v.x, y - v.y, z - v.z }; }
	Vector3  operator/(float f) const { DEKUASSERT((f != 0.0f), "[Deku::Math::Vector3] Cannot divide by zero!"); float divisor = 1 / f; return { x * divisor,y * divisor ,z * divisor };
	}
	// - ,*,  /

	Vector3& operator+=(const Vector3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	Vector3& operator-=(const Vector3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	Vector3& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}
	Vector3& operator/=(float f)
	{
		DEKUASSERT(f != 0, "[Deku::Math::Vector3] Cannot divide by zero");
		float divisor = 1 / f;

		*this *= divisor;

		return *this;
	}
	// -=, *=. /=

	static Vector3 Zero() { return Vector3(); }





};

} //namespace math
} // namespace Deku

#endif //ifndef INCLUDED_DEKU_MATH_VECTOR3_H
