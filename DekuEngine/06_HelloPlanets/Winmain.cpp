#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>



using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Math;




int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{

	// setup our application window
	Deku::Core::Window myWindow;
	myWindow.Initialize(hInstance, "Planets", 1280, 720);
	std::vector<uint32_t> indices;
	//initialize the graphics system
	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);
	SimpleDraw::StaticInitialize();



	MeshPX mesh;
	MeshBuffer earthBuffer;
	VertexShader earthVs;
	PixelShader earthPs;
	ConstantBuffer constantEarthBuffer;
	Texture earth{};
	Sampler earthSampler;

	MeshPX skyBox;
	MeshBuffer skyBoxBuffer;
	VertexShader skyboxVs;
	PixelShader skyboxPs;
	ConstantBuffer constantSkyboxBuffer;
	Texture skyBoxTexture;
	Sampler skyboxSampler;

	MeshPX moon;
	MeshBuffer moonBuffer;
	Texture moonTexture;
	ConstantBuffer constantMoonBuffer;

	MeshPX sun;
	MeshBuffer sunBuffer;
	Texture sunTexture;
	ConstantBuffer constantSunBuffer;

	MeshPX mercury;
	MeshBuffer mercuryBuffer;
	Texture mercuryTexture;
	ConstantBuffer constantMercuryBuffer;

	MeshPX venus;
	MeshBuffer venusBuffer;
	Texture venusTexture;
	ConstantBuffer constantVenusBuffer;


	MeshPX mars;
	MeshBuffer marsBuffer;
	Texture marsTexture;
	ConstantBuffer constantmarsBuffer;

	Camera camera;
	float speed = 1.0f;
	camera.SetPosition({ 0.0f,0.0f,-400.0f });
	camera.SetDirection({ 0.0f,0.0f,1.0f });
	camera.SetFarPlane(6000.0f);

	//mesh = MeshBuilder::CreateSphereUV(30, 30, 15);
	//earth.Initialize(L"../Assets/Images/earth.jpg");
	//earthBuffer.Initialize(mesh.mVertices.data(), (int)sizeof(VertexPX), mesh.mVertices.size(), mesh.mIndices.data(), mesh.mIndices.size());
	//earthVs.Initialize("../Assets/Shaders/Texturing.fx", VertexPX::Format);
	//earthPs.Initialize("../Assets/Shaders/Texturing.fx");
	//earthSampler.Initialize(Sampler::Filter::Linear, Sampler::AddressMode::Clamp);
	//constantEarthBuffer.Initialize(sizeof(Matrix4), &earthVs);
	//earthBuffer.SetTopology(MeshBuffer::Topology::Triangles);


	
	skyBox = MeshBuilder::CreateSkyBoxUV(3000, 3000, 3000);

	skyBoxBuffer.SetTopology(MeshBuffer::Topology::Triangles);
	skyBoxBuffer.Initialize(skyBox.mVertices.data(), (int)sizeof(VertexPX), skyBox.mVertices.size(), skyBox.mIndices.data(), skyBox.mIndices.size());
	skyboxSampler.Initialize(Sampler::Filter::Anisotropic, Sampler::AddressMode::Border);
	skyBoxTexture.Initialize(L"../Assets/Images/space3.png");
 	skyboxVs.Initialize("../Assets/Shaders/Texturing.fx", VertexPX::Format);
	skyboxPs.Initialize("../Assets/Shaders/Texturing.fx");
	constantSkyboxBuffer.Initialize(sizeof(Math::Matrix4));


	//moon = MeshBuilder::CreateSphereUV(14, 14, 7);
	//moonBuffer.Initialize(moon.mVertices.data(), static_cast<int>(sizeof(VertexPX)), moon.mVertices.size(), moon.mIndices.data(), moon.mIndices.size());
	//moonTexture.Initialize(L"../Assets/images/moon1.jpg");
	//constantMoonBuffer.Initialize(16, nullptr);
	//
	//sun = MeshBuilder::CreateSphereUV(50, 50, 25);
	//sunBuffer.Initialize(sun.mVertices.data(), (int)sizeof(VertexPX), sun.mVertices.size(), sun.mIndices.data(), sun.mIndices.size());
	//sunTexture.Initialize(L"../Assets/images/sun1.jpg");
	//constantSunBuffer.Initialize(16, nullptr);
	//
	//mercury = MeshBuilder::CreateSphereUV(20, 20, 10);
	//mercuryBuffer.Initialize(mercury.mVertices.data(), static_cast<int>(sizeof(VertexPX)), mercury.mVertices.size(), mercury.mIndices.data(), mercury.mIndices.size());
	//mercuryTexture.Initialize(L"../Assets/images/mercury1.jpg");
	//constantMercuryBuffer.Initialize(16, nullptr);
	//
	//venus = MeshBuilder::CreateSphereUV(25, 25, 14);
	//venusBuffer.Initialize(venus.mVertices.data(), static_cast<int>(sizeof(VertexPX)), venus.mVertices.size(), venus.mIndices.data(), venus.mIndices.size());
	//venusTexture.Initialize(L"../Assets/images/venus2.jpg");
	//constantVenusBuffer.Initialize(16, nullptr);
	//
	//mars = MeshBuilder::CreateSphereUV(30, 30, 14);
	//marsBuffer.Initialize(mars.mVertices.data(), static_cast<int>(sizeof(VertexPX)), mars.mVertices.size(), mars.mIndices.data(), mars.mIndices.size());
	//marsTexture.Initialize(L"../Assets/images/mars1.jpg");
	//constantmarsBuffer.Initialize(16, nullptr);

	float thetaX = 0.0f;
	float thetaY = 0.0f;
	float thetaZ = 0.0f;
	bool done = false;
	while (!done)
	{
		// all use one constant buffer, pixelshader, vertexshader
		// matrix multiply to get specific behaviours

		// ie Re, RmTm
		//
		Matrix4 sunRoty = Matrix4::RotateMatrixY(thetaY * 0.5 * Constants::DegToRad);

		//mercury
		Matrix4 mercuryRoty = Matrix4::RotateMatrixY(thetaY * 2 * Constants::DegToRad);
		Matrix4 mercuryTranslate = Matrix4::TranslateMatrix({ 100,20,0 });

		//venus
		Matrix4 venusRoty = Matrix4::RotateMatrixY(thetaY * 1.5 * Constants::DegToRad);
		Matrix4 venusTranslate = Matrix4::TranslateMatrix({ 50,-10,0 }) * mercuryTranslate;


		// earth rotMatrix
		Matrix4 earthRotY = Matrix4::RotateMatrixY(thetaY * Constants::DegToRad);
		Matrix4 earthTranslate = Matrix4::TranslateMatrix({ 50,30,0 }) * venusTranslate;


		//mars
		Matrix4 marsRoty = Matrix4::RotateMatrixY(thetaY * 0.75 * Constants::DegToRad);
		Matrix4 marsTranslate = Matrix4::TranslateMatrix({ 50,-20,0 }) * earthTranslate;

		//Moon rotation & translation, multiply by moon identity
		Matrix4 moonRoty = Matrix4::RotateMatrixY(thetaY * Constants::DegToRad);
		Matrix4 moonTranslate = Matrix4::TranslateMatrix({ 30,0,0 });



		Matrix4 matRotx = Matrix4::RotateMatrixX(180.0f * Constants::DegToRad);
		Matrix4 matRoty = Matrix4::RotateMatrixY(thetaY * Constants::DegToRad);
		Matrix4 matRotz = Matrix4::RotateMatrixZ(0.0f);

		;
		done = myWindow.ProcessMessage();
		done = GetAsyncKeyState(VK_ESCAPE);
		//Run Game Logic


		//camera

		if (GetAsyncKeyState(0x57))
		{
			camera.Walk(speed);
		}
		else if (GetAsyncKeyState(0x53))
		{
			camera.Walk(-speed);
		}
		if (GetAsyncKeyState(0x41))
		{
			camera.Strafe(-speed);
		}
		else if (GetAsyncKeyState(0x44))
		{
			camera.Strafe(speed);
		}
		if (GetAsyncKeyState(0x46))
		{
			camera.Yaw(speed* 0.005);
		}
		else if (GetAsyncKeyState(0x52))
		{
			camera.Yaw(-speed * 0.005);
		}
		else if (GetAsyncKeyState(VK_SHIFT))
		{
			camera.Rise(-speed);
		}
		else if (GetAsyncKeyState(VK_SPACE))
		{
			camera.Rise(speed);
		}



		if (GetAsyncKeyState(VK_UP))
		{
			thetaX += 0.5f;
		}
		else
			if (GetAsyncKeyState(VK_DOWN))
			{
				thetaX -= 0.5f;
			}
		if (GetAsyncKeyState(VK_RIGHT))
		{
			thetaY += 0.5f;
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			thetaY -= 0.5f;
		}
		GraphicsSystem::Get()->BeginRender();

		//before draw
		Matrix4 matrices[3];
		/// Note constant buffer takes the matrices transposed!
		matrices[0] = matRotx * earthRotY  * earthTranslate * earthRotY;
		matrices[0] = matrices[0].Transpose();
		matrices[1] = camera.GetViewMatrix().Transpose();
		matrices[2] = camera.GetPerspectiveMatrix(1280.0f / 720.0f).Transpose();

		//constantEarthBuffer.Bind(matrices);
		//earth.BindPS(0);
		//earthVs.Bind();
		//earthPs.Bind();
		//earthBuffer.Render();


		skyBoxTexture.BindPS(0);
		matrices[0] = Matrix4::Identity().Transpose();
		skyboxVs.Bind();
		skyboxPs.Bind();
		constantSkyboxBuffer.Bind(matrices);
		//
		skyBoxBuffer.Render();
		//
		////moonTexture.BindPS(0);
		//matrices[0] = (moonRoty * moonTranslate * earthTranslate * moonRoty).Transpose();
		//constantMoonBuffer.Bind(matrices);
		//
		//moonBuffer.Render();
		//
		//sunTexture.BindPS(0);
		//matrices[0] = sunRoty.Transpose();
		//constantSunBuffer.Bind(matrices);
		//sunBuffer.Render();
		//
		//mercuryTexture.BindPS(0);
		//matrices[0] = (mercuryRoty * mercuryTranslate * mercuryRoty).Transpose();
		//constantMercuryBuffer.Bind(matrices);
		//mercuryBuffer.Render();
		//
		//venusTexture.BindPS(0);
		//matrices[0] = (venusRoty * venusTranslate * venusRoty).Transpose();
		//constantVenusBuffer.Bind(matrices);
		//venusBuffer.Render();
		//
		//marsTexture.BindPS(0);
		//matrices[0] = (marsRoty * marsTranslate * marsRoty).Transpose();
		//constantmarsBuffer.Bind(matrices);
		//marsBuffer.Render();
		//
		thetaY += 0.5f;
		
		SimpleDraw::DrawSphere(3.0f, 40.0f, 25, 25, Colors::DarkRed);
		SimpleDraw::Render(camera);

		GraphicsSystem::Get()->EndRender();

	}

	skyBoxTexture.Terminate();
	earth.Terminate();
	earthSampler.Terminate();
	earthVs.Terminate();
	earthPs.Terminate();
	earthBuffer.Terminate();
	constantEarthBuffer.Terminate();

	skyboxSampler.Terminate();
	skyboxVs.Terminate();
	skyboxPs.Terminate();
	skyBoxBuffer.Terminate();
	constantSkyboxBuffer.Terminate();

	moonBuffer.Terminate();
	moonTexture.Terminate();
	constantMoonBuffer.Terminate();

	sunBuffer.Terminate();
	sunTexture.Terminate();
	constantSunBuffer.Terminate();

	mercuryBuffer.Terminate();
	mercuryTexture.Terminate();
	constantMercuryBuffer.Terminate();


	venusBuffer.Terminate();
	venusTexture.Terminate();
	constantVenusBuffer.Terminate();

	marsBuffer.Terminate();
	marsTexture.Terminate();
	constantmarsBuffer.Terminate();

	SimpleDraw::StaticTerminate();
	GraphicsSystem::StaticTerminate();
	myWindow.Terminate();
	return 0;
}


