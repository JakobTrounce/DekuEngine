#include <AI.h>
#include "Map.h"
#include <ImGui/Inc/Imgui.h>
AI::AIWorld world;
X::TextureId tester;
AI::Agent test(world);
AI::SteeringModule str(test);
X::Math::Vector2 forces = {0.0f,0.0f};

std::vector<AI::Agent> flockAgents;




enum class behaviourTypes
{

};


int rows = 25;
int columns =25;
X::Math::Vector2 startDrawPos{600,200};
void Initialize()
{
	CameraGame::StaticInitialize();
	tester = X::LoadTexture("stone.png");
	test.MaxSpeed() = 400;
	world.GetNavGraph().Init(columns,rows);
	str.AddBehaviour<AI::SeekBehaviour>()->SetActive(true);
	str.AddBehaviour<AI::FleeBehaviour>()->SetActive(false);
	str.AddBehaviour<AI::ObstacleAvoidanceBehaviour>()->SetActive(false);
	str.AddBehaviour<AI::WanderBehaviour>()->SetActive(false);
	str.AddBehaviour<AI::FlockBehaviour>()->SetActive(false);
	str.AddBehaviour<AI::ArriveBehaviour>()->SetActive(false);
	test.Position() = { 0.0f,0.0f };
	for (auto a : flockAgents)
	{

	}

}

void OnGui()
{
	ImGui::Begin("BehaviourTypes");
	ImGui::SetWindowSize({ 300.0f, 100 });
	ImGui::SetWindowPos({ 0,0 });
	//ImGui::SliderInt();
	ImGui::End();
}
void Terminate()
{
	CameraGame::StaticTerminate();
	str.Purge();
}

void SeekArriveDemo(float deltaTime)
{
		test.Destination() = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
		forces += str.Calculate();

		//test.Velocity().x = trunc(test.Velocity().x + forces.x * test.MaxSpeed());
		//test.Velocity().y = trunc(test.Velocity().y + forces.y * test.MaxSpeed());
		test.Velocity() = forces;
		test.Position() += test.Velocity()*deltaTime;
}

void ObstacleAvoidanceWanderDemo(float deltaTime)
{
	test.Destination() = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
	//if (X::IsMousePressed(X::Mouse::LBUTTON))
	//{
	//	X::Math::Circle obstacle
	//	{
	//		{ (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() },
	//		32.0f
	//	};
	//	test.World().AddObstacles(obstacle);
	//}
	//for (auto o : test.World().GetObstacles())
	//{
	//	X::DrawScreenCircle(o.center, o.radius, X::Math::Vector4::Blue());
	//}
	forces += str.Calculate();
	test.Velocity() = forces;
	test.Position() += test.Velocity()*deltaTime;

}

void FlockDemo(float deltaTime)
{
	
}

bool GameLoop(float deltaTime)
{


	test.Destination() = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
	
	if (X::IsMousePressed(X::Mouse::LBUTTON))
	{
		X::Math::Circle obstacle
		{
			{ (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() },
			32.0f
		};
		test.World().AddObstacles(obstacle);
	}
	for (auto o : test.World().GetObstacles())
	{
		X::DrawScreenCircle(o.center, o.radius, X::Math::Vector4::Blue());
	}
	
	
	if (X::IsKeyPressed(X::Keys::S))
	{
		str.GetBehaviour<AI::SteeringBehaviour>("Seek")->SetActive(true);
	}

	//test.Velocity().x = trunc(test.Velocity().x + forces.x * test.MaxSpeed());
	//test.Velocity().y = trunc(test.Velocity().y + forces.y * test.MaxSpeed());
	forces += str.Calculate();
	test.Velocity() = forces;
	test.Position() += test.Velocity()*deltaTime;

	X::DrawSprite(tester, test.Position());
	
	return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	X::Start();
	Initialize();
	X::Run(GameLoop);
	Terminate();
	X::Stop();
	return 0;

}

