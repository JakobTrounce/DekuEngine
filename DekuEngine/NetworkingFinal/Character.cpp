#include "Character.h"
#include "TripleShot.h"
#include "Skill.h"

void Character::Update(float deltaTime)
{
	Move(deltaTime);
	mBulletSpawnPos = mPosition;
	if (X::IsMouseDown(X::Mouse::LBUTTON) && !IsSkillActive() && CanAttack())
	{
		Attack();
		mAtkTimer = mAtkDelay;
	}
	else if (X::IsKeyPressed(X::Keys::ONE))
	{
		UseSkill(1);
	}
	else if (X::IsKeyPressed(X::Keys::TWO))
	{
		UseSkill(2);
	}
	else if (X::IsKeyPressed(X::Keys::THREE))
	{
		UseSkill(3);
	}
	mAtkTimer -= deltaTime;
}

//-----------------------------------------

void Character::Render()
{
	X::DrawSprite(mTexture, mPosition);
}


//-----------------------------------------

void Character::UseSkill(int skill)
{
	switch (skill)
	{
	case 1: mSkills[0]->Activate();
		break;
	case 2: mSkills[1]->Activate();
		break;	
	case 3: mSkills[2]->Activate();
		break;			
	}
}


//------------------------------------------

void Character::Move(float deltaTime)
{
	X::Math::Vector2 velocity{0.0f,0.0f};
	if (X::IsKeyDown(X::Keys::A))
	{
		velocity.x -= mMoveSpeed * deltaTime;
	}
	if (X::IsKeyDown(X::Keys::D))
	{
		velocity.x += mMoveSpeed * deltaTime;
	}
	if (X::IsKeyDown(X::Keys::W))
	{
		velocity.y -= mMoveSpeed * deltaTime;
	}
	if (X::IsKeyDown(X::Keys::S))
	{
		velocity.y += mMoveSpeed * deltaTime;
	}

	mPosition += velocity;
}

//-------------------------------------------

void Character::Attack()
{
	X::Math::Vector2 mousePos = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
	X::Math::Vector2 direction = X::Math::Normalize( mousePos - mPosition);

	ProjectileManager::Get()->Fire(mBulletSpawnPos, direction);
} 

//-------------------------------------------

void Character::Load()
{
	
	mTexture = X::LoadTexture(mTextureName.c_str());
	mSkills.push_back(&mShot);
	mAtkDelay = 0.1f;
}

//--------------------------------------------

void Character::Unload()
{
	mTexture = 0;
}