#include "ProjectileManager.h"

namespace
{
	ProjectileManager* sInstance = nullptr;
}

void ProjectileManager::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[ProjectileManager] Singleton already initialized.");
	sInstance = new ProjectileManager();
}

void ProjectileManager::StaticTerminate()
{
	X::SafeDelete(sInstance);
}

ProjectileManager* ProjectileManager::Get()
{
	XASSERT(sInstance != nullptr, "[ProjectileManager] Singleton not initialized.");
	return sInstance;
}

ProjectileManager::ProjectileManager()
{
}

ProjectileManager::~ProjectileManager()
{
}

void ProjectileManager::Load()
{
	for (auto& Projectile : mProjectiles)
	{
		Projectile.Load();
	}
}

void ProjectileManager::Unload()
{
	for (auto& Projectile : mProjectiles)
	{
		Projectile.Unload();
	}
}

void ProjectileManager::Update(float deltaTime)
{
	for (auto& Projectile : mProjectiles)
	{
		Projectile.Update(deltaTime);
	}
}

void ProjectileManager::Render()
{
	for (auto& Projectile : mProjectiles)
	{
		Projectile.Render();
		auto Circle = Projectile.GetBoundingCircle();
	}
}

void ProjectileManager::Fire(const X::Math::Vector2& pos, const X::Math::Vector2& vel)
{
	mProjectiles[mProjectileIndex].Fire(pos, vel);
	mProjectileIndex = (mProjectileIndex + 1) % kProjectileCount;
}


Projectile ProjectileManager::GetProjectile(int index)
{
	return mProjectiles[index];
}