#include "TripleShot.h"
#include "ProjectileManager.h"
#include "GameManager.h"

void TripleShot::Activate()
{
	X::Math::Vector2 mousePos = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
	X::Math::Vector2 direction = X::Math::Normalize(mousePos - mPlayerPos);

	X::Math::Vector2 posOffset = { 25.0f,0.0f };
	DirOffset = { 25.0f,0 };
	DirOffset = X::Math::Normalize(DirOffset);
	mPlayerPos = GameManager::Get()->PlayerPos();

	ProjectileManager::Get()->Fire((mPlayerPos - posOffset), direction - DirOffset);
	ProjectileManager::Get()->Fire((mPlayerPos), direction);
	ProjectileManager::Get()->Fire((mPlayerPos+posOffset), direction + DirOffset);
}