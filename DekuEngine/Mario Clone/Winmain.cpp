#include <Deku/Inc/Deku.h>

using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Input;
using namespace Deku::Math;
using namespace Deku::Physics;


void Init();
void Terminate();

Deku::Core::Window myWindow;
Deku::Graphics::Texture player;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	
	static auto lastTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();

	float speed = 25.0f;
	Vector3 playerPosition = { 500.0f,500.0f,0.0f };
	// Initialize window
	
	myWindow.Initialize(hInstance, "Hello Cube", 1280, 720);
	std::vector<uint32_t> indices;
	//initialize the graphics system
	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);
	Init();
	bool done = false;
	while (!done)
	{
		currentTime = std::chrono::high_resolution_clock::now();
		float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastTime).count() / 1000.0f;
		done = myWindow.ProcessMessage();

		auto inputSystem = InputSystem::Get();
		inputSystem->Update();

		
		
		

		if (inputSystem->IsKeyPressed(KeyCode::ESCAPE))
		{
			done = true;
		}

		if(inputSystem->IsKeyDown(KeyCode::A))
		{
			playerPosition.x += -speed * deltaTime;
		}
		else if (inputSystem->IsKeyDown(KeyCode::D))
		{
			playerPosition.x += speed * deltaTime;
		}

		playerPosition.y += Physics::Constants::Gravity * deltaTime;

		GraphicsSystem::Get()->BeginRender();
		SpriteRenderer::Get()->BeginRender();
		SpriteRenderer::Get()->Draw(player, {playerPosition});


		DebugUI::BeginRender();

		ImGui::DragFloat("moveSpeed", &speed, 1.0f, 0.0f, 1000.0f);

		DebugUI::EndRender();

		SpriteRenderer::Get()->EndRender();
		GraphicsSystem::Get()->EndRender();
		lastTime = currentTime;
	}
	Terminate();
	
}



void Init()
{
	// init singletons
	SpriteRenderer::StaticInitialize();
	DebugUI::StaticInitialize(myWindow.GetWindowHandle());
	InputSystem::StaticInitialize(myWindow.GetWindowHandle());
	// init variables
	player.Initialize(L"../Assets/Images/Link1.png");
	player.BindPS(0);
}
void Terminate()
{
	SpriteRenderer::StaticTerminate();
	InputSystem::StaticTerminate();
	GraphicsSystem::StaticTerminate();
	DebugUI::StaticTerminate();
	player.Terminate();
}