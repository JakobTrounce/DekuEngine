#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Deku::Math;


namespace MathTest
{
	// macro is doing code injection for you
	TEST_CLASS(Vector2Test)
	{
	public:

		TEST_METHOD(TestConstructor)
		{
			Vector2 v0;
			Assert::AreEqual(v0.x, 0.0f);
			Assert::AreEqual(v0.y, 0.0f);

			Vector2 v1(1.0f);
			Assert::AreEqual(v1.x, 1.0f);
			Assert::AreEqual(v1.y, 1.0f);

			Vector2 v2(1.0f, 2.0f);
			Assert::AreEqual(v2.x, 1.0f);
			Assert::AreEqual(v2.y, 2.0f);
		}

		TEST_METHOD(TestAddition)
		{
			Vector2 v0(1.0f);
			Vector2 v1(2.0f, 3.0f);
			Vector2 v2 = v0 + v1;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);

			Assert::AreEqual(v2.x, 3.0f);
			Assert::AreEqual(v2.y, 4.0f);

		}

		TEST_METHOD(TestSubtraction)
		{
			Vector2 v0(1.0f);
			Vector2 v1(2.0f, 3.0f);
			Vector2 v2 = v0 - v1;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);


			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);

			Assert::AreEqual(v2.x, -1.0f);
			Assert::AreEqual(v2.y, -2.0f);
		}

		TEST_METHOD(TestDivision)
		{
			Vector2 v0(2.0f, 4.0f);
			Vector2 v2 = v0 / 2.0f;

			Assert::AreEqual(v0.x, 2.0f);
			Assert::AreEqual(v0.y, 4.0f);

			Assert::AreEqual(v2.x, 1.0f);
			Assert::AreEqual(v2.y, 2.0f);
		}

		TEST_METHOD(TestMultiplication)
		{
			Vector2 v0(2.0f, 4.0f);
			Vector2 v2 = v0 * 2.0f;

			Assert::AreEqual(v0.x, 2.0f);
			Assert::AreEqual(v0.y, 4.0f);

			Assert::AreEqual(v2.x, 4.0f);
			Assert::AreEqual(v2.y, 8.0f);
		}

		TEST_METHOD(TestAdditionAssignment)
		{
			Vector2 v0(1.0f);
			Vector2 v1(2.0f, 3.0f);
			v0 += v1;

			Assert::AreEqual(v0.x, 3.0f);
			Assert::AreEqual(v0.y, 4.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
		}

		TEST_METHOD(TestSubtractionAssignment)
		{
			Vector2 v0(1.0f);
			Vector2 v1(2.0f, 3.0f);
			v0 -= v1;

			Assert::AreEqual(v0.x, -1.0f);
			Assert::AreEqual(v0.y, -2.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 3.0f);
		}

		TEST_METHOD(TestMultiplicationAssignment)
		{
			Vector2 v0(2.0f, 3.0f);
			float f = 2.0f;
			v0 *= f;

			Assert::AreEqual(v0.x, 4.0f);
			Assert::AreEqual(v0.y, 6.0f);

			Assert::AreEqual(f, 2.0f);
		}

		TEST_METHOD(TestDivisionAssignment)
		{
			Vector2 v0(2.0f, 4.0f);
			float f = 2.0f;
			v0 /= f;

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 2.0f);

			Assert::AreEqual(f, 2.0f);
		}

		//TEST_METHOD(TestCross)
		//{
		//	Vector2 v0(1.0f);
		//	Vector2 v1(2.0f, 5.0f);
		//	 
		//	Assert::AreEqual(v0.x, 1.0f);
		//	Assert::AreEqual(v0.y, 1.0f);
		//	Assert::AreEqual(v0.z, 1.0f);
		//
		//
		//	Assert::AreEqual(v1.x, 2.0f);
		//	Assert::AreEqual(v1.y, 5.0f);
		//	Assert::AreEqual(v1.z, 3.0f);
		//
		//	Assert::AreEqual(v2.x, -2.0f);
		//	Assert::AreEqual(v2.y, -1.0f);
		//	Assert::AreEqual(v2.z, 3.0f);
		//
		//}

		TEST_METHOD(TestDot)
		{
			Vector2 v0(1.0f);
			Vector2 v1(2.0f, 5.0f);
			float f = Dot(v0, v1);

			Assert::AreEqual(v0.x, 1.0f);
			Assert::AreEqual(v0.y, 1.0f);

			Assert::AreEqual(v1.x, 2.0f);
			Assert::AreEqual(v1.y, 5.0f);

			Assert::AreEqual(f, 7.0f);
		}

		TEST_METHOD(TestMagnitude)
		{
			Vector2 v1(3.0f, 4.0f);
			float magnitude = Magnitude(v1);

			Assert::AreEqual(v1.x, 3.0f);
			Assert::AreEqual(v1.y, 4.0f);

			Assert::AreEqual(magnitude, 5.0f);



		}

		TEST_METHOD(TestNormalize)
		{
			Vector2 v0 = (1.0f, 1.0f);
			Vector2 normalized = Normalize(v0);
			float epsilon = 0.0001f;
			float diff = std::fabsf(0.7071f - normalized.x);
			bool isEqual = false;

			if (diff <= epsilon)
			{
				isEqual = true;
			}

			Assert::IsTrue(isEqual);

		}
	};
}