#include "CameraGame.h"

namespace
{
	CameraGame *sInstance = nullptr;
}

void CameraGame::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[CameraManeger] Singleton Already Initialized");
	sInstance = new CameraGame;
}

void CameraGame::StaticTerminate()
{
	X::SafeDelete(sInstance);
}

CameraGame* CameraGame::Get()
{
	XASSERT(sInstance != nullptr,
		"[CameraGame] Not Initialized.");
	return sInstance;
}

CameraGame::CameraGame()
{
}


CameraGame::~CameraGame()
{
}

void CameraGame::SetViewPos(X::Math::Vector2 pos)
{
	pos.x -= 625;
	pos.y -= 345;

	mViewPosition = pos;
}

X::Math::Vector2 CameraGame::ConvertToWorld(X::Math::Vector2 screenPos)
{
	return screenPos + mViewPosition;
}
X::Math::Vector2 CameraGame::ConvertToScreen(X::Math::Vector2 worldPos)
{
	return worldPos - mViewPosition;
}

void CameraGame::Render(X::TextureId id, X::Math::Vector2 worldPos)
{
	X::DrawSprite(id, ConvertToScreen(worldPos));
}
