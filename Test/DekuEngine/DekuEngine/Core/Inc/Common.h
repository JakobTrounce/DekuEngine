#ifndef INCLUDED_DEKUENGINE_CORE_COMMON_H
#define INCLUDED_DEKUENGINE_CORE_COMMON_H

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

// Win32 headers
#include <objbase.h>
#include <Windows.h>

//Standard headers
#include <algorithm>
#include <Array>
#include <chrono>
#include <cstdio>
#include <cstdint>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

template <class T>
inline void SafeRelease(T*& ptr)
{
	if (ptr)
	{
		ptr->Release();
		ptr = nullptr;
	}
}


#endif //#ifndef INCLUDED_DEKUENGINE_CORE_COMMON_H