#ifndef INCLUDED_DEKU_CORE_WindowMessageHandler_H
#define INCLUDED_DEKU_CORE_WindowMessageHandler_H


namespace Deku::Core{

class WindowMessageHandler
{
public:
	using Callback = LRESULT(CALLBACK*)(HWND, UINT, WPARAM, LPARAM);

	void Hook(HWND window, Callback cb);
	void UnHook();

	LRESULT ForwardMessage(HWND window, UINT message, WPARAM wParam, LPARAM lParam);

private:
	HWND mWindow{ nullptr };
	Callback mPreviousCallback{ nullptr };
};

}	//namespace Deku::Core


#endif	//Defined INCLUDED_DEKU_CORE_WindowMessageHandler_H
