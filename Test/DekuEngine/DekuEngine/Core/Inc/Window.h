#ifndef INCLUDED_DEKU_CORE_WINDOW_H
#define INCLUDED_DEKU_CORE_WINDOW_H


namespace Deku {
namespace Core {
class Window
{
public : 
	Window() = default;

	void Initialize(HINSTANCE instance, LPCSTR appName, uint32_t width, uint32_t height);
	void Terminate();

	bool ProcessMessage();

	HWND GetWindowHandle() const { return mWindow; }

private:
	// default member intitialization
	HINSTANCE mInstance{nullptr};
	HWND mWindow{nullptr};
	std::string mAppName;
};
} // namespace Core
} // namespace Deku






#endif // INCLUDED_DEKU_CORE_WINDOW_H