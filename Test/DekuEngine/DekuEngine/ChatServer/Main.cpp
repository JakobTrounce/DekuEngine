#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <cstring>
#include <thread>
#include <vector>
#include <mutex>
#pragma comment(lib,"Ws2_32.lib")

std::mutex recvLock;
std::mutex safetyLock;
const int maxClients = 2;
void RecvClientMsg(SOCKET &client,std::vector<std::string>& queue,int id);
void Dispatcher(std::vector<std::string>& queue, SOCKET (&clients)[maxClients], int& numOfClients,int& Id);

int prevId;
int main(int argc, char* argv[])
{
	//NotShared
	int count = 0;
	uint16_t port = 8888;
	std::thread clientThreads[maxClients];
	std::thread dispatcher;
	//shared between threads
	std::vector<std::string> messageQueue;
	SOCKET clients[maxClients];


	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);



	// create a socket using TCP/IP
	SOCKET listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//have to bind socket to an address

	// fill server address info
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET; // address family or format

	// the address format tells these how to read the ip
	serverInfo.sin_addr.s_addr = INADDR_ANY; // check for any ip
	serverInfo.sin_port = htons(port); //  connection port

	printf("Listening on port %hu...\n", port);

	//bind the address to our listener socket and listen for connection
	bind(listener, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));
	listen(listener, maxClients); //  create how many connections the listener can queue up until they start getting dropped
	// server forever...
	dispatcher = std::thread(Dispatcher, std::ref(messageQueue), std::ref(clients), std::ref(count),std::ref(prevId));
	while (true)
	{
		//accept any incoming client connection **blocking**
		if (count < maxClients)
		{
			clients[count] = accept(listener, NULL, NULL);
			if (clients[count] == WSAEINVAL)
			{
				std::cout << "named Socket not listening" << std::endl;
			}
			else
			{
				clientThreads[count] = std::thread(RecvClientMsg, std::ref(clients[count]), std::ref(messageQueue), count);
				count++;
			}
		}
	}
	//close all sockets
	for (int i = 0; i < maxClients; ++i)
	{
		clientThreads[i].join();
	}
	dispatcher.join();
	closesocket(listener);

	//Shutdown Winsock
	WSACleanup();
	return 0;
}

void RecvClientMsg(SOCKET &client,std::vector<std::string>& queue,int id)
{
	int bytesReceived = 0;
	char buffer[1024];
	while (true)
	{
			bytesReceived = recv(client, buffer, std::size(buffer), 0);
			if (bytesReceived == SOCKET_ERROR)
			{
				printf("recv failed.\n");
				break;
			}
			else
			{
				buffer[bytesReceived] = '\0';
				printf("Received: %s\n", buffer);
			}
			{
				std::lock_guard<std::mutex> lock(safetyLock);
				std::string message(buffer);
				queue.push_back(message);
				prevId = id;
			}
			

	}
		closesocket(client);
}

void Dispatcher(std::vector<std::string>& queue,SOCKET (&clients)[maxClients],int& numOfClients,int& Id)
{

	int bytesSent = 0;
	while (true)
	{
		safetyLock.lock();
		if (!queue.empty())
		{
				for (int i = 0; i < numOfClients; ++i)
				{					
					if (i != Id)
					{
						bytesSent = send(clients[i], queue.back().c_str(), (int)queue.back().length(), 0);
					}
				}
				queue.pop_back();
		}
		safetyLock.unlock();
		Sleep(100);
	}
}