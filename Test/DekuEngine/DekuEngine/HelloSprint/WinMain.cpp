#include <XEngine.h>
#include <Network.h>

struct Runner
{
    int frameId;
    float position;
};

Network::TCPSocket listenSocket;
Network::TCPSocket* otherSocket;
Network::SocketAddress clientAddress;

bool isHost = false;
X::TextureId textureIds[4];
X::TextureId bg;
std::vector<Runner> runners;
std::mutex mutex;

std::thread sendThread;
std::thread recvThread;

void GameInit()
{
    Network::Initialize();
	bg = X::LoadTexture("Field.png");
    textureIds[0] = X::LoadTexture("mario_walk_01.png");
    textureIds[1] = X::LoadTexture("mario_walk_02.png");

    textureIds[2] = X::LoadTexture("LinkRun.png");
    textureIds[3] = X::LoadTexture("LinkRun2.png");


    runners.push_back(Runner());
    runners.back().frameId = 0;
    runners.back().position = 100.0f;

    runners.push_back(Runner());
    runners.back().frameId = 0;
    runners.back().position = 100.0f;

}

void GameCleanUp()
{
    Network::Terminate();
}

void UpdatePlayer()
{
	X::DrawSprite(bg, { 1280 * 0.5,720 * 0.5 });
    Runner& runner = isHost ? runners[0] : runners[1];

    if (runner.frameId == 0)
    {
        if (X::IsKeyPressed(X::Keys::RIGHT))
        {
            runner.frameId = 1;
            runner.position += 2.0f;

            {
                std::lock_guard<std::mutex> lock(mutex);
                otherSocket->Send(&runner, sizeof(Runner));
            }
        }
    }
    else
    {
        if (X::IsKeyPressed(X::Keys::LEFT))
        {
            runner.frameId = 0;
            runner.position += 2.0f;

            {
                std::lock_guard<std::mutex> lock(mutex);
                otherSocket->Send(&runner, sizeof(Runner));
            }

        }
    }
}

void Host()
{
    isHost = true;
    Network::SocketAddress addr(8888);
    listenSocket.Bind(addr);
    listenSocket.Listen(10);

    otherSocket = listenSocket.Accept(clientAddress);
    recvThread = std::thread([]()
    {
        while (true)
        {
            char buffer[1024];
            int bytesReceived = otherSocket->Receive(buffer, sizeof(buffer));
            if (bytesReceived > 0 && bytesReceived == sizeof(Runner))
            {
                std::lock_guard<std::mutex> lock(mutex);
                memcpy_s(&runners[1], sizeof(Runner), buffer, bytesReceived);
                // update the runner
            }
        }
    });
}

void Join(const char* hostip)
{
    otherSocket = new Network::TCPSocket();
    listenSocket.Connect(Network::SocketAddress(hostip, 8888));

    recvThread = std::thread([]()
    {
        while (true)
        {
            char buffer[1024];
            int bytesReceived = otherSocket->Receive(buffer, sizeof(buffer));
            if (bytesReceived > 0 && bytesReceived == sizeof(Runner))
            {
                std::lock_guard<std::mutex> lock(mutex);
                memcpy_s(&runners[0], sizeof(Runner), buffer, bytesReceived);
                // update the runner
            }
        }
    });
}

bool GameLoop(float deltaTime)
{
    UpdatePlayer();

    for (size_t i = 0; i < runners.size(); i++)
    {
        Runner runner = runners[i];
        X::DrawSprite(textureIds[runner.frameId + (i*2)], { runner.position, 200.0f + i *(200.0f) });
    }


    return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR cmd, int)
{
    X::Start("xconfig.json");

    GameInit();
	if (cmd != nullptr)
	{
		Join(cmd);
	}
	else
	{
		Host();
	}

    X::Run(GameLoop);
    GameCleanUp();
    X::Stop();
    
    return 0;
}