#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <cstring>
#include <strsafe.h>
#include <sstream>
#include <fstream>
#include <tchar.h>
#include <vector>
#pragma comment(lib,"Ws2_32.lib")
#pragma comment(lib,"User32.lib")


int main(int argc, char* argv[])
{
	int bytesSent;
	std::vector<std::string> fileNames;
	FILE* file;
	WIN32_FIND_DATA fileDirData;
	LARGE_INTEGER fileSize;
	TCHAR Dir[MAX_PATH];
	size_t lengthOfArg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	uint16_t port = 8888;

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	//socket under the hood is just an ID
	SOCKET listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//have to bind socket to an address

	// fill server address info
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET; // address family or format

	// the address format tells these how to read the ip
	serverInfo.sin_addr.s_addr = INADDR_ANY; // check for any ip
	serverInfo.sin_port = htons(port); //  connection port

	printf("Listening on port %hu...\n", port);

	//bind the address to our listener socket and listen for connection
	bind(listener, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));
	listen(listener, 10); //  create how many connections the listener can queue up until they start getting dropped

	char buffer[1024];
	char* fileBuffer;
	std::string response;
	std::string temp;
	std::wstring txt;
	int count = 1;
	StringCchCopy(Dir, MAX_PATH, TEXT("../Data"));
	StringCchCat(Dir, MAX_PATH, TEXT("/*"));
	// find first file
	hFind = FindFirstFile(Dir, &fileDirData);
	// list all files in directory with size
	do
	{
		if (fileDirData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			_tprintf(TEXT("%s     <DIR>\n"), fileDirData.cFileName);
		}
		else
		{
			fileSize.LowPart = fileDirData.nFileSizeLow;
			fileSize.HighPart = fileDirData.nFileSizeHigh;
			_tprintf(TEXT("%s     size = %ld bytes\n"), fileDirData.cFileName, fileSize.QuadPart);
			txt = fileDirData.cFileName;
			std::string temp(txt.begin(),txt.end());

			response += temp + ", ";
			fileNames.push_back(temp);
			count++;
		}
	} while (FindNextFile(hFind, &fileDirData)!= 0);
	
	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{

	}

	FindClose(hFind);

	// server forever...
	while (true)
	{

		SOCKET client = accept(listener, NULL, NULL);
		if (client == WSAEINVAL)
		{
			std::cout << "named Socket not listening" << std::endl;
		}

		// we do this
		// recv
		//send
		// closesocket5

		int bytesReceived = recv(client, buffer, std::size(buffer) - 1, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			printf("recv failed.\n");
			return -1;
		}
		if (bytesReceived == 0)
		{
			printf("Connection closed.\n");
			return 0;
		}
		else
		{
			buffer[bytesReceived] = '\0';
			std::string msg = buffer;
			if (msg == "list" || msg == "List")
			{

				bytesSent = send(client, response.c_str(), (int)response.length(), 0);
			}
			bytesReceived = recv(client, buffer, std::size(buffer) - 1, 0);
			buffer[bytesReceived] = '\0';
				bytesSent = 0;
				msg = buffer;
				for (auto it = fileNames.begin(); it != fileNames.end(); ++it)
				{
					if (msg == *it)
					{
						break;
					}
				}
				msg = "../Data/" + msg;
					 fopen_s(&file,msg.c_str(), "rb");
					 fseek(file, 0L, SEEK_END); 
					 int fileSize = ftell(file);
					 fseek(file, 0L, SEEK_SET);
					 fileBuffer = (char*)malloc(fileSize*sizeof(char));
					 fread(fileBuffer, fileSize, 1, file);
					 fclose(file);
					 int totalBytes = 0;
					do
					{
						bytesSent = send(client, fileBuffer + totalBytes, fileSize - totalBytes, 0);
						totalBytes += bytesSent;
					} while (totalBytes < fileSize);
		}
		closesocket(client);
		free(fileBuffer);
	}

	//close all sockets
	closesocket(listener);

	//Shutdown Winsock
	WSACleanup();
	return 0;
}