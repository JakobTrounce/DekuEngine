Template
write logic without specifying the type
creates code at compile time
gets ignored by compiler at default
ignored unless called
--------------------------------------------
types of templates

class template
function template

---------------------------------------------------
template instantiation
the term for when code is generated for a templated function or class
only happens when compiler does not find a suitable candidate
--------------------------------------------------------------------
explicit template specialization
creating an overloaded function with same name as template function, but different body
intercept the compiler so it does not generate template code
-------------------------------------------------------------------------------------
SFINAE
- substitution failure is not an error
