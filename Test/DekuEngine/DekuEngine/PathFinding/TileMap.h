#ifndef TILEMAP_HEADER_H
#define TILEMAP_HEADER_H

#include <XEngine.h>
#include <fstream>
#include "Camera.h"

class TileMap
{
public:
	TileMap();
	~TileMap();
	void load();
	void unload();
	void render(Camera &c);
	bool CheckCollison(const X::Math::LineSegment& edge) const;
	int GetRows() { return m_rows; }
	int GetColumns() { return m_columns; }
	int GetIndex(int, int) const;
	int* GetMap() { return m_map; }

private:
	int *m_map;
	X::TextureId *m_Tiles;
	std::string info;
	std::ifstream m_textureFile;
	X::Math::Vector2 tilePos = { 0,0 };
	std::ifstream m_mapFile;
	float m_tileSize;
	int m_NumOfTextures;
	char Texture[256];
	char mapNum;
	int m_rows;
	int m_columns;
	int m_index;
	int m_size;
	//////////////////////

};


#endif
