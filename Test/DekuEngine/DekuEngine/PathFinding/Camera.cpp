#include "Camera.h"

void Camera::SetViewPos( X::Math::Vector2 & playerPos)
{
	mViewPosition = playerPos - offset;
}

X::Math::Vector2 Camera::ConvertToWorld(X::Math::Vector2 screenPos)
{
	mWorldPos = screenPos + mViewPosition;

	return mWorldPos;
}

X::Math::Vector2 Camera::ConvertToScreen(X::Math::Vector2 WorldPos)
{
	mScreenPos = WorldPos - mViewPosition;

	return mScreenPos;
}

void Camera::Render(X::TextureId id, X::Math::Vector2 worldPos)
{
	X::DrawSprite(id, ConvertToScreen(worldPos));
}

bool Camera::CanSee(float size, X::Math::Vector2 spritePos, X::Math::Vector2 viewPos)
{
	if (spritePos.x >= viewPos.x - size && spritePos.x <= viewPos.x + 1250 + size && spritePos.y >= viewPos.y - size && spritePos.y <= viewPos.y + 690 + size)
	{
		return true;
	}
	else
	return false;
}
