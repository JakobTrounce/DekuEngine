#ifndef CAMERA_HEADER_H
#define CAMERA_HEADER_H
#include <XEngine.h>

class Camera
{
public:
	void SetViewPos( X::Math::Vector2 &playerPos);
	X::Math::Vector2 ConvertToWorld(X::Math::Vector2 screenPos);
	X::Math::Vector2 ConvertToScreen(X::Math::Vector2 WorldPos);
	void Render(X::TextureId id, X::Math::Vector2 worldPos);
	X::Math::Vector2 GetViewPos()
	{
		return mViewPosition;
	}
	bool CanSee(float, X::Math::Vector2, X::Math::Vector2);
private:
	X::Math::Vector2 mViewPosition;
	X::Math::Vector2 mScreenPos;
	X::Math::Vector2 mWorldPos;
	X::Math::Vector2 offset = {625,345};
};


#endif
