#include "TileMap.h"


TileMap::TileMap()
{
}

TileMap::~TileMap()
{
}

void TileMap::load()
{
	m_mapFile.open("level01.txt");
	m_textureFile.open("tileset01.txt");
	m_mapFile >> info >> m_tileSize >> info >> m_rows >> info >> m_columns;
	m_textureFile >> info >> m_NumOfTextures;
	m_size = m_rows*m_columns;
	m_map = new int[m_size];
	m_Tiles = new X::TextureId[m_NumOfTextures];

	for (int i = 0; i < m_NumOfTextures; ++i)
	{
		m_textureFile >> Texture;
		m_Tiles[i] = X::LoadTexture(Texture);
	}
	for (int i = 0; i < m_size; ++i)
	{
		m_mapFile >> m_map[i];
	}
	m_mapFile.close();
	m_textureFile.close();
}

void TileMap::unload()
{
	
	X::SafeDeleteArray(m_map);
	X::SafeDeleteArray(m_Tiles);

}

void TileMap::render(Camera &c)
{
	
	for (int row = 0; row < m_rows; ++row)
	{
		if ((row+1)*32 > c.GetViewPos().y - X::GetScreenHeight()/2 && (row-1)*32 < c.GetViewPos().y + X::GetScreenHeight())
		{
			for (int column = 0; column < m_columns; ++column)
			{
				if (c.CanSee(m_tileSize, tilePos, c.GetViewPos()))
				{
					c.Render(m_Tiles[m_map[GetIndex(row, column)]], tilePos);
				}
				tilePos.x += m_tileSize;
			}
		}
			tilePos.y += m_tileSize;
			tilePos.x = 0.0f;
	}
	tilePos = { 0,0 };
}

int TileMap::GetIndex(int row, int column) const
{
	 int index = row*m_columns + column;
	
	return index;
}


bool TileMap::CheckCollison(const X::Math::LineSegment& edge) const
{
	int startX = (int)(edge.from.x / m_tileSize);
	int endX = (int)(edge.to.x / m_tileSize);
	int startY = (int)(edge.from.y / m_tileSize);
	int endY = (int)(edge.to.y / m_tileSize);



	for (int y = startY; y <= endY; ++y)
	{
		for (int x = startX; x <= endX; ++x)
		{
			int tileIndex = GetIndex(y, x);
			if (tileIndex == -1)
			{
				continue;
			}
			if (m_map[tileIndex] != 0 && m_map[tileIndex] != 1)
			{
				return true;
			}
		}
	}
	return false;
}


/*

pros and cons:
Array:
- uses contiguous memory locations
- random access (Fast!) O(c), via pointer arithmetic(+-/*)
- Expensive to resize(O(n) time + O(2n space) )
[ ][ ][ ][ ][ ]

Linked List:
 - Allocation on demand
 - Easy to resize (O(c) time + O(c) space)
 - Sequential access

[ ]  [ ]<---[ ]
 |           ^ 
 |           |
 | - - - - - -

 Collision


*/