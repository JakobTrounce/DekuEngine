
#include <XEngine.h>
#include <AI.h>
#include "Map.h"

AI::Graph graph;
X::TextureId startPoint;
X::TextureId endPoint;

Map map;
int rows;
int columns;
bool startActive = false;
bool endActive = false;
bool pathFound = false;

X::Math::Vector2 startDrawPos = {0.0f,0.0f};
X::Math::Vector2 endDrawPos = {0.0f,0.0f};

float CalculateCost(AI::Graph::Coord current, AI::Graph::Coord neighbour)
{
	int cost = (current.x - neighbour.x) + (current.y - neighbour.y);
	cost = abs(cost);
	if (cost == 2)
	{
		return 1.4f;
	}
	return 1;
}

float CalculateHeuristic(AI::Graph::Coord end, AI::Graph::Coord neighbour)
{
	int x = (neighbour.x - end.x);
	int y = (neighbour.y - end.y);
	float use_x = static_cast<float>(abs(x));
	float use_y = static_cast<float>(abs(y));

	return 1.0f*(use_x+use_y)+(1.41f-2.0f*1.0f)*std::min(use_x,use_y);
}

void Initialize()
{
	CameraGame::StaticInitialize();
	map.Load("level2.txt", "tileset01.txt");
	rows = map.GetRow();
	columns = map.GetCol();
	graph.Init(columns, rows);
	startPoint = X::LoadTexture("mushroom.png");
	endPoint = X::LoadTexture("stone.png");


}

void Terminate()
{
	CameraGame::StaticTerminate();
	X::ClearAllTextures();
}

bool GameLoop(float deltaTime)
{

	map.Render();
	for (int x = 0; x < rows * 32; x += 32.0f)
	{
		for (int y = 0; y < columns * 32; y += 32.0f)
		{
			if (X::IsMousePressed(X::Mouse::LBUTTON))
			{
				if (X::GetMouseScreenX() >= x && X::GetMouseScreenX() <= x + 32.0f && X::GetMouseScreenY() >= y && X::GetMouseScreenY() <= y + 32.0f)
				{
					startDrawPos = { (float)x-16.0f,(float)y-16.0f};
					startActive = true;
				}
			}
			if (X::IsMousePressed(X::Mouse::RBUTTON))
			{
				if (X::GetMouseScreenX() >= x && X::GetMouseScreenX() <= x + 32.0f && X::GetMouseScreenY() >= y && X::GetMouseScreenY() <= y + 32.0f)
				{
					endDrawPos = { (float)x-16.0f,(float)y-16.0f };
					endActive = true;
				}
			}
		}
	}
	if (X::IsKeyPressed(X::Keys::B))
	{
		AI::Graph::Coord start{startDrawPos.x/32.0f, startDrawPos.y/32.0f};
		AI::Graph::Coord end{endDrawPos.x/32.0f,endDrawPos.y/32.0f };
		graph.RunBFS(start,end);
		pathFound = true;
	}
	if (X::IsKeyPressed(X::Keys::D))
	{
		AI::Graph::Coord start{ startDrawPos.x / 32.0f, startDrawPos.y / 32.0f };
		AI::Graph::Coord end{ endDrawPos.x / 32.0f,endDrawPos.y / 32.0f };
		graph.RunDijkstra(start, end,CalculateCost);
		pathFound = true;
	}
	if (X::IsKeyPressed(X::Keys::A))
	{
		AI::Graph::Coord start{ startDrawPos.x / 32.0f, startDrawPos.y / 32.0f };
		AI::Graph::Coord end{ endDrawPos.x / 32.0f,endDrawPos.y / 32.0f };
		graph.RunAStar(start, end,CalculateCost,CalculateHeuristic);
		pathFound = true;
	}
	if (X::IsKeyPressed(X::Keys::R))
	{
		AI::Graph::Coord start{ startDrawPos.x / 32.0f, startDrawPos.y / 32.0f };
		AI::Graph::Coord end{ endDrawPos.x / 32.0f,endDrawPos.y / 32.0f };
		graph.RunDFS(start, end);
		pathFound = true;
	}
	if (pathFound)
	{
		std::vector<AI::Graph::Coord> path = graph.GetPath();
		std::vector<AI::Graph::Coord> totalPath = graph.GetTotalPath();
		for (int i = 1; i < totalPath.size() - 1; ++i)
		{
			AI::Graph::Coord parent = graph.GetNode({ totalPath[i].x, totalPath[i].y }).parent->coord;
			X::DrawScreenLine(totalPath[i].x*32.0f+16.0f, totalPath[i].y*32.0f+16.0f, parent.x*32.0f+16.0f, parent.y*32.0f+16.0f, X::Math::Vector4::Blue());
		}
		for (int i = 0; i < path.size()-1; ++i)
		{
			X::DrawScreenLine(path[i].x*32.0f+16.0f, path[i].y*32.0f+16.0f, path[i + 1].x*32.0f+16.0f,path[i+1].y*32.0f+16.0f, X::Math::Vector4::Red());
		}
		
	}
	if (X::IsKeyPressed(X::Keys::W))
	{
		graph.CreateWalls(map.GetMap());
	}
	if (startActive)
	{
		X::DrawSprite(startPoint, startDrawPos);
	}
	if (endActive)
	{
		X::DrawSprite(endPoint, endDrawPos);
	}
	if (X::IsKeyPressed(X::Keys::Q))
	{
		startActive = false;
		endActive = false;
	}
	return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int )
{
	X::Start();
	Initialize();
	X::Run(GameLoop);
	Terminate();
	X::Stop();
	return 0;

}


