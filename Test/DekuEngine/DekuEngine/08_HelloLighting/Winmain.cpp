#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>



using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Math;




int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{

	// setup our application window
	Deku::Core::Window myWindow;
	myWindow.Initialize(hInstance, "Hello Lighting", 1280, 720);
	//initialize the graphics system
	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);
	SimpleDraw::StaticInitialize(1000);
	


	Camera camera;
	float speed = 1.0f;
	camera.SetPosition({ 0.0f,0.0f,-10.0f });
	camera.SetDirection({ 0.0f,0.0f,1.0f });
	camera.SetFarPlane(5000.0f);



	
	
	
	bool done = false;
	while (!done)
	{
		done = myWindow.ProcessMessage();
		done = GetAsyncKeyState(VK_ESCAPE);
		//Run Game Logic


		//camera

		if (GetAsyncKeyState(0x57))
		{
			camera.Walk(speed);
		}
		else if (GetAsyncKeyState(0x53))
		{
			camera.Walk(-speed);
		}
		if (GetAsyncKeyState(0x41))
		{
			camera.Strafe(-speed);
		}
		else if (GetAsyncKeyState(0x44))
		{
			camera.Strafe(speed);
		}
		if (GetAsyncKeyState(0x46))
		{
			camera.Yaw(speed* 0.005);
		}
		else if (GetAsyncKeyState(0x52))
		{
			camera.Yaw(-speed * 0.005);
		}
		else if (GetAsyncKeyState(VK_SHIFT))
		{
			camera.Rise(-speed);
		}
		else if (GetAsyncKeyState(VK_SPACE))
		{
			camera.Rise(speed);
		}


		GraphicsSystem::Get()->BeginRender();
		
		SimpleDraw::AddLine({ 50.0f,50.0f,0.0f }, { 150.0f,150.0f,0.0f }, Colors::Fuchsia);
			
		SimpleDraw::Render(camera);
		//before draw
	

		GraphicsSystem::Get()->EndRender();

	}

	
	SimpleDraw::StaticTerminate();
	GraphicsSystem::StaticTerminate();
	myWindow.Terminate();
	return 0;
}


