#ifndef INCLUDED_DEKU_MATH_VECTOR4_H
#define INCLUDED_DEKU_MATH_VECTOR4_H

namespace Deku {
	namespace Math {

		struct Vector4
		{
			union
			{
				struct { float x, y, z, w; };
				std::array<float, 4> v;
				// std::array does bound checking for you, safer version of static array
				// no downside.
			};

			constexpr Vector4() noexcept : Vector4(0.0f) {}
			constexpr Vector4(float f) noexcept : x(f), y(f), z(f), w(f) {}
			constexpr Vector4(float x, float y, float z, float w) noexcept : x(x), y(y), z(z), w(w) {}

			Vector4  operator-() const { return{ -x,-y,-z, -w }; }
			Vector4  operator+(const Vector4& v) const { return { x + v.x,y + v.y,z + v.z, w + v.w }; }
			Vector4  operator*(float f) const { return { x * f, y * f,z * f, w * f }; }
			Vector4  operator-(const Vector4& v) const { return { x - v.x, y - v.y, z - v.z, w - v.w }; }
			Vector4  operator/(float f) const { DEKUASSERT((f != 0.0f), "[Deku::Math::Vector4] Cannot divide by zero!"); float divisor = 1 / f; return { x * divisor,y * divisor ,z * divisor, w * divisor }; }
			// - ,*,  /

			Vector4& operator+=(const Vector4& v)
			{
				x += v.x;
				y += v.y;
				z += v.z;
				w += v.w;
				return *this;
			}

			Vector4& operator-=(const Vector4& v)
			{
				x -= v.x;
				y -= v.y;
				z -= v.z;
				w -= v.w;
				return *this;
			}

			Vector4& operator*=(float f)
			{
				x *= f;
				y *= f;
				z *= f;
				w *= f;
				return *this;
			}
			Vector4& operator/=(float f)
			{
				DEKUASSERT(f != 0, "[Deku::Math::Vector4] Cannot divide by zero");
				float divisor = 1 / f;
				
				*this *= divisor;

				return *this;
			}
			// -=, *=. /=


			// Colors

		};

	} //namespace math
} // namespace Deku

#endif //ifndef INCLUDED_DEKU_MATH_Vector4_H
