#ifndef INCLUDED_DEKU_DEKUMATH_H
#define INCLUDED_DEKU_DEKUMATH_H

//facade header for math library

#include "Common.h"

#include "Vector3.h"
#include "Vector2.h"
#include "Vector4.h"
#include "Matrix4.h"
#include "Constants.h"

namespace Deku {
namespace Math{

	inline constexpr Vector3 Cross(const Vector3& v0, const Vector3& v1)
	{
		Vector3 returnme;

		returnme.x = (v0.y * v1.z) - (v0.z * v1.y);
		returnme.y = (v0.z * v1.x) - (v0.x * v1.z);
		returnme.z = (v0.x * v1.y) - (v0.y * v1.x);

		return returnme;
	}
	inline constexpr float Dot(const Vector3& v0, const Vector3& v1)
	{
		return  (v0.x*v1.x) + (v0.y*v1.y) + (v0.z*v1.z) ;
	}
	inline constexpr float MagnitudeSqr(const Vector3& v)
	{
		return Dot(v, v);
	}
	inline float Magnitude(const Vector3& v)
	{
		return sqrt(MagnitudeSqr(v));
	}
	inline Vector3 Normalize(const Vector3& v)
	{
		return (v * ( 1.0f / Magnitude(v)));
	}

	inline constexpr float Dot(const Vector2& v0, const Vector2& v1)
	{
		return (v0.x*v1.x) + (v0.y*v1.y);
	}
	inline constexpr float MagnitudeSqr(const Vector2& v)
	{
		return Dot(v, v);
	}
	inline float Magnitude(const Vector2& v)
	{
		if (v.x != 0.0f || v.y != 0.0f)
		{
			return sqrt(MagnitudeSqr(v));
		}
		else
		{
			return 0;
		}

	}
	inline Vector2 Normalize(const Vector2& v)
	{
		if (v.x != 0.0f || v.y != 0.0f)
		{
			return (v * (1.0f / Magnitude(v)));
		}
		else
		{
			return Vector2(1.0f);
		}
	}

	inline constexpr Vector4 Cross(const Vector4& v0, const Vector4& v1)
	{
		Vector4 returnme;

		returnme.x = (v0.y * v1.z) - (v0.z * v1.y);
		returnme.y = (v0.z * v1.x) - (v0.x * v1.z);
		returnme.z = (v0.x * v1.y) - (v0.y * v1.x);

		return returnme;
	}
	inline constexpr float Dot(const Vector4& v0, const Vector4& v1)
	{
		return  (v0.x*v1.x) + (v0.y*v1.y) + (v0.z*v1.z) + (v0.w * v1.w);
	}
	inline constexpr float MagnitudeSqr(const Vector4& v)
	{
		return Dot(v, v);
	}
	inline float Magnitude(const Vector4& v)
	{
		return sqrt(MagnitudeSqr(v));
	}
	inline Vector4 Normalize(const Vector4& v)
	{
		return (v * (1.0f / Magnitude(v)));
	}

	
	
} //namespace math
} // namespace Deku

#endif //INCLUDED_DEKU_DEKUMATH_H
