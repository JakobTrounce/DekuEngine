#ifndef INCLUDED_DEKU_MATH_CONSTANTS_H
#define INCLUDED_DEKU_MATH_CONSTANTS_H
namespace Deku {
namespace Math {
namespace Constants
{
	constexpr float Pi = 3.1415926535f;
	constexpr float TwoPi = Pi * 2.0f;
	constexpr float DegToRad = Pi / 180.0f;
	constexpr float RadToDeg = 180.0f / Pi;
	constexpr float Epsilon = 0.0001f;
}
}
}
#endif // INCLUDED_DEKU_MATH_CONSTANTS_H