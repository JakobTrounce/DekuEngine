#ifndef INCLUDED_CHARACTER_H
#define INCLUDED_CHARACTER_H
#include <Network.h>
#include <XEngine.h>
#include <string>
#include "ProjectileManager.h"
#include "TripleShot.h"
class Item;
class Skill;
class Character
{
public:
	Character(float speed,std::string textureName) : mMoveSpeed(speed), mTextureName(textureName) {}
	Character()
	{

	}
	~Character()
	{

	}
	virtual void Update(float deltaTime);
	void Render();
	virtual void Load();
	virtual void Unload();
	//virtual void Serialize();
	//virtual void Deserialize();
	virtual void UseSkill(int skill);

	const X::Math::Vector2 Position() const { return mPosition; }
	bool IsSkillActive() { return mSkillActive; }
	bool CanAttack() {(mAtkTimer <= 0.0f) ? mCanAttack = true : mCanAttack = false; return mCanAttack; }

	
private:
	//member variables
	float mHealth{ 0 };
	float mArmor{ 0 };
	float mAtk{ 0 };
	float mMoveSpeed{ 0 };
	float mAtkDelay{ 0 };
	float mAtkTimer{ 0 };
	bool  mSkillActive{ false };
	bool mCanAttack{ false };
	std::string mName{""};
	std::string mTextureName{ "" };
	X::Math::Vector2 mPosition{500.0f,500.0f};
	X::Math::Vector2 mBulletSpawnPos{ 0.0f,0.0f };
	X::TextureId mTexture{0};
	std::vector<Item*> inventory{nullptr};
	TripleShot mShot;
	std::vector<Skill*> mSkills;

	//member functions

	void Move(float deltaTime);
	void Attack();
	
};

#endif