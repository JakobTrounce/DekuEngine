#ifndef INCLUDED_TRIPLESHOT_H
#define INCLUDED_TRIPLESHOT_H
#include "Skill.h"
class TripleShot : public Skill
{
public:
	 void  Activate() override;
private:

	X::Math::Vector2 mPlayerPos{ 0,0 };

	X::Math::Vector2 DirOffset{0,0};
};

#endif
