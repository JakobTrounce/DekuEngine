#include "Character.h"
#include "ProjectileManager.h"
#include "GameManager.h"
#include <XEngine.h>

void Initialize();
void Terminate();

Character main(250.0f,"zelda.png");

bool GameLoop(float deltaTime)
{

	main.Update(deltaTime);
	main.Render();
	ProjectileManager::Get()->Update(deltaTime);
	ProjectileManager::Get()->Render();
	GameManager::Get()->Update(deltaTime, main.Position());

	return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	X::Start();
	Initialize();

	X::Run(GameLoop);

	Terminate();
	X::Stop();
}

void Initialize()
{
	ProjectileManager::StaticInitialize();
	ProjectileManager::Get()->Load();
	GameManager::StaticInitialize();
	GameManager::Get()->Load();
	main.Load();
}

void Terminate()
{
	ProjectileManager::Get()->Unload();
	ProjectileManager::StaticTerminate();
	GameManager::Get()->Unload();
	GameManager::StaticTerminate();
	main.Unload();
}