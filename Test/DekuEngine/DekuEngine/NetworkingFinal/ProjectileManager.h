#ifndef INCLUDED_PROJECTILEMANAGER_H
#define INCLUDED_PROJECTILEMANAGER_H
#include "Projectile.h"

class ProjectileManager
{
public:
	static void StaticInitialize();
	static void StaticTerminate();
	static ProjectileManager* Get();

public:
	ProjectileManager();
	~ProjectileManager();

	void Load();
	void Unload();
	void Update(float deltaTime);
	void Render();

	void Fire(const X::Math::Vector2& pos, const X::Math::Vector2& vel);

	Projectile GetProjectile(int index);
private:
	static const int kProjectileCount = 200;
	Projectile mProjectiles[kProjectileCount];
	int mProjectileIndex = 0;
};


#endif
