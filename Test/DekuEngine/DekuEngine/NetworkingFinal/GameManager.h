#ifndef INCLUDED_GAMEMANAGER_H
#define INCLUDED_GAMEMANAGER_H
#include <XEngine.h>
class GameManager
{
public:
	static void StaticInitialize();
	static void StaticTerminate();

	static GameManager* Get();

public:

	GameManager();
	~GameManager();

	void Load();
	void Unload();
	void Update(float deltaTime,X::Math::Vector2 playerPos);
	const int ScreenWidth() const { return mScreenWidth; }
	const int ScreenHeight() const { return mScreenHeight; }
	const X::Math::Vector2 PlayerPos() const { return mPlayerPos; }

private:

	int mScreenHeight;
	int mScreenWidth;
	X::Math::Vector2 mPlayerPos{ 0,0 };
};


#endif
