#include "DoubleShot.h"
#include "ProjectileManager.h"
#include "GameManager.h"

void DoubleShot::Activate()
{
	X::Math::Vector2 mousePos = { (float)X::GetMouseScreenX(),(float)X::GetMouseScreenY() };
	X::Math::Vector2 direction = X::Math::Normalize(mousePos - mPlayerPos);

	DirOffset = { 25.0f,0 };
	mPlayerPos = GameManager::Get()->PlayerPos();

	ProjectileManager::Get()->Fire((mPlayerPos), direction);
	ProjectileManager::Get()->Fire((mPlayerPos + DirOffset), direction);
}