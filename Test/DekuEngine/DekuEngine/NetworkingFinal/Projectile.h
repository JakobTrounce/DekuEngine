#ifndef INCLUDED_PROJECTILE_H
#define INCLUDED_PROJECTILE_H
#include <XEngine.h>
#include <string>
class Projectile
{
public:
	Projectile(std::string textureName);
	Projectile();
	~Projectile();

	void Load();
	void Unload();
	void Update(float deltaTime);
	void Render();

	void Fire(const X::Math::Vector2& pos, const X::Math::Vector2& vel);

	bool IsOutOfBounds();
	bool IsColliding();

	bool IsDebugging() const { return mDebugging; }
    bool IsActive() const { return mActive; }

	void Kill();

	X::Math::Circle GetBoundingCircle();

private:
	X::Math::Vector2 mPosition{ 0.0f,0.0f };
	X::Math::Vector2 mVelocity{ 0.0f,0.0f };
	X::TextureId mTexture{ 0 };
	std::string mTextureName;
	float mSpeed{ 300.0f };
	bool mActive{ false };
	bool mDebugging{ false };
	float ProjectileWidth{ 0.0f };
	float ProjectileHeight{ 0.0f };
};




#endif
