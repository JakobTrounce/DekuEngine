#include "GameManager.h"


namespace
{
	GameManager* sInstance = nullptr;
}

void GameManager::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[GameManager] Singleton already initialized.");
	sInstance = new GameManager();
}

void GameManager::StaticTerminate()
{
	X::SafeDelete(sInstance);
}

GameManager* GameManager::Get()
{
	XASSERT(sInstance != nullptr, "[GameManager] Singleton not initialized.");
	return sInstance;
}

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::Load()
{
	mScreenHeight = X::GetScreenHeight();
	mScreenWidth = X::GetScreenWidth();
}

void GameManager::Unload()
{

}

void GameManager::Update(float deltaTime, X::Math::Vector2 playerPos)
{
	mPlayerPos = playerPos;
}