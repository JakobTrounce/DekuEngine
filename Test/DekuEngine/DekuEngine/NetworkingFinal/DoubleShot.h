#ifndef INCLUDED_DOUBLESHOT_H
#define INCLUDED_DOUBLESHOT_H
#include "Skill.h"
class DoubleShot : public Skill
{
public:
	void  Activate() override;
private:

	X::Math::Vector2 mPlayerPos{ 0,0 };

	X::Math::Vector2 DirOffset{ 0,0 };
};

#endif
