#ifndef INCLUDED_COMMAND_H
#define INCLUDED_COMMAND_H
#include <XEngine.h>
#include <Network.h>

class Command
{
public:
	void SetDuration(float duration);
	float GetCommandTime(float deltaTime);

	void Update(float deltaTime);
	bool Empty() const;

	bool MoveForward() const;
	bool MoveBackward() const;
	bool MoveRight() const;
	bool MoveLeft() const;

	void Serialize(Network::StreamWriter& writer);
	void Deserialize(Network::StreamReader& reader);


	enum TimedCommand // old school
	{
		kUp		= 0x1 << 0,
		kDown	= 0x1 << 1,
		kRight	= 0x1 << 2,
		kLeft	= 0x1 << 3
	};

	enum OneShotCommand
	{
		//shoot 0x1 << 0
	};
	float mDuration{0.0f};
	uint32_t mTimedCommand{0};
	uint32_t mOneShotCommand{ 0 };
};



#endif