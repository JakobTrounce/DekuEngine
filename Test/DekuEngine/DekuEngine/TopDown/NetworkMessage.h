#ifndef INCLUDED_NETWORKMESSAGE_H
#define INCLUDED_NETWORKMESSAGE_H
#include <XEngine.h>
enum class MessageType : uint32_t
{
	Snapshot,
	Assignment,
	Create,
	Update,
	Destroy,
	Command
};





#endif