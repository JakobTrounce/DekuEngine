#ifndef INCLUDED_GAMEOBJECT_H
#define INCLUDED_GAMEOBJECT_H
#include <Network.h>
#include <XEngine.h>

class GameObject
{
public:

	ABSTRACT_CLASS_ID(GameObject, 'GOB');
	GameObject(bool sync);

	virtual void Load();
	virtual void Update(float deltaTime) = 0;
	virtual void Draw() = 0;

	virtual void Serialize(Network::StreamWriter& writer) const = 0;
	virtual void Deserialize(Network::StreamReader& reader) = 0;

	void Kill() { mDestroyed = true; }
	void Clean() { mDirty = false; }
	void Dirty() {mDirty = true;}

	//uint32_t GetType() const { return mType; }
	uint32_t GetNetworkId() const { return mNetworkId; }
	bool IsActive() const { return !mDestroyed; }
	bool IsSync() const { return mSync; }
	bool IsDirty() const { return mDirty; }
private:
	friend class GameObjectManager;
	uint32_t mNetworkId;
	bool mDestroyed;
	bool mSync;
	bool mDirty;


};
using GameObjectList = std::vector<GameObject*>;
#endif