#include "Map.h"



Map::Map()
{
}


Map::~Map()
{
}

void Map::Load(std::string lvlName, std::string textName)
{
	lvl.open(lvlName);
	if (lvl.is_open())
	{
		std::string info;
		lvl >> info >> mTileSize;
		lvl >> info >> mRow;
		lvl >> info >> mColumn;
		mMap = new int[mRow * mColumn];

		for (int index = 0; index < (mColumn * mRow); ++index)
		{
			if (index > 100)
			{
				index = index;
			}
			lvl >> mMap[index];
		}
	}

	lvl.close();

	texture.open(textName);
	if (texture.is_open())
	{
		std::string Name;
		int numTile;
		texture >> Name;
		texture >> numTile;

		int numbers = numTile;

		mTile = new X::TextureId[numTile];

		for (int num = 0; num < numTile; ++num)
		{
			char name[256];
			texture >> name;
			mTile[num] = X::LoadTexture(name);
		}
	}
	texture.close();
}


void Map::Render()
{
	X::Math::Vector2 mPos{ 0,0 };

	for (int buffer = 0; buffer < mRow * mColumn;)
	{
		mPos.x = 0.0f;
		if (buffer > 90)
		{
			buffer = buffer;
		}
		for (int col = 0; col < mColumn; ++col)
		{
			CameraGame::Get()->Render(mTile[mMap[buffer + col]], mPos);
			mPos.x += mTileSize;
		}
		mPos.y += mTileSize;
		buffer += mColumn;
	}
}

void Map::Unload()
{
	X::SafeDeleteArray(mMap);
	X::SafeDeleteArray(mTile);

	lvl.close();
	texture.close();
}

bool Map::CheckCollision(const X::Math::LineSegment & edge, std::list<int> checker) const
{
	int startX = (int)(edge.from.x / mTileSize);
	int startY = (int)(edge.from.y / mTileSize);
	int endX = (int)(edge.to.x / mTileSize);
	int endY = (int)(edge.to.y / mTileSize);

	for (int y = startY; y <= endY; ++y)
	{
		for (int x = startX; x <= endX;++x)
		{
			int tileIndex = y * mColumn + x;

			for (auto it = checker.begin(); it != checker.end(); ++it)
			{
				if (mMap[tileIndex] == *it)
				{
					return false;
				}
			}
		}
	}

	return true;
}

void Map::SaveMap(std::string name)
{
	std::ofstream newlvl;
	

	newlvl.open(name, std::ofstream::out); //if need be save it in the original file
	
	newlvl << "TileSize: " << mTileSize << "\n";
	newlvl << "Row: " << mRow <<"\n";
	newlvl << "Col: " << mColumn <<"\n";

	for (int row = 0; row < mRow * mColumn;)
	{
		for (int col = 0; col < mColumn; ++col)
		{
			newlvl << mMap[row + col] << " ";
		}
		newlvl << "\n";
		row += mColumn;
	}
}
