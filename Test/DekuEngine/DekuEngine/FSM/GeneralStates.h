#ifndef INCLUDED_GENERALSTATES_H
#define INCLUDED_GENERALSTATES_H
#include <AI/Inc/AI.h>


enum class GeneralStates : int
{
	RestState,
	DepositState,
	Work
};

enum class Materials : int
{
	wood,
	stone
};






template <class T>
 class RestState : public AI::State<T>
{
public:
	float timer{ 0.0f };
	RestState() {}
	RestState(X::Math::Vector2 hm);
	~RestState() {}
	void Enter(T& agent) override;
	void Update(T& agent, float deltaTime) override;
	void Exit(T& agent) override;


	X::Math::Vector2 mHome;
	AI::Graph::Coord home;
	AI::Graph mNav;
	bool isHome{ false };
	bool pathFound{ false };
	AI::PathPlanner path;



};

 template <class T>
 class DepositInventoryState : public AI::State<T>
 {
 public:
	 float timer{ 0.0f };
	 DepositInventoryState() {}
	 DepositInventoryState(X::Math::Vector2 warehouse);
	 ~DepositInventoryState() {}
	 void Enter(T& agent) override;
	 void Update(T& agent,float deltaTime) override;
	 void Exit(T& agent) override;

	 X::Math::Vector2 mWarehouse;
	 AI::Graph::Coord warehouse;
	 AI::Graph mNav;
	 int stone = 0;
	 int wood = 0;
	 bool atWarehouse{ false };
	 bool pathFound{ false };
	 AI::PathPlanner path;
 };

 template<class T>
 RestState<T>::RestState(X::Math::Vector2 hm)
	 :
	 mHome(hm)
 {

 }
 
 template <class T>
 void RestState<T>::Enter(T& agent)
 {

 }

 template<class T>
 void RestState<T>::Update(T& agent, float deltaTime)
 {
	 float energyTime = 2.0f;
	 int energyGain = 10;
	 if (!isHome)
	 {
		 if (!pathFound)
		 {
			 path.RequestPath(mHome);
			pathFound = true;
		 }
		 else if (pathFound)
		 {

			 // follow path using seek steering
			 if (home == home)
			 {
				 isHome = true;
			 }
		 }
	 }
	 else
	 {

	 }
	  if(agent.GetEnergy() < agent.GetMaxEnergy()&& isHome)
	 {
		 timer += deltaTime;
		 if (timer >= energyTime)
		 {
			 agent.GainEnergy(energyGain);
		 }
	 }
	 if (agent.GetEnergy() >= agent.GetMaxEnergy())
	 {
		 agent.SetEnergy(agent.GetMaxEnergy());
		 agent.mStateMachine->ChangeState((int)GeneralStates::Work);
	 }
 }

 template<class T>
 void RestState<T>::Exit(T& agent)
 {

 }

 template<class T>
 DepositInventoryState<T>::DepositInventoryState(X::Math::Vector2 warehouse)
	 :
	 mWarehouse(warehouse)
 {

 }

 template<class T>
 void DepositInventoryState<T>::Enter(T& agent)
 {
	 warehouse.x = (int)mWarehouse.x / 32.0f;
	 warehouse.y = (int)mWarehouse.y / 32.0f;
 }

 template<class T> 
 void DepositInventoryState<T>::Update(T& agent,float deltaTime)
 {
	 AI::Graph::Coord pos = { agent.GetPos().x/32.0f,agent.GetPos().y/32.0f };
	 if (!atWarehouse)
	 {
		 if (!pathFound)
		 {
			 path.RequestPath(mWarehouse);
			// mNav.RunAStar(pos, warehouse, CalculateCost, CalculateHeuristic);
			 pathFound = true;
		 }
		 else
		 {
			 //seek through the path
			 if (pos == warehouse)
			 {
				 atWarehouse = true;
			 }
		 }
	 }
	 else if(agent.GetInventory().size() > 0 && atWarehouse)
	 {
		 if (agent.GetInventory()[0] == (int)Materials::stone)
		 {
			 for (auto it = agent.GetInventory().begin(); it != agent.GetInventory().end(); ++it)
			 {
				 stone++;
			 }
		 }
		 else if (agent.GetInventory()[0] == (int)Materials::wood)
		 {
			 for (auto it = agent.GetInventory().begin(); it != agent.GetInventory().end(); ++it)
			 {
				 wood++;
			 }
		 }
	 }
	 else
	 {
		 agent.mStateMachine->ChangeState((int)GeneralStates::Work);
	 }
 }

 template<class T>
 void DepositInventoryState<T>::Exit(T& agent)
 {

 }

#endif // INCLUDED_GENERALSTATES_H
