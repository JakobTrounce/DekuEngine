
#include <AI/Inc/AI.h>
#include "Map.h"
#include "Miner.h"

Map map;
Miner miner({400.0f,400.0f}, 100);


void Initialize()
{
	CameraGame::StaticInitialize();
	map.Load("TownMap.txt", "TownTextures.txt");
	miner.Load();


}

void Terminate()
{
	miner.Unload();
	map.Unload();
	CameraGame::StaticTerminate();
}
void OnGui()
{
}

bool GameLoop(float deltaTime)
{
	miner.Update(deltaTime);
	map.Render();
	miner.Render();

	return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	X::Start();
	Initialize();
	X::Run(GameLoop);
	Terminate();
	X::Stop();
	return 0;

}