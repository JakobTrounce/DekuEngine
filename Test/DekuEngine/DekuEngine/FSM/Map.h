#pragma once
#include <XEngine.h>
#include "CameraGame.h"
#include <fstream>
#include <list>

class Map
{
public:
	Map();
	~Map();

	void Load(std::string lvlName, std::string textName);
	void Render();
	void Unload();

	int GetIndex(int row, int col) const { return row * mColumn + col; }

	bool CheckCollision(const X::Math::LineSegment& edge, std::list<int> checker) const;
	void SaveMap(std::string name);

	int GetRow() { return mRow; }
	int GetCol() { return mColumn; }
	float GetSize() { return mTileSize; }

	int GetTileCode(int x, int y) { return mMap[GetIndex(x, y)];}
	int *GetMap() { return mMap; }

private:
	X::TextureId *mTile;
	int *mMap;
	float mTileSize;
	int mRow;
	int mColumn;
	std::ifstream lvl;
	std::ifstream texture;
};

