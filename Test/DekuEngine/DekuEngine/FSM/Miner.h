#ifndef INCLUDED_MINER_H
#define INCLUDED_MINER_H
#include "GeneralStates.h"
#include "AI/Inc/Agent.h"


class Miner
{
	public:

	Miner();
	Miner(X::Math::Vector2 pos,int Energy);
	void Load();
	void Unload();
	void Update(float deltaTime);
	void Render();
	X::Math::Vector2 GetPos() { return mPos; }
	void LoseEnergy(int amount) { mEnergy -= amount; }
	std::vector<int> GetInventory() { return mInventory; }
	int GetEnergy() { return mEnergy; }
	int GetMaxInvSize() { return maxInvSize; }
	int GetMaxEnergy() { return maxEnergy; }
	void SetEnergy(int amount) { mEnergy = amount; }
	void GainEnergy(int amount) { mEnergy += amount; }


	AI::StateMachine<Miner>* mStateMachine;
private:
	X::TextureId mTexture;
	int maxInvSize = 10;
	X::Math::Vector2 mPos;
	std::vector<int> mInventory;
	int mEnergy;
	int maxEnergy;
};



#endif // INCLUDED_MINER_H
