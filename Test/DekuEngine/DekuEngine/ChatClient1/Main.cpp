#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <Windows.h>
#include <thread>
#include <conio.h>
#include<mutex>
#pragma comment(lib,"Ws2_32.lib")
void ReceiveIncoming(SOCKET &connection);
std::mutex messageLock;
std::string message;

int main(int argc, char* argv[])
{
	char c;
	const char* hostAddress = "127.0.0.1";
	uint16_t port = 8888;

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	SOCKET mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//Resolve host using address
	in_addr iaHost;
	iaHost.s_addr = inet_addr(hostAddress);
	HOSTENT* hostEntry = gethostbyaddr((const char*)&iaHost, sizeof(struct in_addr), AF_INET);

	//fill host address information and connect
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr = *((LPIN_ADDR)*hostEntry->h_addr_list);
	serverInfo.sin_port = htons(port);
	connect(mySocket, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	printf("Connected to Chat server\n");
	int bytesSent = 0;
	std::thread received(ReceiveIncoming, std::ref(mySocket));
	while (true)
	{
		std::getline(std::cin, message);
		if (GetAsyncKeyState(VK_RETURN))
		{
			bytesSent = send(mySocket, message.c_str(), (int)message.length(), 0);
			messageLock.lock();
			message.clear();
			messageLock.unlock();
		}

		//wait for server response **blocking**
		// send and recieve bytes, not strings
	}
	//close all sockets
	received.join();
	closesocket(mySocket);
	//Shutdown Winsock
	WSACleanup();
	return 0;
}


void ReceiveIncoming(SOCKET &connection)
{
	char buffer[1024];
	int bytesReceived = 0;
	while (true)
	{

	bytesReceived = recv(connection, buffer, std::size(buffer) - 1, 0);
	buffer[bytesReceived] = '\0';
		if (bytesReceived == SOCKET_ERROR)
		{

			printf("recv failed.\n");
		}
		else if (bytesReceived > 0)
		{
			printf("%s \n", buffer);
		}
	}
}