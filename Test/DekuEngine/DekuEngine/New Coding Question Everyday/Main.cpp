#include <iostream>

bool CodingQuestionDay1();
void CodingQuestionDay2();
int main()
{
	CodingQuestionDay2();
}

bool CodingQuestionDay1()
{
	/*Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
*/

	int numList[4] = { 10, 15, 3, 6 };

	int sum = 17;

	for (int i = 0; i < 4; ++i)
	{
		for (int x = 0; x < 4; ++x)
		{
			if (numList[i] + numList[x] == sum)
			{
				std::cout << "True" << std::endl;
				return true;
			}
		}
	}
	std::cout << "False" << std::endl;
	return false;

}
void CodingQuestionDay2()
{
	/*Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].

Follow-up: what if you can't use division?*/

	int input[5] = { 1,2,3,4,5 };
	int output[5];

	for (int i = 0; i < 5; ++i)
	{
			output[i] = 1;
	
		for (int x = 0; x < 5; ++x)
		{
			if (x == i)
			{
				continue;
			}

			output[i] *= input[x];
		}
	}
	for (int i = 0; i < 5; ++i)
	{
		std::cout << output[i] << std::endl;
	}
}