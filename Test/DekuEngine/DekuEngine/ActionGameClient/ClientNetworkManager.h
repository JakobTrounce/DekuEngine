#ifndef INCLUDED_CLIENTNETWORKMANAGER_H
#define INCLUDED_CLIENTNETWORKMANAGER_H

#include "TopDown/ActionGame.h"
#include "Network/Inc/Network.h"
#include <XEngine.h>

class ClientNetworkManager
{

	public:
		static void StaticInitialize();
		static void StaticTerminate();
		static ClientNetworkManager* Get();

		void Initialize();
		void Terminate();

		ClientNetworkManager();
		~ClientNetworkManager();

		bool ConnectToServer(const char* host, uint32_t port);

		bool SendMessageToServer(const Network::MemoryStream& memStream);
		bool HandleMessage();
		uint32_t GetClientId() const { return mClientId; }
	private:


		Network::TCPSocket mSocket;
		uint32_t mClientId;
};



#endif
