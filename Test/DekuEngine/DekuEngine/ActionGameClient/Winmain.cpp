#include "ClientNetworkManager.h"

enum class GameState
{
	Connecting,
	Connected,
	Disconnected
};

GameState gameState = GameState::Connecting;
const char* host = "127.0.0.1";
const uint16_t port = 8888;

void UpdateConnecting()
{
	X::DrawScreenText("Connecting.....", 10.0f, 10.0f, 16.0f, X::Math::Vector4::White());

	if (ClientNetworkManager::Get()->ConnectToServer(host, port))
	{
		gameState = GameState::Connected;
	}
	else
	{
		gameState = GameState::Disconnected;
	}
}

void UpdateConnected(float deltaTime)
{
	X::DrawScreenText("Connected", 10.0f, 10.0f, 16.0f, X::Math::Vector4::White());

	if (ClientNetworkManager::Get()->HandleMessage())
	{
		Command command;
		if (X::IsKeyDown(X::Keys::W))
			command.mTimedCommand |= Command::kUp;
		if(X::IsKeyDown(X::Keys::A))
			command.mTimedCommand |= Command::kLeft;
		if(X::IsKeyDown(X::Keys::S))
			command.mTimedCommand |= Command::kDown;
		if(X::IsKeyDown(X::Keys::D))
			command.mTimedCommand |= Command::kRight;
		if (!command.Empty())
		{
			command.SetDuration(deltaTime);

			Network::MemoryStream memStream;
			Network::StreamWriter writer(memStream);
			writer.Write(MessageType::Command);
			command.Serialize(writer);
			ClientNetworkManager::Get()->SendMessageToServer(memStream);
		}
		GameObjectManager::Get()->Update(deltaTime);
		GameObjectManager::Get()->Draw();
	}
	else
	{
		gameState = GameState::Disconnected;
	}
}

void UpdateDisconnected()
{
	X::DrawScreenText("Disconnected", 10.0f, 10.0f, 16.0f, X::Math::Vector4::White());
}

bool GameLoop(float deltaTime)
{
	switch (gameState) {
	case GameState::Connecting:
		UpdateConnecting();
		break;
	case GameState::Connected:
		UpdateConnected(deltaTime);
		break;
	case GameState::Disconnected:
		UpdateDisconnected();
		break;
	}
	return X::IsKeyPressed(X::Keys::ESCAPE);
}
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	if (lpCmdLine[0] != 0)
	{
		host = lpCmdLine;
	}

	GameObjectManager::StaticInitialize();
	ClientNetworkManager::StaticInitialize();

	GameObjectManager::Get()->GetFactory().Register<Character>();

	X::Start();
	X::Run(GameLoop);
	X::Stop;

	GameObjectManager::StaticTerminate();
	ClientNetworkManager::StaticTerminate();

	return 0;
}