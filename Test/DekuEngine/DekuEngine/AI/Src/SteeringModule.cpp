#include "Precompiled.h"
#include "SteeringModule.h"

using namespace AI;
SteeringModule::SteeringModule(Agent& agent)
	:mAgent(agent)
{

}

SteeringModule::~SteeringModule()
{
	XASSERT(mBehaviours.empty(), "[SteeringModule]");
}

void SteeringModule::Purge()
{
	mBehaviours.clear();
}

X::Math::Vector2 SteeringModule::Calculate()
{
	X::Math::Vector2 total;
	for (auto& b : mBehaviours)
	{
		if (b->IsActive())
		{
			total += b->Calculate(mAgent);
		}
	}
	return total;
}