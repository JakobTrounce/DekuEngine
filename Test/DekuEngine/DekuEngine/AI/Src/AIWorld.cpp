#include "Precompiled.h"
#include "AIWorld.h"

#include "Agent.h"

using namespace AI;

AIWorld::AIWorld()
{
}
AIWorld::~AIWorld()
{
}

void AIWorld::AddObstacles(const X::Math::Circle& obstacle)
{
	mObstacles.push_back(obstacle);
}

void AIWorld::AddWall(const X::Math::LineSegment& wall)
{
	mWalls.push_back(wall);
}

void AIWorld::UnregisterEntity(Entity* entity)
{
	for (auto it = mEntitys.begin(); it != mEntitys.end(); ++it)
	{
		if ((*it)->GetId() == entity->GetId())
		{
			mEntitys.erase(it);
			break;
		}
	}
}

Entity* AIWorld::GetEntity(int id)
{
	for (auto it = mEntitys.begin(); it != mEntitys.end(); ++it)
	{
		if ((*it)->GetId() == id)
		{
			return *it;
		}
	}
}


bool AIWorld::HasLOS(const X::Math::LineSegment& line) const
{
	for (auto w : GetWalls())
	{
		if (X::Math::Intersect(line, w))
		{
			return false;
		}
	}

	for (auto o : GetObstacles())
	{
		if (X::Math::Intersect(line, o))
		{
			return false;
		}
	}

	return true;
}