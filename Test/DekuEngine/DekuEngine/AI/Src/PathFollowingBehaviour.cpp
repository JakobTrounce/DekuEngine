#include "Precompiled.h"
#include "PathFollowingBehaviour.h"

void AI::PathFollowingBehaviour::SetPath(std::vector < X::Math::Vector2>& path)
{
	mPath = path;
}
X::Math::Vector2 AI::PathFollowingBehaviour::Calculate(Agent & agent)
{
	X::Math::Vector2 desiredVel = (X::Math::Normalize(agent.Destination() - agent.Position())) * agent.MaxSpeed();
	if (current < mPath.size()-1)
	{
		 current++;
		 return desiredVel - agent.Velocity();
	}
	else if(current == mPath.size()-1)
	{
		float distance = X::Math::DistanceSqr(agent.Position(), agent.Destination());
		X::Math::Circle slowRadius
		{
			agent.Destination(),
			100.0f
		};

		if (distance <= slowRadius.radius*slowRadius.radius)
		{
			desiredVel = desiredVel * (distance / (slowRadius.radius*slowRadius.radius));
			if (desiredVel.x > agent.MaxSpeed() || desiredVel.x < -agent.MaxSpeed())
			{
				desiredVel.x = (desiredVel.x > 0) ? agent.MaxSpeed() : agent.MaxSpeed() * -1;
			}
			if (desiredVel.y > agent.MaxSpeed() || desiredVel.y < -agent.MaxSpeed())
			{
				desiredVel.y = (desiredVel.y > 0) ? agent.MaxSpeed() : agent.MaxSpeed() * -1;
			}
		}
	}
	


	return X::Math::Vector2();
}
