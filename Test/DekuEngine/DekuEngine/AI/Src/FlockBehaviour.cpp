#include "Precompiled.h"

#include "FlockBehaviour.h"

using namespace AI;


X::Math::Vector2 FlockBehaviour::Calculate(Agent& agent)
{
	X::Math::Vector2 alignment = CalculateAlignment(agent);
	X::Math::Vector2 cohesion = CalculateCohesion(agent);
	X::Math::Vector2 separation = CalculateSeparation(agent);

	X::Math::Vector2 desiredVel = alignment + cohesion + separation;

	desiredVel = X::Math::Normalize(desiredVel)*agent.MaxSpeed();
	return desiredVel - agent.Velocity();
}

X::Math::Vector2 FlockBehaviour::CalculateAlignment(Agent& agent)
{
	int neighbours = 0;
	X::Math::Vector2 forces;
	auto agentList = agent.World().GetAgentList();

	for (const auto& a : agentList)
	{
		if (a.Position().x != agent.Position().x &&a.Position().y != agent.Position().y )
		{
			float distance = X::Math::Distance(agent.Position(), a.Position());
			if (distance < 300.0f)
			{
				forces += a.Velocity();
				neighbours++;
			}
		}
	}
	if (neighbours == 0)
	{
		return forces;
	}
	else
	{
		forces.x /= neighbours;
		forces.y /= neighbours;
		forces = X::Math::Normalize(forces);
		return forces;
	}
}

X::Math::Vector2 FlockBehaviour::CalculateCohesion(Agent& agent)
{
	int neighbours = 0;
	X::Math::Vector2 forces;
	auto agentList = agent.World().GetAgentList();

	for (const auto& a : agentList)
	{
		if (a.Position().x != agent.Position().x&& a.Position().y != agent.Position().y)
		{
			float distance = X::Math::Distance(agent.Position(), a.Position());
			if (distance < 300.0f)
			{
				forces += a.Position();
				neighbours++;
			}
		}
	}
	if (neighbours == 0)
	{
		return forces;
	}
	else
	{
		forces.x /= neighbours;
		forces.y /= neighbours;
		forces = (forces - agent.Position());
		forces = X::Math::Normalize(forces);
		return forces;
	}
}

X::Math::Vector2 FlockBehaviour::CalculateSeparation(Agent& agent)
{
	int neighbours = 0;
	X::Math::Vector2 forces;
	auto agentList = agent.World().GetAgentList();

	for (const auto& a : agentList)
	{
		if (a.Position().x != agent.Position().x &&a.Position().y != agent.Position().y)
		{
			float distance = X::Math::Distance(agent.Position(), a.Position());
			if (distance < 300.0f)
			{
				forces += a.Position() - agent.Position();
				neighbours++;
			}
		
			if (neighbours == 0)
			{
				return forces;
			}
			else
			{
				forces.x /= neighbours;
				forces.y /= neighbours;
				forces.x *= -1;
				forces.y *= -1;
				forces = X::Math::Normalize(forces);
				return forces;
			}
		}
	}

}