#include "Precompiled.h"
#include "Agent.h"

#include "AIWorld.h"

using namespace AI;

Agent::Agent(AIWorld& world)
	: mWorld(world)
	, mPosition(0.0f, 0.0f)
	, mVelocity(0.0f, 0.0f)
	, mDestination(0.0f, 0.0f)
	, mHeading(0.0f, 1.0f)
	, mMaxSpeed(0.0f)
{
}

Agent::~Agent()
{
}

X::Math::Matrix3 Agent::LocalToWorld() const
{
	return X::Math::Matrix3(
		-mHeading.y, mHeading.x, 0.0f,
		mHeading.x, mHeading.y, 0.0f,
		mPosition.x, mPosition.y, 1.0f
	);
}