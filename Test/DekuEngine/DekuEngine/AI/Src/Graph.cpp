
#include "Precompiled.h"
#include "Graph.h"

using namespace AI;

Graph::Graph()
	:mColumns{ 0 }
	, mRows{ 0 }
	, mNodes{ nullptr }
{

}

Graph::~Graph()
{

}

void Graph::Init(uint32_t col, uint32_t row)
{
	mNodes = std::make_unique<Node[]>(col*row);
	mColumns = col;
	mRows = row;
	for (uint32_t x = 0; x < mColumns; ++x)
	{
		for (uint32_t y = 0; y < mRows; ++y)
		{
			mNodes[x + (y*mColumns)].coord.x = x;
			mNodes[x + (y*mColumns)].coord.y = y;
		}
	}
}

void Graph::Clear()
{
	mNodes.reset();
	mColumns = 0;
	mRows = 0;
}

Graph::Node& Graph::GetNode(Coord coord)
{
	//XASSERT(coord.x < mColumns && coord.y < mRows, "[Graph] Invalid Coordinates");
	return mNodes[coord.x + (coord.y*mColumns)];
}

bool Graph::RunBFS(Coord start,Coord end)
{
	Reset();
	mOpenList.push_back(start);
	GetNode(start).opened = true;

	bool found = false;
	while (!found && !mOpenList.empty())
	{
		Coord current = mOpenList.front();
		mOpenList.pop_front();
		if (current == end)
		{
			found = true;
		}
		else
		{
			for (int x = current.x - 1; x <= current.x+1; ++x)
			{
				for (int y = current.y - 1; y <= current.y+1; ++y)
				{
					if (x >= 0 && x < mColumns && y >= 0 && y < mRows)
					{
						Coord temp = { x, y };
						if (!GetNode(temp).blocked && !GetNode(temp).opened && !GetNode(temp).closed)
						{
							mOpenList.push_back(temp);
							GetNode(temp).opened = true;
							GetNode(temp).parent = &GetNode(current);
						}
					}
				}
			}
			// Loop current.x/y +-1 
			// if not blocked open/close
			// add to open list
		}

		mClosedList.push_back(current);
		GetNode(current).closed = true;
	}
	return found;
}


bool Graph::RunDijkstra(Coord start, Coord end,CostFunc cost)
{
	Reset();
	mOpenList.push_back(start);
	GetNode(start).opened = true;
	GetNode(start).g = 0.0f;
	bool found = false;
	while (!found && !mOpenList.empty())
	{

		Coord current = mOpenList.front();
		mOpenList.remove(current);
		if (current == end)
		{
			found = true;
		}
		else
		{
			for (int x = current.x - 1; x <= current.x + 1; ++x)
			{
				for (int y = current.y - 1; y <= current.y + 1; ++y)
				{
					if (x >= 0 && x < mColumns && y >= 0 && y < mRows)
					{
						Coord neighbour = { x, y };
						if (!GetNode(neighbour).blocked)
						{
							if (!GetNode(neighbour).opened)
							{
								GetNode(neighbour).g = GetNode(current).g + cost(current, neighbour);
								GetNode(neighbour).parent = &GetNode(current);
								GetNode(neighbour).opened = true;
								auto it = mOpenList.begin();
								if (mOpenList.size() != 0)
								{
									while (it != mOpenList.end() && GetNode(neighbour).g > GetNode(*it).g)
									{
										it++;
									}
									mOpenList.insert(it, neighbour);
								}
								else
								{
									mOpenList.insert(it,neighbour);
								}
							}
							else 
							{
								if (GetNode(neighbour).parent != nullptr)
								{
									if (GetNode(current).g < GetNode(neighbour).parent->g)
									{
										GetNode(neighbour).parent = &GetNode(current);
									}
								}
							}
						}
					}
				}
			}
			// Loop current.x/y +-1 
			// if not blocked open/close
			// add to open list
		}

		mClosedList.push_back(current);
		GetNode(current).closed = true;
	}
	return found;
}

bool Graph::RunDijkstra(Coord start, int type, CostFunc cost)
{
	Reset();
	mOpenList.push_back(start);
	GetNode(start).opened = true;
	GetNode(start).g = 0.0f;
	bool found = false;
	while (!found && !mOpenList.empty())
	{
		Coord current = mOpenList.front();
		mOpenList.remove(current);
		if (GetNode(current).entity.GetType() == type)
		{
			found = true;
		}
		else
		{
			for (int x = current.x - 1; x <= current.x + 1; ++x)
			{
				for (int y = current.y - 1; y <= current.y + 1; ++y)
				{
					if (x >= 0 && x < mColumns && y >= 0 && y < mRows)
					{
						Coord neighbour = { x, y };
						if (!GetNode(neighbour).blocked)
						{
							if (!GetNode(neighbour).opened)
							{
								GetNode(neighbour).g = GetNode(current).g + cost(current, neighbour);
								GetNode(neighbour).parent = &GetNode(current);
								GetNode(neighbour).opened = true;
								auto it = mOpenList.begin();
								if (mOpenList.size() != 0)
								{
									while (it != mOpenList.end() && GetNode(neighbour).g > GetNode(*it).g)
									{
										it++;
									}
									mOpenList.insert(it, neighbour);
								}
								else
								{
									mOpenList.insert(it, neighbour);
								}
							}
							else
							{
								if (GetNode(neighbour).parent != nullptr)
								{
									if (GetNode(current).g < GetNode(neighbour).parent->g)
									{
										GetNode(neighbour).parent = &GetNode(current);
									}
								}
							}
						}
					}
				}
			}
			// Loop current.x/y +-1 
			// if not blocked open/close
			// add to open list
		}

		mClosedList.push_back(current);
		GetNode(current).closed = true;
	}
	return true;
}

bool Graph::RunAStar(Coord start, Coord end, CostFunc cost, HeuristicFunc heu)
{
	Reset();
	mOpenList.push_back(start);
	GetNode(start).opened = true;
	bool found = false;
	while (!found && !mOpenList.empty())
	{

		Coord current = mOpenList.front();
		mOpenList.remove(current);
		if (current == end)
		{
			found = true;
		}
		else
		{
			for (int x = current.x - 1; x <= current.x + 1; ++x)
			{
				for (int y = current.y - 1; y <= current.y + 1; ++y)
				{
					if (x >= 0 && x < mColumns && y >= 0 && y < mRows)
					{
						Coord neighbour = { x, y };
						if (!GetNode(neighbour).blocked)
						{
							if (!GetNode(neighbour).opened)
							{
								GetNode(neighbour).g = GetNode(current).g + cost(current, neighbour);
								GetNode(neighbour).h = heu(end, neighbour);
								GetNode(neighbour).parent = &GetNode(current);
								GetNode(neighbour).opened = true;
								auto it = mOpenList.begin();
								if (mOpenList.size() != 0)
								{
									for (; it != mOpenList.end(); ++it)
									{
										if ((GetNode(neighbour).g + GetNode(neighbour).h) < (GetNode(*it).g + GetNode(*it).h))
											break;
									}
									//while (it != mOpenList.end() || (GetNode(neighbour).g + GetNode(neighbour).h) > ( GetNode(*it).g+GetNode(*it).h))
									//{
									//	it++;
									//}
									mOpenList.insert(it, neighbour);
								}
								else
								{
									mOpenList.insert(it,neighbour);
								}
							}
							else
							{
								if (GetNode(neighbour).parent != nullptr)
								{
									float newG = cost(current, neighbour) + GetNode(current).g;
									if (newG < GetNode(neighbour).g)
									{
										GetNode(neighbour).parent = &GetNode(current);
										GetNode(neighbour).g = newG;

										mOpenList.sort([this](const Coord& first, const Coord& second)
										{
											float one = GetNode(first).g + GetNode(first).h;
											float two = GetNode(second).g + GetNode(second).h;

											return one < two;
										});
									}
								}
							}
						}
					}
				}
			}
			// Loop current.x/y +-1 
			// if not blocked open/close
			// add to open list
		}

		mClosedList.push_back(current);
		GetNode(current).closed = true;
	}
	return found;
}


bool Graph::RunDFS(Coord start, Coord end)
{
	Reset();
	mOpenList.push_back(start);
	GetNode(start).opened = true;

	bool found = false;
	while (!found && !mOpenList.empty())
	{
		Coord current = mOpenList.back();
		mOpenList.pop_back();
		if (current == end)
		{
			found = true;
		}
		else
		{
			for (int x = current.x - 1; x <= current.x + 1; ++x)
			{
				for (int y = current.y - 1; y <= current.y + 1; ++y)
				{
					if (x >= 0 && x < mColumns && y >= 0 && y < mRows)
					{
						Coord temp = { x, y };
						if (!GetNode(temp).blocked && !GetNode(temp).opened && !GetNode(temp).closed)
						{
							mOpenList.push_back(temp);
							GetNode(temp).opened = true;
							GetNode(temp).parent = &GetNode(current);
						}
					}
				}
			}
			// Loop current.x/y +-1 
			// if not blocked open/close
			// add to open list
		}

		mClosedList.push_back(current);
		GetNode(current).closed = true;
	}
	return found;
}
std::vector<Graph::Coord> Graph::GetPath()
{
	std::vector<Graph::Coord> temp;
	Coord current = mClosedList.back();
	Coord start = mClosedList.front();
	temp.push_back(current);
	while (!(current == start))
	{
		current = GetNode(current).parent->coord;
		temp.push_back(current);
	} 
	std::reverse(temp.begin(), temp.end());
	
	return temp;
}

void Graph::Reset()
{
	for (uint32_t i = 0; i < mColumns*mRows; ++i)
	{
		mNodes[i].g = 0.0f;
		mNodes[i].h = 0.0f;
		mNodes[i].opened = false;
		mNodes[i].closed = false;
		mNodes[i].parent = nullptr;
	}
	mOpenList.clear();
	mClosedList.clear();
}

void Graph::DrawGraph()
{
	for (int x = 0; x <= mRows * 32; x += 32.0f)
	{
		for (int y = 0; y <= mColumns * 32; y += 32.0f)
		{
			X::DrawScreenLine(0.0f, (float)y, (float)x, (float)y, X::Math::Vector4::Magenta());
			X::DrawScreenLine((float)x, 0.0f, (float)x, (float)y, X::Math::Vector4::Magenta());
		}
	}
}

std::vector<Graph::Coord>Graph::GetTotalPath()
{
	std::vector<Graph::Coord> path;
	for (auto it = mClosedList.begin(); it != mClosedList.end(); ++it)
	{
		path.push_back(*it);
	}


	return path;
}

void Graph::CreateWalls(int map[])
{
	for (int x = 0; x < mColumns; ++x)
	{
		for (int y = 0; y < mRows; ++y)
		{
			if(map[x + y * mColumns] != 1 && map[x + y * mColumns] != 0)
			{
				mNodes[x + (y*mColumns)].blocked = true;
			}
		}
	}
			
}

void AI::Graph::ResetWalls()
{
	for (uint32_t i = 0; i < mColumns*mRows; ++i)
	{
		mNodes[i].blocked = false;
	}
}

X::Math::Vector2 AI::Graph::ConvertToVector2(AI::Graph::Coord coord)
{
	return {(float)coord.x,(float)coord.y};
}

Graph::Coord AI::Graph::ConvertToCoord(X::Math::Vector2 vector)
{
	Coord coord = { (int)vector.x,(int)vector.y };
	return coord;
}

Graph::Node Graph::GetClosestNode(X::Math::Vector2 pos)
{
	Node closest;
	closest.coord = { 0,0 };
	for (int i = 0; i < mColumns*mRows; ++i)
	{
		if (X::Math::Distance(pos, { (float)closest.coord.x,(float)closest.coord.y }) < X::Math::Distance(pos, { (float)mNodes[i].coord.x,(float)mNodes[i].coord.y }))
		{
			closest.coord = mNodes[i].coord;
		}

	}
	return closest;
}


