#include "Precompiled.h"
#include "FleeBehaviour.h"

X::Math::Vector2 AI::FleeBehaviour::Calculate(Agent & agent)
{
	X::Math::Vector2 desired = X::Math::Normalize(agent.Position() - agent.Destination())*agent.MaxSpeed();

	return desired - agent.Velocity();
}
