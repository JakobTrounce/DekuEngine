#include"precompiled.h"
#include "PathPlanner.h"

using namespace AI;



float CalculateCost(AI::Graph::Coord current, AI::Graph::Coord neighbour)
{
	int cost = (current.x - neighbour.x) + (current.y - neighbour.y);
	cost = abs(cost);
	if (cost == 2)
	{
		return 1.4f;
	}
	return 1;
}


float CalculateHeuristic(AI::Graph::Coord end, AI::Graph::Coord neighbour)
{
	int x = (neighbour.x - end.x);
	int y = (neighbour.y - end.y);
	float use_x = static_cast<float>(abs(x));
	float use_y = static_cast<float>(abs(y));

	return 1.0f*(use_x + use_y) + (1.41f - 2.0f*1.0f)*std::min(use_x, use_y);
}



void PathPlanner::RequestPath(const X::Math::Vector2& destination)
{
	if (mAgent.World().HasLOS({ mAgent.Position(), destination }))
	{
		mPath.push_back(mAgent.Position());
		mPath.push_back(destination);

		return;
	}
	else
	{
		Graph::Node agent;
		Graph::Node dest;
		Graph::Coord agentCoord;
		agent = mAgent.World().GetNavGraph().GetClosestNode(mAgent.Position());
		dest = mAgent.World().GetNavGraph().GetClosestNode(destination);
		if(!mAgent.World().GetNavGraph().RunAStar(agent.coord, dest.coord, CalculateCost, CalculateHeuristic))
		{
			return;
		}
		else
		{
			for (auto it = mAgent.World().GetNavGraph().GetPath().begin(); it != mAgent.World().GetNavGraph().GetPath().end(); ++it)
			{
				mPath.push_back(mAgent.World().GetNavGraph().ConvertToVector2(*it));
			}
			auto it = mPath.begin();
			mPath.emplace(it, mAgent.Position());
		}

	}
}

void PathPlanner::RequestPath(int type)
{
	std::vector<Entity*> entitylist = mAgent.World().GetEntitys();
	for (auto it = entitylist.begin(); it != entitylist.end(); ++it)
	{
		if ((*it)->GetType() == type)
		{
			break;
		}
	}
	Graph::Node agent = mAgent.World().GetNavGraph().GetClosestNode(mAgent.Position());
	if (!mAgent.World().GetNavGraph().RunDijkstra(agent.coord,type, CalculateCost))
	{
		return;
	}
	else
	{
		for (auto it = mAgent.World().GetNavGraph().GetPath().begin(); it != mAgent.World().GetNavGraph().GetPath().end(); ++it)
		{
			mPath.push_back(mAgent.World().GetNavGraph().ConvertToVector2(*it));
		}
		auto it = mPath.begin();
		mPath.emplace(it,mAgent.Position());
	}
}
