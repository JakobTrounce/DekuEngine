#include "Precompiled.h"
#include "ArriveBehaviour.h"

using namespace AI;


X::Math::Vector2 ArriveBehaviour::Calculate(Agent& agent)
{

	X::Math::Vector2 desiredVel = X::Math::Normalize(agent.Destination() - agent.Position()) * agent.MaxSpeed();
	float distance = X::Math::DistanceSqr(agent.Position(), agent.Destination());
	X::Math::Circle slowRadius
	{
		agent.Destination(),
		100.0f
	};

	if (distance <= slowRadius.radius*slowRadius.radius)
	{
		desiredVel = desiredVel *(distance / (slowRadius.radius*slowRadius.radius));
		if (desiredVel.x > agent.MaxSpeed() || desiredVel.x < -agent.MaxSpeed())
		{
			desiredVel.x = (desiredVel.x > 0) ? agent.MaxSpeed() : agent.MaxSpeed() * -1;
		}
		if (desiredVel.y > agent.MaxSpeed() || desiredVel.y < -agent.MaxSpeed())
		{
			desiredVel.y = (desiredVel.y > 0) ? agent.MaxSpeed() : agent.MaxSpeed() * -1;
		}
	}
	else
	{
		desiredVel =  desiredVel;
	}

	return (desiredVel - agent.Velocity());
}