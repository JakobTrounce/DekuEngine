#ifndef INCLUDED_AI_STEERINGBEHAVIOUR_H
#define INCLUDED_AI_STEERINGBEHAVIOUR_H
#include <XEngine.h>
namespace AI
{
class Agent;
class SteeringBehaviour
{
	
public:
	SteeringBehaviour() : mActive(false) {}
	virtual ~SteeringBehaviour() {}

	virtual X::Math::Vector2 Calculate(Agent& agent) = 0;
	virtual const char* GetName() const = 0;

	void SetActive(bool active) { mActive = active; }
	bool IsActive() const { return mActive; }
private:
	bool mActive;
};
} // namespace AI




#endif