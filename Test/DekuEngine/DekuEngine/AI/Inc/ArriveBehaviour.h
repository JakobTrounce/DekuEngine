#ifndef INCLUDED_ARRIVEBEHAVIOUR_H
#define INCLUDED_ARRIVEBEHAVIOUR_H

#include "SteeringBehaviour.h"
#include "Agent.h"

namespace AI {

	class ArriveBehaviour : public SteeringBehaviour
	{
	public:
		ArriveBehaviour() {}

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "Arrive"; }
	};
} // namespace AI
#endif