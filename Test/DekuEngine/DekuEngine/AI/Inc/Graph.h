#ifndef INCLUDED_AI_GRAPH_H
#define INCLUDED_AI_GRAPH_H
#include "Entity.h"
namespace AI
{

class Graph
{
public:
	struct Coord
	{
		uint32_t x, y;

		bool operator==(const Coord& other) const
		{
			return x == other.x && y == other.y;
		}
	};
	using CostFunc = std::function<float(Coord, Coord)>;
	using HeuristicFunc = std::function<float(Coord, Coord)>;
	struct Node
	{
		Coord coord;
		Node* parent{ nullptr };
		bool opened{ false };
		bool closed{ false };
		bool blocked{ false };
		float g{0.0f};
		float h{0.0f};
		Entity entity;
	};
	Graph();
	~Graph();
	void Init(uint32_t col, uint32_t row);
	void Clear();

	Node& GetNode(Coord coord);

	bool RunBFS(Coord start, Coord end);
	bool RunDFS(Coord start, Coord end);
	bool RunDijkstra(Coord start, Coord end,CostFunc cost);
	bool RunDijkstra(Coord start, int type, CostFunc cost);
	bool RunAStar(Coord start, Coord end,CostFunc cost,HeuristicFunc heu);


	Node GetClosestNode(X::Math::Vector2 pos);
	std::vector<Coord> GetPath();
	std::vector<Coord> GetTotalPath();
	uint32_t GetColumns() const { return mColumns; }
	uint32_t GetRows() const { return mRows; }
	void DrawGraph();
	void CreateWalls(int[]);
	void ResetWalls();

	X::Math::Vector2 ConvertToVector2(AI::Graph::Coord coord);
	Coord ConvertToCoord(X::Math::Vector2);

private:
	void Reset();
	std::unique_ptr<Node[]> mNodes;
	uint32_t mColumns;
	uint32_t mRows;
	std::list<Coord> mOpenList;
	std::list<Coord> mClosedList;
	



};

} // namespace AI

#endif
