#ifndef INCLUDED_AI_FLEEBEHAVIOUR_H
#define INCLUDED_AI_FLEEBEHAVIOUR_H


#include "SteeringBehaviour.h"
#include "Agent.h"

namespace AI {

	class FleeBehaviour : public SteeringBehaviour
	{
	public:
		FleeBehaviour() {}

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "Flee"; }
	};
} // namespace AI

#endif // #include INCLUDED_AI_FLEEBEHAVIOR_H