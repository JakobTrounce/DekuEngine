#ifndef INCLUDED_AI_AIWORLD_H
#define INCLUDED_AI_AIWORLD_H

#include "Agent.h"
#include "Graph.h"
#include "Entity.h"

namespace AI {

class AIWorld
{
public:
	typedef std::vector<X::Math::Circle> Obstacles;
	typedef std::vector<X::Math::LineSegment> Walls;

	AIWorld();
	~AIWorld();

	void AddObstacles(const X::Math::Circle& obstacle);
	const Obstacles& GetObstacles() const { return mObstacles; }

	void AddWall(const X::Math::LineSegment& wall);
	const Walls& GetWalls() const { return mWalls; }

	Graph& GetNavGraph()				{ return mNavGraph; }
	const Graph& GetNavGraph() const	{ return mNavGraph; }

	std::vector<Agent>& GetAgentList() { return mAgentList; }
	const std::vector<Agent> GetAgentList() const { return mAgentList; }

	void RegisterEntity(Entity* entity) { mEntitys.push_back(entity); }
	void UnregisterEntity(Entity* entity);

	std::vector<Entity*> GetEntitys() { return mEntitys; }
	const std::vector<Entity*> GetEntitys() const { return mEntitys; }

	Entity* GetEntity(int id);

	bool HasLOS(const X::Math::LineSegment& line)const;
private:
	Obstacles mObstacles;
	Walls mWalls;
	Graph mNavGraph;
	bool mInitialized;
	std::vector<Agent> mAgentList;
	std::vector<Entity*> mEntitys;
};

} // namespace AI

#endif // #include INCLUDED_AI_AIWORLD_H