#ifndef INCLUDED_AI_STATE_H
#define INCLUDED_AI_STATE_H

namespace AI
{
	template <typename AgentType>
	class State
	{
	public:
		virtual ~State() {}
		virtual void Enter(AgentType& agent) = 0;
		virtual void Update(AgentType& agent, float deltaTime) = 0;
		virtual void Exit(AgentType& agent) = 0;
	};
}
#endif // !INCLUDED_AI_STATE_H
