#ifndef INCLUDED_PATHPLANNING_H
#define INCLUDED_PATHPLANNING_H
#include "Graph.h"
#include "Agent.h"
#include "AIWorld.h"
namespace AI
{

	class PathPlanner


	{
	public:
		void RequestPath(const X::Math::Vector2& destination);
		void RequestPath(int type);

		const std::vector<X::Math::Vector2>& GetPath() const { return mPath; }
	private:
		Agent & mAgent;
		std::vector<X::Math::Vector2> mPath;
	};
}




#endif
