#ifndef INCLUDED_FLOCKBEHAVIOUR_H
#define INCLUDED_FLOCKBEHAVIOUR_H
#include "SteeringBehaviour.h"
#include "Agent.h"
#include "AIWorld.h"

namespace AI {

	class FlockBehaviour : public SteeringBehaviour
	{
	public:
		FlockBehaviour() {}

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "Flock"; }

	private:
		X::Math::Vector2 CalculateAlignment(Agent& agent);
		X::Math::Vector2 CalculateSeparation(Agent& agent);
		X::Math::Vector2 CalculateCohesion(Agent& agent);
	};
} // namespace AI

#endif
