#ifndef INCLUDED_PATHFOLLOWINGBEHAVIOUR_H
#define INCLUDED_PATHFOLLOWINGBEHAVIOUR_H
#include "SteeringBehaviour.h"

#include "AIWorld.h"

class Agent;


namespace AI
{
	class PathFollowingBehaviour : public SteeringBehaviour
	{
		PathFollowingBehaviour();
		~PathFollowingBehaviour();

		void SetPath(std::vector<X::Math::Vector2>& path);

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "PathFollowing"; }
	private:
		int current = 0;
		std::vector<X::Math::Vector2> mPath;
	};
}




#endif
