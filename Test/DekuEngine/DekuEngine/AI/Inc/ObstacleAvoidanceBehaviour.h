#ifndef INCLUDED_AI_OBSTACLEAVOIDANCEBEHAVIOUR_H
#define INCLUDED_AI_OBSTACLEAVOIDANCEBEHAVIOUR_H


#include "SteeringBehaviour.h"
#include "Agent.h"
#include"AIWorld.h"

namespace AI {

	class ObstacleAvoidanceBehaviour : public SteeringBehaviour
	{
	public:
		ObstacleAvoidanceBehaviour() {}

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "ObstacleAvoidance"; }

	private:
		X::Math::Circle GetNearestObstacle(Agent& agent, const X::Math::Vector2& ahead);
	};
} // namespace AI

#endif // #include INCLUDED_AI_OBSTACLEAVOIDANCEBEHAVIOR_H