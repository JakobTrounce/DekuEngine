#ifndef INCLUDED_ENTITY_H
#define INCLUDED_ENTITY_H
namespace AI
{

	enum class EntityType
	{
		health = 0,
		armor,

	};
	class Entity
	{
	public:
		Entity() {};
		Entity(X::Math::Vector2 pos, int type, int id);
		~Entity() {};

		X::Math::Vector2& Position() { return mPosition; }
		const X::Math::Vector2& Position() const { return mPosition; }

		const int GetId() const { return mId; }
		const int GetType() const { return mType; }
	protected:
		X::Math::Vector2 mPosition;
		int mType;
		int mId;

	};
}




#endif
