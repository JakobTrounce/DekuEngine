#ifndef INCLUDED_AI_SEEKBEHAVIOR_H
#define INCLUDED_AI_SEEKBEHAVIOR_H

#include "SteeringBehaviour.h"
#include "Agent.h"

namespace AI {

	class SeekBehaviour : public SteeringBehaviour
	{
	public:
		SeekBehaviour();

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "Seek"; }
	};
} // namespace AI

#endif // #include INCLUDED_AI_SEEKBEHAVIOR_H