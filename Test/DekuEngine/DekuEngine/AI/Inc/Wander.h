#ifndef INCLUDED_AI_WANDERBEHAVIOUR_H
#define INCLUDED_AI_WANDERBEHAVIOUR_H


#include "SteeringBehaviour.h"
#include "Agent.h"

namespace AI {

	class WanderBehaviour : public SteeringBehaviour
	{
	public:
		WanderBehaviour() {}

		X::Math::Vector2 Calculate(Agent& agent) override;
		const char* GetName() const override { return "Wander"; }
	};
} // namespace AI

#endif // #include INCLUDED_AI_FLEEBEHAVIOR_H
