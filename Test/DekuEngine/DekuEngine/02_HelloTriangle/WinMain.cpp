#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>

using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Math;

void ChangeVertices(const std::vector<Vertex>& vertices, VertexShader& vs, MeshBuffer& mb);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	
	// setup our application window
	Deku::Core::Window myWindow;
	myWindow.Initialize(hInstance, "Hello Triangle",1280, 720);

	//initialize the graphics system
	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);


	
	// define our vertex data
	const std::vector<Vertex> triforce =
	{
		// triforce
		{{0.0f,0.9f,0.0f}, Vector4::Yellow()},
		{{0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{ {-0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{{0.1f,0.6f,0.0f}, Vector4::Yellow()},
		{{0.2f, 0.3f,0.0f}, Vector4::Yellow()},
		{{0.0f, 0.3f, 0.0f}, Vector4::Yellow()},
		{{-0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{{0.0f,0.3f,0.0f}, Vector4::Yellow()},
		{{-0.2f,0.3f,0.0f}, Vector4::Yellow()},
	};
	
	std::vector<Vertex> Star =
	{
		{{0.3f,0.95f,0.0f}},
		{{0.3f,-0.95f,0.0f}},
		{{-0.375f,0.0f,0.0f}},

		{{1.0f,0.0f,0.0f}},
		{{-0.81f,-0.6f,0.0f}},
		{{-0.125f,0.37,0.0f}},

		{{-0.81f,0.6f,0.0f}},
		{{1.0f,0.0f,0.0f}},
		{{-0.125f,-0.37f,0.0f}}
	};

	std::vector<Vertex> square =
	{
		{{0.0f, 0.0f,0.0f}, Vector4::Green()},
		{{0.6f, 0.0f,0.0f}, Vector4::Cyan()},
		{{0.6f, -0.6f,0.0f}, Vector4::Yellow()},
		{{0.0f,0.0f,0.0f}, Vector4::Yellow()},
		{{0.6f,-0.6f,0.0f}, Vector4::Cyan()},
		{{0.0f,-0.6f,0.0f}, Vector4::Green()}
	};

	std::vector<Vertex> Mirrored = 
	{
		{{0.0f,0.9f,0.0f}, Vector4::Yellow()},
		{{0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{ {-0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{{0.1f,0.6f,0.0f}, Vector4::Yellow()},
		{{0.2f, 0.3f,0.0f}, Vector4::Yellow()},
		{{0.0f, 0.3f, 0.0f}, Vector4::Yellow()},
		{{-0.1f, 0.6f,0.0f}, Vector4::Yellow()},
		{{0.0f,0.3f,0.0f}, Vector4::Yellow()},
		{{-0.2f,0.3f,0.0f}, Vector4::Yellow()},

		{{0.0f,-0.3f,0.0f}, Vector4::Black()},
		{{-0.1f, 0.0f,0.0f}, Vector4::Violet()},
		{ {0.1f, 0.0f,0.0f}, Vector4::Violet()},
		{{-0.1f,0.0f,0.0f}, Vector4::Black()},
		{{-0.2f, 0.3f,0.0f}, Vector4::Violet()},
		{{0.0f, 0.3f, 0.0f}, Vector4::Violet()},
		{{0.1f, 0.0f,0.0f}, Vector4::Black()},
		{{0.0f,0.3f,0.0f}, Vector4::Violet()},
		{{0.2f,0.3f,0.0f}, Vector4::Violet()}


	};

	std::vector<Vertex> boat = 
	{
		// middle base
		{{0.0f,0.2f,0.0f}, Vector4::Green()},
		{{0.2f,-0.2f,0.0f}, Vector4::Green()},
		{{-0.2f, -0.2f, 0.0f}, Vector4::Green()},

		// right base
		{{0.0f,0.2f,0.0f}, Vector4::Green()},
		{{0.4f,0.2f,0.0f}, Vector4::Green()},
		{{0.2f, -0.2f, 0.0f}, Vector4::Green()},

		//left base
		{{0.0f,0.2f,0.0f}, Vector4::Green()},
		{{-0.2f,-0.2f,0.0f}, Vector4::Green()},
		{{-0.4f, 0.2f, 0.0f}, Vector4::Green()},

		//top right
		{{-0.2f,0.6f,0.0f}, Vector4::Blue()},
		{{0.0f,0.3f,0.0f}, Vector4::Blue()},
		{{-0.2f, 0.3f, 0.0f}, Vector4::Blue()},
		//top left
		{{0.0f,0.6f,0.0f}, Vector4::Blue()},
		{{0.0f,0.3f,0.0f}, Vector4::Blue()},
		{{-0.2f, 0.3f, 0.0f}, Vector4::Blue()},
		
		//top right
		{{0.0f,0.6f,0.0f}, Vector4::Blue()},
		{{0.2f,0.3f,0.0f}, Vector4::Blue()},
		{{0.0f, 0.3f, 0.0f}, Vector4::Blue()},
		//top left
		{{0.2f,0.6f,0.0f}, Vector4::Blue()},
		{{0.2f,0.3f,0.0f}, Vector4::Blue()},
		{{0.0f, 0.3f, 0.0f}, Vector4::Blue()},

		// flagpole

		//{{0.0f,-0.6f,0.0f}, Vector4::Orange()},
		//{{0.0f,-0.3f,0.0f}, Vector4::Orange()},
		//{{0.025f, -0.45f, 0.0f}, Vector4::Orange()},
		//
		//{{0.0f,-0.1f,0.0f}, Vector4::Orange()},
		//{{0.025f,-0.45f,0.0f}, Vector4::Orange()},
		//{{0.0f, -0.3f, 0.0f}, Vector4::Orange()}

	};

	VertexShader vs;
	PixelShader ps;
	MeshBuffer mb;

	mb.Vertices(triforce);
	mb.Initialize(triforce);
	vs.Initialize();
	ps.Initialize();
	bool done = false;
	while (!done)
	{
		done = myWindow.ProcessMessage();
		done = GetAsyncKeyState(VK_ESCAPE);
		//Run Game Logic

		GraphicsSystem::Get()->BeginRender();

		if (GetAsyncKeyState(0x53))
		{
			ChangeVertices(Star, vs, mb);
		}
		else if (GetAsyncKeyState(0x54))
		{
			ChangeVertices(triforce, vs, mb);
		}
		else if (GetAsyncKeyState(0x4D))
		{
			ChangeVertices(Mirrored, vs, mb);
		}
		else if (GetAsyncKeyState(0x52))
		{
			ChangeVertices(square, vs, mb);
		}
		else if (GetAsyncKeyState(0x42))
		{
			ChangeVertices(boat, vs, mb);
		}

		vs.Bind();
		ps.Bind();
		mb.Render();

		GraphicsSystem::Get()->EndRender();
	}

	GraphicsSystem::StaticTerminate();
	vs.Terminate();
	ps.Terminate();
	myWindow.Terminate();
	return 0;
}

void ChangeVertices(const std::vector<Vertex>& vertices, VertexShader& vs, MeshBuffer& mb)
{
	mb.Vertices(vertices);
	mb.Initialize(vertices);
}
