#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <cstring>
#pragma comment(lib,"Ws2_32.lib")

std::string StarWarsSpeech();
int main(int argc, char* argv[])
{
	uint16_t port = 8888;
	printf("Hello World\n");

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	


	// create a socket using TCP/IP
	//using TCP means you need a specific address to connect
	//socket under the hood is just an ID
	SOCKET listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//have to bind socket to an address

	// fill server address info
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET; // address family or format

	// the address format tells these how to read the ip
	serverInfo.sin_addr.s_addr = INADDR_ANY; // check for any ip
	serverInfo.sin_port = htons(port); //  connection port

	printf("Listening on port %hu...\n", port);

	//bind the address to our listener socket and listen for connection
	bind(listener, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));
	listen(listener, 10); //  create how many connections the listener can queue up until they start getting dropped

	char buffer[1024];
	// server forever...
	while (true)
	{
		//accept any incoming client connection **blocking**
		// why this is blocking
		//// the application hits a halt at this line, it must wait for a response from the network card
		// this is called a blocking call
		
		SOCKET client = accept(listener, NULL, NULL);
		if (client == WSAEINVAL)
		{
			std::cout << "named Socket not listening" << std::endl;
		}
		
		// we do this
		// recv
		//send
		// closesocket

		std::string response;
		int bytesReceived = recv(client, buffer, std::size(buffer) - 1, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			printf("recv failed.\n");
			return -1;
		}
		if (bytesReceived == 0)
		{
			printf("Connection closed.\n");
			return 0;
		}
		else
		{
			buffer[bytesReceived] = '\0';
			if(buffer == "Tragedy")
			{
				std::cout << "plaguies";
				response = StarWarsSpeech();
			}
			else if (buffer == "")
			{
				response = "I can't hear you";
			}
			else if (buffer == "Lime")
			{
				response = "You put the lime in the coconut";
			}
			else
			{
				response = StarWarsSpeech();
			}
			printf("Received: %s\n", buffer);
			std::cout << "Sending: " << response << std::endl;
			int bytesSent = send(client, response.c_str(), (int)response.length(), 0);
		}
		
		closesocket(client);
	}

	//close all sockets
	closesocket(listener);

	//Shutdown Winsock
	WSACleanup();
	return 0;
}

std::string StarWarsSpeech()
{
	return "I thought not. It's not a story the Jedi would tell you. It's a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life... He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful... the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic, he could save others from death, but not himself.";
}