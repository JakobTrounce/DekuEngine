#pragma once

#include "TopDown/ActionGame.h"
#include "Network/Inc/Network.h"
#include <XEngine.h>


class ServerNetworkManager
{
public:
	static void StaticInitialize();
	static void StaticTerminate();
	static ServerNetworkManager* Get();

	ServerNetworkManager();
	~ServerNetworkManager();

	void Run(uint16_t port);

private:
	void HandleNewClients();
	void ProcessClientMessage();
	void BroadcastCreate(GameObject* gameObject);
	void BroadcastWorldUpdate();

	struct Client
	{
		Network::TCPSocket* clientSocket;
		uint32_t networkId;
	};

	std::vector<Client> mClients;
	Network::TCPSocket mListener;
 };