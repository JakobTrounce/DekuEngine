#include "ServerNetworkManager.h"

namespace
{
	ServerNetworkManager* sInstance = nullptr;
}

void ServerNetworkManager::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[ServerNetworkManager] Manager already initialized");
	sInstance = new ServerNetworkManager();
}

void ServerNetworkManager::StaticTerminate()
{
	XASSERT(sInstance != nullptr, "[ServerNetworkManager] Manager already Terminated");
	X::SafeDelete(sInstance);
}

ServerNetworkManager* ServerNetworkManager::Get()
{
	XASSERT(sInstance != nullptr, "[ServerNetworkManager] Manager Not initialized");
	return sInstance;
}


ServerNetworkManager::ServerNetworkManager()
{

}

ServerNetworkManager::~ServerNetworkManager()
{

}

void ServerNetworkManager::StaticInitialize()
{
	Network::Initialize();
}

void ServerNetworkManager::StaticTerminate()
{
	Network::Terminate();
}

void ServerNetworkManager::Run(uint16_t port)
{
	if (!mListener.Open() || !mListener.SetNoDelay(true) || !mListener.SetNonBlocking(true))
	{
		return;
	}

	Network::SocketAddress serverInfo(port);
	if (!mListener.Bind(serverInfo) || !mListener.Listen())
	{
		return;
	}

	while (true)
	{
		HandleNewClients();

		ProcessClientMessage();

		GameObjectManager::Get()->Update(0.01f);

		BroadcastWorldUpdate();
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	mListener.Close();
}

void ServerNetworkManager::HandleNewClients()
{
	Network::SocketAddress address;
	Network::TCPSocket* clientSocket = mListener.Accept(address);
	if (clientSocket == nullptr)
	{
		return;
	}

	const int clientId = (int)mClients.size();

	printf("client %d has joined \n", clientId);

	

	// send client snapshot
	Network::MemoryStream memStream;
	Network::StreamWriter writer(memStream);
	writer.Write(MessageType::Snapshot);
	GameObjectManager::Get()->Serialize(writer, false);
	clientSocket->Send(memStream.GetData(), memStream.GetHead());

	//create new plater for client

	Character* newCharacter = (Character*)GameObjectManager::Get()->CreateGameObject(Character::ClassId);
	memStream.Reset();
	writer.Write(MessageType::Assignment);
	writer.Write(newCharacter->GetNetworkId());
	clientSocket->Send(memStream.GetData(), memStream.GetHead());

	// add new client
	Client newClient;
	newClient.clientSocket = clientSocket;
	mClients.push_back(newClient);

	newCharacter->SetPosition({ X::RandomFloat(100.0f,1170.0f),X::RandomFloat(100.0f,620.0f)});
	BroadcastCreate(newCharacter);

	


}

void ServerNetworkManager::ProcessClientMessage()
{
	for (size_t i = 0; i < mClients.size(); ++i)
	{
		Client& client = mClients[i];
		if (client.clientSocket == nullptr)
		{
			continue;
		}
		uint8_t buffer[16384];
		int bytesReceived = client.clientSocket->Receive(buffer, (int)std::size(buffer));
		if(bytesReceived > 0)
		{
			Network::MemoryStream memStream(buffer, bytesReceived);
			Network::StreamReader reader(memStream);
			while(reader.GetRemainingDataSize())
			{
				uint32_t messageType;
				reader.Read(messageType);
				switch (MessageType(messageType))
				{
				case MessageType::Command:
					Command command;
					command.Deserialize(reader);
					Character* myCharacter = (Character*)GameObjectManager::Get()->FindGameObject(client.networkId);
					myCharacter->Apply(command);
					break;
				}
			}
		}
		else if (bytesReceived == SOCKET_ERROR)
		{
			mClients[i].clientSocket->Close();
			X::SafeDelete(mClients[i].clientSocket);
		}
	}
}

void ServerNetworkManager::BroadcastCreate(GameObject* gameObject)
{
	Network::MemoryStream memStream;
	Network::StreamWriter writer(memStream);
	writer.Write(MessageType::Create);
	writer.Write(gameObject->GetClassId());
	writer.Write(gameObject->GetNetworkId());
	gameObject->Serialize(writer);

	for (auto& client : mClients)
	{
		if (client.clientSocket != nullptr)
		{
			client.clientSocket->Send(memStream.GetData(), memStream.GetHead());
		}
	}
}

void ServerNetworkManager::BroadcastWorldUpdate()
{
	Network::MemoryStream memStream;
	Network::StreamWriter writer(memStream);
	writer.Write(MessageType::Update);
	GameObjectManager::Get()->Serialize(writer,true);
	if (memStream.GetHead <= 4)
		return;

	for (auto& client : mClients)
	{
		if (client.clientSocket != nullptr)
		{
			client.clientSocket->Send(memStream.GetData(), memStream.GetHead());
		}
	}
}