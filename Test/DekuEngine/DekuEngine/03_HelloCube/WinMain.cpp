#include <Core/Inc/Core.h>
#include <Graphics/Inc/Graphics.h>



using namespace Deku::Core;
using namespace Deku::Graphics;
using namespace Deku::Math;




int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	
	// setup our application window
	Deku::Core::Window myWindow;
	myWindow.Initialize(hInstance, "Hello Cube",1280, 720);
	std::vector<uint32_t> indices;
	//initialize the graphics system
	GraphicsSystem::StaticInitialize(myWindow.GetWindowHandle(), false);



	
	MeshPC mesh;
	MeshBuffer earthBuffer;
	VertexShader earthVs;
	PixelShader earthPs;
	ConstantBuffer constantEarthBuffer;
	Texture earth;
	Sampler earthSampler;


	Camera camera;
	float speed = 1.0f;
	camera.SetPosition({ 0.0f,0.0f,-10.0f });
	camera.SetDirection({ 0.0f,0.0f,1.0f });

	mesh = MeshBuilder::CreateCube();
	earthBuffer.Initialize(mesh.mVertices.data(), (int)sizeof(VertexPC), mesh.mVertices.size(), mesh.mIndices.data(), mesh.mIndices.size());
	earthVs.Initialize("../Assets/Shaders/DoTransform.fx", VertexPC::Format);
	earthPs.Initialize("../Assets/Shaders/DoTransform.fx");
	constantEarthBuffer.Initialize(3, sizeof(Matrix4));



	float thetaX = 0.0f;
	float thetaY = 0.0f;
	float thetaZ = 0.0f;
	bool done = false;
	while (!done)
	{
		// all use one constant buffer, pixelshader, vertexshader
		// matrix multiply to get specific behaviours
		
		// ie Re, RmTm
		//

		
		
		Matrix4 matRotx = Matrix4::RotateMatrixX(thetaX * Constants::DegToRad);
		Matrix4 matRoty = Matrix4::RotateMatrixY(thetaY * Constants::DegToRad);
		Matrix4 matRotz = Matrix4::RotateMatrixZ(0.0f);
		
	;
		done = myWindow.ProcessMessage();
		done = GetAsyncKeyState(VK_ESCAPE);
		//Run Game Logic


		//camera

		if (GetAsyncKeyState(0x57))
		{
			camera.Walk(speed);
		}
		else if(GetAsyncKeyState(0x53))
		{
			camera.Walk(-speed);
		}
		if (GetAsyncKeyState(0x41))
		{
			camera.Strafe(-speed);
		}
		else if (GetAsyncKeyState(0x44))
		{
			camera.Strafe(speed);
		}
		if (GetAsyncKeyState(0x46))
		{
			camera.Yaw(speed* 0.005);
		}
		else if (GetAsyncKeyState(0x52))
		{
			camera.Yaw(-speed * 0.005);
		}
		else if (GetAsyncKeyState(VK_SHIFT))
		{
			camera.Rise(-speed);
		}
		else if (GetAsyncKeyState(VK_SPACE))
		{
			camera.Rise(speed);
		}

		

		if (GetAsyncKeyState(VK_UP))
		{
			thetaX += 0.5f;
		}
		else
		if (GetAsyncKeyState(VK_DOWN))
		{
			thetaX -= 0.5f;
		}
		if (GetAsyncKeyState(VK_RIGHT))
		{
			thetaY += 0.5f;
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			thetaY -= 0.5f;
		}
		GraphicsSystem::Get()->BeginRender();

		//before draw
		Matrix4 matrices[3];
		/// Note constant buffer takes the matrices transposed!
		matrices[0] = matRotx * matRoty;
		matrices[0] = matrices[0].Transpose();
		matrices[1] = camera.GetViewMatrix().Transpose();
		matrices[2] = camera.GetPerspectiveMatrix(1280.0f / 720.0f).Transpose();

		constantEarthBuffer.Bind(matrices);
		earthPs.Bind();
		earthVs.Bind();
		
		earthBuffer.Render();




		thetaY += 0.5f;

		GraphicsSystem::Get()->EndRender();

	}

	earth.Terminate();
	earthSampler.Terminate();
	earthVs.Terminate();
	earthPs.Terminate();
	earthBuffer.Terminate();
	constantEarthBuffer.Terminate();



	GraphicsSystem::StaticTerminate();
	myWindow.Terminate();
	return 0;
}


