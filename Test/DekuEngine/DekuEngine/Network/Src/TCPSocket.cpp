#include "Precompiled.h"
#include "TCPSocket.h"

Network::TCPSocket::TCPSocket()
	: mSocket(INVALID_SOCKET)
{
	Open();
}

Network::TCPSocket::~TCPSocket()
{
	Close();
}

bool Network::TCPSocket::Open()
{
	// Socket already opened!
	mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mSocket == INVALID_SOCKET)
	{
		// Failed to open socket
		return false;
	}
	return true;
}

void Network::TCPSocket::Close()
{
	if (mSocket != INVALID_SOCKET)
	{
		closesocket(mSocket);
		mSocket = INVALID_SOCKET;
	}
}

bool Network::TCPSocket::Connect(const SocketAddress& address)
{
	if (mSocket == INVALID_SOCKET && !Open())
	{
		return false;
	}

	int result = connect(mSocket, &address.mSockAddr, address.GetSize());
	if (result == SOCKET_ERROR)
	{
		int lastError = WSAGetLastError();
		if (lastError == WSAEWOULDBLOCK)
		{
			fd_set write, err;
			FD_ZERO(&write);
			FD_ZERO(&err);
			FD_SET(mSocket, &write);
			FD_SET(mSocket, &err);

			TIMEVAL timeout = { 10, 0 }; // timeout after 10 seconds
			result = select(0, NULL, &write, &err, &timeout);
			if (result == 0)
			{
				// Timed out
				return false;
			}
			else
			{
				if (FD_ISSET(mSocket, &write))
				{
					return true;
				}
				if (FD_ISSET(mSocket, &err))
				{
					return false;
				}
			}
		}
		else
		{
			// Failed to connect socket.
			return false;
		}
	}

	return true;
}

bool Network::TCPSocket::Bind(const SocketAddress& address)
{
	if (mSocket == INVALID_SOCKET && !Open())
	{
		return false;
	}

	int result = bind(mSocket, &address.mSockAddr, address.GetSize());
	if (result == SOCKET_ERROR)
	{
		// Failed to bind socket.
		return false;
	}
	return true;
}

bool Network::TCPSocket::Listen(int backLog)
{
	int err = listen(mSocket, backLog);
	if (err == SOCKET_ERROR)
	{
		// Failed to listen
		return false;
	}
	return true;
}

Network::TCPSocket* Network::TCPSocket::Accept(SocketAddress& fromAddress)
{
	socklen_t length = fromAddress.GetSize();
	SOCKET newSocket = accept(mSocket, &fromAddress.mSockAddr, &length);
	if (newSocket == INVALID_SOCKET)
	{
		int lastError = WSAGetLastError();
		if (lastError == WSAEWOULDBLOCK)
		{
			return nullptr;
		}
		else
		{
			// Failed to accept connection
			return nullptr;
		}
	}

	TCPSocket* newConnection = new TCPSocket();
	newConnection->mSocket = newSocket;
	return newConnection;
}

int Network::TCPSocket::Send(const void * buffer, int len)
{
	int bytesSent = send(mSocket, static_cast<const char*>(buffer), len, 0);
	if (bytesSent == SOCKET_ERROR)
	{
		// Failed to send
		return SOCKET_ERROR;
	}
	return bytesSent;
}

int Network::TCPSocket::Receive(void * buffer, int len)
{
	int bytesRead = recv(mSocket, static_cast<char*>(buffer), len, 0);
	if (bytesRead == SOCKET_ERROR)
	{
		int lastError = WSAGetLastError();
		if (lastError == WSAEWOULDBLOCK)
		{
			bytesRead = 0;
		}
		else
		{
			// Failed to receive data!
			return SOCKET_ERROR;
		}
	}
	return bytesRead;
}


// If not 'using namespace network', have to explicit add Network:: to the function. :)