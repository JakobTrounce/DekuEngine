#ifndef INCLUDED_NETWORK_NETWORKUTIL_H
#define INCLUDED_NETWORK_NETWORKUTIL_H



#include "SocketAddress.h"

namespace Network {
	void Initialize();
	void Terminate();
}

#endif