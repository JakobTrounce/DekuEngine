#include "Precompiled.h"
#include "MeshBuilder.h"
#include <random>


using namespace Deku;
using namespace Deku::Graphics;


 MeshPC MeshBuilder::CreateCube()
{
	 MeshPC cube;
	 // vertices required for cube.
	 // 8 vertices
	 VertexPC vertex{ {0.0f,0.0f,0.0f}, Colors::AliceBlue };
	 cube.mVertices.push_back(vertex); // 0
	 cube.mVertices.push_back({ {0.0f,0.1f,0.0f},Colors::Black}); // 1
	 cube.mVertices.push_back({ {0.1f,0.0f,0.0f}, Colors::DarkBlue }); // 2
	 cube.mVertices.push_back({ {0.1f,0.1f,0.0f}, Colors::White }); // 3
	 cube.mVertices.push_back({ {0.1f,0.1f,0.1f}, Colors::Ivory}); // 4
	 cube.mVertices.push_back({ {0.1f,0.0f,0.1f}, Colors::Aqua }); // 5
	 cube.mVertices.push_back({ {0.0f,0.0f,0.1f}, Colors::Yellow }); // 6
	 cube.mVertices.push_back({ {0.0f,0.1f,0.1f}, Colors::AliceBlue }); // 7

	 // create indices

	 /// face 1
	 // tri 1
	 cube.mIndices.push_back(0);
	 cube.mIndices.push_back(1);
	 cube.mIndices.push_back(2);

	 // tri 2
	 cube.mIndices.push_back(1);
	 cube.mIndices.push_back(3);
	 cube.mIndices.push_back(2);

	 /// face 2
	 // tri 3
	 cube.mIndices.push_back(2);
	 cube.mIndices.push_back(3);
	 cube.mIndices.push_back(5);

	 // tri 4
	 cube.mIndices.push_back(3);
	 cube.mIndices.push_back(4);
	 cube.mIndices.push_back(5);

	 /// face 3
	 // tri 5
	 cube.mIndices.push_back(4);
	 cube.mIndices.push_back(7);
	 cube.mIndices.push_back(6);

	 //tri 6
	 cube.mIndices.push_back(4);
	 cube.mIndices.push_back(6);
	 cube.mIndices.push_back(5);

	 ///face 4
	 //tri 7
	 cube.mIndices.push_back(7);
	 cube.mIndices.push_back(1);
	 cube.mIndices.push_back(0);

	 //tri8
	 cube.mIndices.push_back(7);
	 cube.mIndices.push_back(0);
	 cube.mIndices.push_back(6);

	 ///face 5
	 //tri 9

	 cube.mIndices.push_back(7);
	 cube.mIndices.push_back(4);
	 cube.mIndices.push_back(3);

	 //tri 10
	 cube.mIndices.push_back(7);
	 cube.mIndices.push_back(3);
	 cube.mIndices.push_back(1);

	 ///bottom 6
	 //tri 11
	 cube.mIndices.push_back(2);
	 cube.mIndices.push_back(5);
	 cube.mIndices.push_back(6);

	 //tri 12
	 cube.mIndices.push_back(6);
	 cube.mIndices.push_back(0);
	 cube.mIndices.push_back(2);

	return cube;
}
 MeshPC MeshBuilder::CreateTriangle()
 {
	 MeshPC triangle;
	 std::vector<VertexPC> vertices
	 {
		 {{-1.0f,0.0f,0.0f}, Colors::AliceBlue },
		 { {0.0f,1.0f,0.0f}, Colors::AliceBlue },
		 { {1.0f,0.0f,0.0f}, Colors::AliceBlue }
	 };

	 std::vector<uint32_t> indices
	 {
		 0,1,2
	 };

	 triangle.mIndices = indices;
	 triangle.mVertices = vertices;
	 return triangle;
 }
 MeshPC MeshBuilder::CreatePlane(int row, int col,float thickness)
 {
	 std::vector<VertexPC> vertices;
	 vertices.reserve(row*col);

	 std::vector<uint32_t> indices;
	 vertices.reserve((row*col) << 1);

	for (int y = 0; y < row; ++y)
	{
		for (int x = 0; x < col; ++x)
		{
			int index = x + (y*col);
			VertexPC vertex = { {static_cast<float>(x),static_cast<float>(y),thickness}, Colors::AliceBlue };
			vertices.push_back(vertex);

			if (x < col -1  && y < row -1 )
			{				
				indices.push_back(index);
				indices.push_back(index + row);
				indices.push_back(index + 1);

				indices.push_back(index + row);
				indices.push_back(index + 1 + row);
				indices.push_back(index + 1);
			}
		}
	}

	MeshPC plane;
	plane.mVertices = vertices;
	plane.mIndices = indices;

	return plane;
 }

 MeshPC MeshBuilder::CreateSphere(int row, int col,float radius)
 {
	 // bhavils random color generator
	 std::random_device rd;
	 std::mt19937 mt(rd());
	 std::uniform_real_distribution<float> dist(0.0f, 1.0f);


	 std::vector<VertexPC> vertices;
	 vertices.reserve(row*col);

	 std::vector<uint32_t> indices;
	 vertices.reserve((row*col) << 1);

	 // pass in row, col, radius, rowHeight

	 float dTheta = Math::Constants::TwoPi / (col -1);
	 float theta = 0.0f;
	 float yDTheta = Math::Constants::Pi / (row - 1);
	 float yTheta = 0.0f;
	 float yStep = 0.0f;
	 // R = radius*sinTheta;
		for (int y = 0; y < row; ++y)
		{
			theta = 0.0f;
			float newRad = radius * sinf(yTheta);
			float height = radius * cosf(yTheta);

			for (int x = 0; x < col; ++x)
			{
			
				float xStep = cos(theta) * newRad;
				float zStep = sin(theta) * newRad;
				


				int index = x + (y*col);
				VertexPC vertex = { {xStep,yStep,zStep}, Colors::DarkOliveGreen };
				vertices.push_back(vertex);

				if (x < col - 1 && y < row - 1)
				{
					indices.push_back(index);
					indices.push_back(index + row);
					indices.push_back(index + 1);

					indices.push_back(index + row);
					indices.push_back(index + 1 + row);
					indices.push_back(index + 1);
					//VertexPC vertices[index] = { x,y,z };
				 //X and Z is gonna be some value of sin and cosin
				}
				theta += dTheta;

			}
			if ((y + 1) * yDTheta * Math::Constants::RadToDeg > 90.0f)
			{
				yTheta = (((y + 1) * yDTheta) - (-Math::Constants::Pi)) * -1.0f;
				yStep = ((cosf(yTheta) * radius) + (radius));
			}
			else
			{
				yTheta = (((y + 1) * yDTheta));
				yStep = ((cosf(yTheta) * radius) + (-radius)) * -1.0f;
			}

		}
		

		// adjust colors
		for (int i = 0; i < vertices.size(); ++i)
		{
			vertices[i].color = { dist(mt), dist(mt), dist(mt), 1.0f };
		}
		return MeshPC({ vertices,indices });
 }

 MeshPX Deku::Graphics::MeshBuilder::CreateSphereUV(int row, int col, float radius)
 {
	

	 std::vector<VertexPX> vertices;
	 vertices.reserve(row*col);

	 std::vector<uint32_t> indices;
	 vertices.reserve((row*col) << 1);

	 // pass in row, col, radius, rowHeight

	 float dTheta = Math::Constants::TwoPi / (col - 1);
	 float theta = 0.0f;
	 float yDTheta = Math::Constants::Pi / (row - 1);
	 float yTheta = 0.0f;
	 float yStep = 0.0f;
	 // R = radius*sinTheta;
	 for (int y = 0; y < row; ++y)
	 {
		 theta = 0.0f;
		 float newRad = radius * sinf(yTheta);
		 float height = radius * cosf(yTheta);

		 for (int x = 0; x < col; ++x)
		 {

			 float xStep = cos(theta) * newRad;
			 float zStep = sin(theta) * newRad;



			 int index = x + (y*col);
			 VertexPX vertex = { {xStep,yStep,zStep}, {static_cast<float>(x) / col, static_cast<float>(y) / row} };
			 vertices.push_back(vertex);

			 if (x < col - 1 && y < row - 1)
			 {
				 indices.push_back(index);
				 indices.push_back(index + row);
				 indices.push_back(index + 1);

				 indices.push_back(index + row);
				 indices.push_back(index + 1 + row);
				 indices.push_back(index + 1);
				 //VertexPC vertices[index] = { x,y,z };
			  //X and Z is gonna be some value of sin and cosin
			 }
			 theta += dTheta;

		 }
		 if ((y + 1) * yDTheta * Math::Constants::RadToDeg > 90.0f)
		 {
			 yTheta = (((y + 1) * yDTheta) - (-Math::Constants::Pi)) * -1.0f;
			 yStep = ((cosf(yTheta) * radius) + (radius));
		 }
		 else
		 {
			 yTheta = (((y + 1) * yDTheta));
			 yStep = ((cosf(yTheta) * radius) + (-radius)) * -1.0f;
		 }

	 }


	 
	 return MeshPX({ vertices,indices,});
 }

 MeshPX Deku::Graphics::MeshBuilder::CreateSkyBoxUV(int height, int width, int depth)
 {

	 float halfHeight = height * 0.5f;
	 float halfWidth = width * 0.5f;
	 float halfDepth = depth * 0.5f;

	 std::vector<VertexPX> vertices;
	 vertices.resize(14);

	 //Front Face
	 vertices[0] = { { -1.0f * halfWidth,  1.0f * halfHeight, -1.0f * halfDepth},  {0.75f, 1.0f / 3.0f} };		//Front top left		00
	 vertices[1] = { {  1.0f * halfWidth,  1.0f * halfHeight, -1.0f * halfDepth},  {1.00f, 1.0f / 3.0f} };		//Front top Right		01
	 vertices[2] = { { -1.0f * halfWidth, -1.0f * halfHeight, -1.0f * halfDepth},  {0.75f, 2.0f / 3.0f} };		//Front Buttom Left		02
	 vertices[3] = { {  1.0f * halfWidth, -1.0f * halfHeight, -1.0f * halfDepth},  {1.00f, 2.0f / 3.0f} };		//Front Buttom Right	03
																				 
	 //Buttom Face																   {				  }
	 vertices[4] = { { -1.0f * halfWidth, -1.0f * halfHeight, -1.0f * halfDepth},  {0.25f, 1.0f } };//Front Buttom Left		04
	 vertices[5] = { {  1.0f * halfWidth, -1.0f * halfHeight, -1.0f * halfDepth},  {0.50f, 1.0f } };//Front Buttom Right	05
	 vertices[6] = { { -1.0f * halfWidth, -1.0f * halfHeight,  1.0f * halfDepth},  {0.25f, 2.0f / 3.0f} };		//Back Buttom Left		06
	 vertices[7] = { {  1.0f * halfWidth, -1.0f * halfHeight,  1.0f * halfDepth},  {0.50f, 2.0f / 3.0f} };		//Back Buttom Right		07
																				
	 //Top Face																	   {				  }
	 vertices[8] = { { -1.0f * halfWidth,  1.0f * halfHeight, -1.0f * halfDepth},  {0.25f, 0.0f } };//Front top left		08
	 vertices[9] = { {  1.0f * halfWidth,  1.0f * halfHeight, -1.0f * halfDepth},  {0.05f, 0.0f } };//Front top Right		09
	 vertices[10] = { { -1.0f * halfWidth,  1.0f * halfHeight,  1.0f * halfDepth}, {0.25f, 1.0f / 3.0f} };		//Back top left			10
	 vertices[11] = { {  1.0f * halfWidth,  1.0f * halfHeight,  1.0f * halfDepth}, {0.50f, 1.0f / 3.0f} };		//Back top Right		11
																				  
	 //Left Face only 2 at the left (front vertices)							   {				  }
	 vertices[12] = { { -1.0f * halfWidth,  1.0f * halfHeight, -1.0f * halfDepth}, {0.0f , 1.0f / 3.0f} };		//Front top left		12
	 vertices[13] = { { -1.0f * halfWidth, -1.0f * halfHeight, -1.0f * halfDepth}, {0.0f , 2.0f / 3.0f} };		//Front Buttom Left		13


	 constexpr uint32_t indices[] =
	 {
		 2,1,0,		//|
		 2,3,1,		//|--- Front face

		 3,7,11,		//|
		 11,9,3,		//|--- Right face

		 10,11,6,	//|
		 11,7,6,		//|--- Back face

		 13,12,10,	//|
		 10,6,13,	//|--- Left face

		 11,10,8,	//|
		 8,9,11,		//|--- Top face

		 7,5,4,		//|
		 4,6,7		//|--- Bottom face
	 };
	 MeshPX mesh;

	 mesh.mVertices = std::move(vertices);
	 mesh.mIndices.insert(mesh.mIndices.end(), std::begin(indices), std::end(indices));

	 return mesh;
	 return MeshPX();
 }

 MeshPN Deku::Graphics::MeshBuilder::CreateSpherePN(int rows, int col, float radius)
 {

	 std::vector<VertexPN> vertices;
	 vertices.reserve(rows*col);

	 std::vector<uint32_t> indices;
	 vertices.reserve((rows*col) << 1);

	 // pass in row, col, radius, rowHeight

	 float dTheta = Math::Constants::TwoPi / (col - 1);
	 float theta = 0.0f;
	 float yDTheta = Math::Constants::Pi / (rows - 1);
	 float yTheta = 0.0f;
	 float yStep = 0.0f;
	 // R = radius*sinTheta;
	 for (int y = 0; y < rows; ++y)
	 {
		 theta = 0.0f;
		 float newRad = radius * sinf(yTheta);
		 float height = radius * cosf(yTheta);

		 for (int x = 0; x < col; ++x)
		 {

			 float xStep = cos(theta) * newRad;
			 float zStep = sin(theta) * newRad;



			 int index = x + (y*col);
			 VertexPN vertex = { {xStep,yStep,zStep}};
			 vertices.push_back(vertex);

			 if (x < col - 1 && y < rows - 1)
			 {
				 indices.push_back(index);
				 indices.push_back(index + rows);
				 indices.push_back(index + 1);

				 indices.push_back(index + rows);
				 indices.push_back(index + 1 + rows);
				 indices.push_back(index + 1);
				 //VertexPC vertices[index] = { x,y,z };
			  //X and Z is gonna be some value of sin and cosin
			 }
			 theta += dTheta;

		 }
		 if ((y + 1) * yDTheta * Math::Constants::RadToDeg > 90.0f)
		 {
			 yTheta = (((y + 1) * yDTheta) - (-Math::Constants::Pi)) * -1.0f;
			 yStep = ((cosf(yTheta) * radius) + (radius));
		 }
		 else
		 {
			 yTheta = (((y + 1) * yDTheta));
			 yStep = ((cosf(yTheta) * radius) + (-radius)) * -1.0f;
		 }

	 }

	 MeshPN mesh;
	 mesh.mVertices = vertices;
	 mesh.mIndices = indices;


	 return mesh;
 }

 