#include "Precompiled.h"
#include "SpriteRenderer.h"

#include "D3DUtil.h"
#include "GraphicsSystem.h"
#include "Texture.h"
#include <DirectXTK/Inc/SpriteBatch.h>

using namespace Deku::Graphics;
using namespace Deku::Math;

namespace
{
	std::unique_ptr<SpriteRenderer> sSpriteRenderer = nullptr;
}

void SpriteRenderer::StaticInitialize()
{
	DEKUASSERT(sSpriteRenderer == nullptr, "[SpriteRenderer] System already initialized!");

	sSpriteRenderer = std::make_unique<SpriteRenderer>();
	sSpriteRenderer->Initialize();
}

void Deku::Graphics::SpriteRenderer::StaticTerminate()
{
	if (sSpriteRenderer != nullptr)
	{
		sSpriteRenderer->Terminate();
		sSpriteRenderer.reset();
	}
}

SpriteRenderer* SpriteRenderer::Get()
{
	DEKUASSERT(sSpriteRenderer != nullptr, "[SpriteRenderer] Sprite renderer is not initialized!");
	return sSpriteRenderer.get();
}

SpriteRenderer::~SpriteRenderer()
{
	DEKUASSERT(mSpriteBatch == nullptr, "[SpriteRenderer] Terminate was not called correctly!");
}

void SpriteRenderer::Initialize()
{
	DEKUASSERT(mSpriteBatch == nullptr, "[SpriteRenderer] Already Initialized!");
	auto context = GetContext();
	mSpriteBatch = std::make_unique<DirectX::SpriteBatch>(context);
}

void SpriteRenderer::BeginRender()
{
	DEKUASSERT(mSpriteBatch != nullptr, "[SpriteRenderer] Not Initialized!");
	mSpriteBatch->Begin();
}

void SpriteRenderer::EndRender()
{
	DEKUASSERT(mSpriteBatch != nullptr, "[SpriteRenderer] Not Initialized!");
	mSpriteBatch->End();
}

void SpriteRenderer::Draw(const Texture& texture, const Vector3& position)
{
	mSpriteBatch->Draw(GetShaderResourceView(texture), DirectX::XMFLOAT2{ position.x, position.y });
}

void SpriteRenderer::Terminate()
{
	DEKUASSERT(mSpriteBatch != nullptr, "[SpriteRenderer] Already terminated!");
	mSpriteBatch.reset();
}