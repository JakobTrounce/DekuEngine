#include "Precompiled.h"
#include "D3DUtil.h"

#include "Texture.h"
#include "GraphicsSystem.h"

ID3D11ShaderResourceView* Deku::Graphics::GetShaderResourceView(const Texture & texture)
{
	return texture.mShaderResourceView;
}

ID3D11Device * Deku::Graphics::GetDevice()
{
	return GraphicsSystem::Get()->mD3DDdevice;
}

ID3D11DeviceContext * Deku::Graphics::GetContext()
{
	return GraphicsSystem::Get()->mImmediateContext;
}