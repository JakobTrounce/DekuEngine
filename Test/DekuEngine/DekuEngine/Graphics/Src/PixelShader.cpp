#include "Precompiled.h"
#include "PixelShader.h"
#include "GraphicsSystem.h"
using namespace Deku;
using namespace Graphics;

void PixelShader::Initialize(std::string shaderName)
{
	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;
	std::wstring name = ConvertString(shaderName);
	HRESULT hr;
	hr = D3DCompileFromFile(
		name.c_str(),
		nullptr,
		nullptr,
		"PS", "ps_5_0",
		0,
		0,
		&shaderBlob,
		&errorBlob
	);


	DEKUASSERT(SUCCEEDED(hr), "[PixelShader] Failed to compile pixel shader. Error: %s", static_cast<const char*>(errorBlob->GetBufferPointer()));

	auto device = GraphicsSystem::Get()->GetDevice();
	hr = device->CreatePixelShader(
		shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(),
		nullptr,
		&pixelShader
	);
	SafeRelease(shaderBlob);
	SafeRelease(errorBlob);
}

void PixelShader::Bind()
{
	auto context = GraphicsSystem::Get()->GetContext();
	context->PSSetShader(pixelShader, nullptr, 0);

}

void PixelShader::Terminate()
{
	SafeRelease(pixelShader);
}

std::wstring PixelShader::ConvertString(std::string & s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}
