#include "Precompiled.h"
#include "Texture.h"
#include "GraphicsSystem.h"

#include <DirectXTK/Inc/WICTextureLoader.h>
using namespace Deku;
using namespace Deku::Graphics;




Texture::~Texture()
{
	DEKUASSERT(mShaderResourceView == nullptr, "[Texture] Terminate must be called before destruction.");
}

void Deku::Graphics::Texture::Initialize(const wchar_t* fileName)
{
	auto device = GraphicsSystem::Get()->GetDevice();
	auto context = GraphicsSystem::Get()->GetContext();

	HRESULT hr = DirectX::CreateWICTextureFromFile(device, context, fileName, nullptr, &mShaderResourceView);
	DEKUASSERT(SUCCEEDED(hr), "[Texture] Failed to load Texture %ls", fileName);

}

void Deku::Graphics::Texture::Terminate()
{
	SafeRelease(mShaderResourceView);
}

void Deku::Graphics::Texture::BindPS(uint32_t slot) const
{
	GraphicsSystem::Get()->GetContext()->PSSetShaderResources(slot, 1, &mShaderResourceView);
}

std::wstring Texture::ConvertString(std::string& string)
{
	int length;
	int slength = (int)string.length() + 1;
	length = MultiByteToWideChar(CP_ACP, 0, string.c_str(), slength, 0, 0);
	wchar_t* buffer = new wchar_t[length];
	MultiByteToWideChar(CP_ACP, 0, string.c_str(), slength, buffer, length);
	std::wstring r(buffer);
	delete[] buffer;
	return r;
}