#include "Precompiled.h"
#include "ConstantBuffer.h"
#include "GraphicsSystem.h"
#include <Math/Inc/Matrix4.h>

using namespace Deku;
using namespace Graphics;

void ConstantBuffer::Initialize(uint32_t constantSize,const void* initData)
{
	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = constantSize;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	//D3D11_SUBRESOURCE_DATA subResourceData{};
	//subResourceData.pSysMem = initData;

	GraphicsSystem::Get()->GetDevice()->CreateBuffer(&desc, nullptr, &mConstantBuffer);
}

void ConstantBuffer::Bind(void* constants)
{
	Set(constants);
	BindVS();
	
}

void Deku::Graphics::ConstantBuffer::Set(void * constants)
{
	auto context = GraphicsSystem::Get()->GetContext();
	context->UpdateSubresource(mConstantBuffer, 0, nullptr, constants, 0, 0); // this downloads your data to vram 
}

void Deku::Graphics::ConstantBuffer::BindPS()
{
	auto context = GraphicsSystem::Get()->GetContext();
	context->PSSetConstantBuffers(0, 1, &mConstantBuffer);
}

void Deku::Graphics::ConstantBuffer::BindVS()
{
	auto context = GraphicsSystem::Get()->GetContext();
	context->VSSetConstantBuffers(0, 1, &mConstantBuffer);
}

// Add BindPs
// Add BindVs
// Add Set
void ConstantBuffer::Terminate()
{
	SafeRelease(mConstantBuffer);
}
