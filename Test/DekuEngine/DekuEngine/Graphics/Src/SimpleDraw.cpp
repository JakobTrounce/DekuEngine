#include "Precompiled.h"
#include "SimpleDraw.h"

#include "ConstantBuffer.h"
#include "GraphicsSystem.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "VertexTypes.h"
#include "MeshBuffer.h"

using namespace Deku;
using namespace Deku::Graphics;
using namespace Deku::Math;

namespace
{
	//Private impimentation
	class SimpleDrawImplementation
	{
	public:
		void Initialize(uint32_t maxVertexCount);
		void Terminate();
		void Render(const Camera& camera);

		void AddLine(const Math::Vector3 & v0, const Math::Vector3 & v1, const Color & color);
		void AddLine(const VertexPC& v0, const VertexPC& v1);
		void AddScreenLine(const Vector2& v0, const Vector2& v1, const Color color);

		void Render3D(const Camera& camera);
		void Render2D(const Camera& camera);

	private:
		ConstantBuffer mConstantBuffer;
		VertexShader mVertexShader;
		PixelShader mPixelShader;
		MeshBuffer mMeshBuffer;


		std::vector<VertexPC> mVertices3D;
		std::vector<VertexPC> mVertices2D;

		bool mInitialize{ false };
	};

	void SimpleDrawImplementation::Initialize(uint32_t maxVertexCount)
	{
		DEKUASSERT(!mInitialize, "[SimpleDrawImpl] Already Initialized.\n");
		mVertexShader.Initialize("../Assets/Shaders/SimpleDraw.fx", VertexPC::Format);
		mPixelShader.Initialize("../Assets/Shaders/SimpleDraw.fx");
		mConstantBuffer.Initialize(sizeof(Math::Matrix4));
		mMeshBuffer.Initialize(nullptr, (int)sizeof(VertexPC), maxVertexCount, true);
		mMeshBuffer.SetTopology(MeshBuffer::Topology::Lines);
		mVertices3D.reserve(maxVertexCount);
		mVertices2D.reserve(maxVertexCount);

		mInitialize = true;
	}

	void SimpleDrawImplementation::Terminate()
	{
		DEKUASSERT(mInitialize, "[SimpleDrawImpl] Already Terminated.\n");
		mVertexShader.Terminate();
		mPixelShader.Terminate();
		mConstantBuffer.Terminate();
		mMeshBuffer.Terminate();

		mInitialize = false;
	}

	void SimpleDrawImplementation::AddLine(const Math::Vector3 & v0, const Math::Vector3 & v1, const Color & color)
	{
		DEKUASSERT(mInitialize, "[SimpleDrawImpl::AddLine] not initialized");
		if (mVertices3D.size() + 2 <= mVertices3D.capacity())
		{
			mVertices3D.emplace_back(VertexPC{ v0, color });
			mVertices3D.emplace_back(VertexPC{ v1, color });
		}
	}

	void SimpleDrawImplementation::AddLine(const VertexPC & v0, const VertexPC & v1)
	{
		DEKUASSERT(mInitialize, "[SimpleDrawImpl::AddLine] not initialized");
		if (mVertices3D.size() + 2 <= mVertices3D.capacity())
		{
			mVertices3D.emplace_back(v0);
			mVertices3D.emplace_back(v1);
		}
	}

	void SimpleDrawImplementation::AddScreenLine(const Vector2& v0, const Vector2& v1, const Color color)
	{
		DEKUASSERT(mInitialize, "[SimpleDraw] Not initialized.");

		if (mVertices2D.size() + 2 <= mVertices2D.capacity())
		{
			mVertices2D.emplace_back(VertexPC{ {v0.x, v0.y, 0.0f}, color });
			mVertices2D.emplace_back(VertexPC{ {v1.x, v1.y, 0.0f}, color });
			
		}
	}

	void SimpleDrawImplementation::Render3D(const Camera& camera)
	{
		DEKUASSERT(mInitialize, "[SimpleDraw] Not initialized.");
		if (mVertices3D.empty())
		{
			return;
		}

		Matrix4 matView = camera.GetViewMatrix();
		Matrix4 matProj = camera.GetPerspectiveMatrix();

		mVertexShader.Bind();
		mPixelShader.Bind();

		Matrix4 transform = Matrix4::Transpose(matView * matProj);

		mConstantBuffer.Bind(&transform);

		mMeshBuffer.Update(mVertices3D.data(), mVertices3D.size());

		mMeshBuffer.Render();

		mVertices3D.clear();
	}

	void SimpleDrawImplementation::Render2D(const Camera& camera)
	{
		DEKUASSERT(mInitialize, "[SimpleDraw] Not initialized.");

		if (mVertices2D.empty())
		{
			return;
		}

		Matrix4 screenSpace
		(
			2.0f / GraphicsSystem::Get()->GetBackBufferWidth(), 0.0f, 0.0f, 0.0f,
			0.0f, -2.0f / GraphicsSystem::Get()->GetBackBufferHeight(), 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f, 1.0f
		);

		mVertexShader.Bind();
		mPixelShader.Bind();

		mConstantBuffer.Bind(&screenSpace.Transpose());

		mMeshBuffer.Update(mVertices2D.data(), mVertices2D.size());

		mMeshBuffer.Render();

		mVertices2D.clear();
	}


	std::unique_ptr<SimpleDrawImplementation> sInstance;
}


void Deku::Graphics::SimpleDraw::StaticInitialize(uint32_t maxVertex)
{
	sInstance = std::make_unique<SimpleDrawImplementation>();
	sInstance->Initialize(maxVertex);
}

void Deku::Graphics::SimpleDraw::StaticTerminate()
{
	sInstance->Terminate();
	sInstance.reset();
}

void Deku::Graphics::SimpleDraw::DrawDebugGrid(int lineCount)
{
	// X Axis
	DrawLine({ -100.0f, 0.0f, 0.0f }, { 100.0f, 0.0f, 0.0f }, Colors::Red);
	// Y Axis
	DrawLine({ 0.0f, -100.0f, 0.0f }, { 0.0f, 100.0f, 0.0f }, Colors::Green);
	// Z Axis
	DrawLine({ 0.0f, 0.0f, -100.0f }, { 0.0f, 0.0f, 100.0f }, Colors::Blue);

	for (int lineNumber = 1; lineNumber < lineCount; ++lineNumber)
	{
		// Z-Axis lines
		DrawLine({ (float)(lineNumber * 1.0f), 0.0f, -100.0f }, { (float)(lineNumber * 1.0f), 0.0f, 100.0f }, Colors::Black);
		DrawLine({ (float)(-lineNumber * 1.0f), 0.0f, -100.0f }, { (float)(-lineNumber * 1.0f), 0.0f, 100.0f }, Colors::Black);

		// X-Axis lines
		DrawLine({ -100.0f, 0.0f, (float)(lineNumber * 1.0f) }, { 100.0f, 0.0f, (float)(lineNumber * 1.0f) }, Colors::Black);
		DrawLine({ -100.0f, 0.0f, (float)(-lineNumber * 1.0f) }, { 100.0f, 0.0f, (float)(-lineNumber * 1.0f) }, Colors::Black);
	}
}

void Deku::Graphics::SimpleDraw::DrawLine(const Deku::Math::Vector3& v0, const Deku::Math::Vector3& v1, const Color color)
{
	sInstance->AddLine(v0, v1, color);
}

void Deku::Graphics::SimpleDraw::DrawSquare(const Deku::Math::Vector3& center, float sideLength, const Color color)
{
	float halfLength = sideLength / 2.0f;
	// Top
	DrawLine({ -halfLength + center.x,  halfLength + center.y, center.z }, { halfLength + center.x,  halfLength + center.y, center.z }, color);
	// Right
	DrawLine({ halfLength + center.x,  halfLength + center.y, center.z }, { halfLength + center.x, -halfLength + center.y, center.z }, color);
	// Bottom
	DrawLine({ halfLength + center.x, -halfLength + center.y, center.z }, { -halfLength + center.x, -halfLength + center.y, center.z }, color);
	// Left
	DrawLine({ -halfLength + center.x, -halfLength + center.y, center.z }, { -halfLength + center.x,  halfLength + center.y, center.z }, color);
}

void Deku::Graphics::SimpleDraw::DrawCircle(const Deku::Math::Vector3& center, float radius, int sideCount, const Color color)
{
	DEKUASSERT((sideCount >= 3), "[SimpleDraw::DrawCircle] Side count must be more than 2!");
	if (sideCount < 3)
	{
		sideCount = 3;
	}

	
	float x = center.x;
	float y = center.y;

	// Just like in mesh builder, slice it up into sections based on number of sides, then draw lines between the start and end for each slice
	const float sliceAngle = (2.0f * Math::Constants::Pi) / sideCount;
	for (uint32_t i = 0; i < sideCount; ++i)
	{
		
		const float theta = i * sliceAngle;
		const float thetaNext = theta + sliceAngle;
		const float x0 = x + (radius * sin(theta));
		const float y0 = y + (radius * cos(theta));
		const float x1 = x + (radius * sin(thetaNext));
		const float y1 = y + (radius * cos(thetaNext));

		DrawLine({ x0, y0, center.z }, { x1, y1, center.z }, color);
	}
}

void Deku::Graphics::SimpleDraw::DrawCube(const Deku::Math::Vector3& center, float sideLength, const Color color)
{
	float halfLength = sideLength / 2.0f;
	// Front
	DrawSquare({ center.x, center.y, center.z - halfLength }, sideLength, color);
	// Back
	DrawSquare({ center.x, center.y, center.z + halfLength }, sideLength, color);

	// Top Left
	DrawLine({ center.x - halfLength, center.y + halfLength, center.z - halfLength }, { center.x - halfLength, center.y + halfLength, center.z + halfLength }, color);
	// Top Right
	DrawLine({ center.x + halfLength, center.y + halfLength, center.z - halfLength }, { center.x + halfLength, center.y + halfLength, center.z + halfLength }, color);
	// Bottom Right
	DrawLine({ center.x + halfLength, center.y - halfLength, center.z - halfLength }, { center.x + halfLength, center.y - halfLength, center.z + halfLength }, color);
	// Botom Left
	DrawLine({ center.x - halfLength, center.y - halfLength, center.z - halfLength }, { center.x - halfLength, center.y - halfLength, center.z + halfLength }, color);
}

void Deku::Graphics::SimpleDraw::DrawSphere(const Deku::Math::Vector3& center, float radius, uint32_t sliceCount, uint32_t ringCount, const Color color)
{
	DEKUASSERT((sliceCount >= 3 && ringCount >= 3), "[SimpleDraw::DrawSphere] Slices or Rings size does not meet minimum requirements!");

	// ringTheta is the angle between each ring (based on number of ringCount) going around a half circle
	const float ringTheta = Math::Constants::Pi / (float)ringCount;
	// slicePhi is the angle between each slice (based on number of sliceCount) going around the full circle
	const float slicePhi = Math::Constants::TwoPi / (float)sliceCount;

	// For each slice (meridians, vertical, orange slice), we draw the lines for each slice, and each ring between a slice and its neighbour slice
	for (uint32_t currentSlice = 0; currentSlice < sliceCount; ++currentSlice)
	{
		for (uint32_t currentRing = 0; currentRing < ringCount; ++currentRing)
		{
			const float startAngle = currentRing * ringTheta;
			const float neighbourAngle = startAngle + ringTheta;
			const float ay = radius * cos(startAngle);
			const float by = radius * cos(neighbourAngle);

			const float theta = currentSlice * slicePhi;
			const float phi = theta + slicePhi;

			const float ar = sqrt(radius * radius - ay * ay);
			const float br = sqrt(radius * radius - by * by);

			const float x0 = center.x + (ar * sin(theta));
			const float y0 = center.y + (ay);
			const float z0 = center.z + (ar * cos(theta));

			const float x1 = center.x + (br * sin(theta));
			const float y1 = center.y + (by);
			const float z1 = center.z + (br * cos(theta));

			const float x2 = center.x + (br * sin(phi));
			const float y2 = center.y + (by);
			const float z2 = center.z + (br * cos(phi));

			DrawLine({ x0, y0, z0 }, { x1, y1, z1 }, color);

			if (currentRing < ringCount - 1)
			{
				DrawLine({ x1, y1, z1 }, { x2, y2, z2 }, color);
			}
		}
	}
}

void Deku::Graphics::SimpleDraw::DrawScreenLine(const Deku::Math::Vector2& v0, const Deku::Math::Vector2& v1, const Color color)
{
	sInstance->AddScreenLine(v0, v1, color);
}

void Deku::Graphics::SimpleDraw::DrawScreenSquare(const Deku::Math::Vector2& center, float sideLength, const Color color)
{
	float halfLength = sideLength / 2.0f;

	// Top
	DrawScreenLine({ -halfLength + center.x,  halfLength + center.y }, { halfLength + center.x,  halfLength + center.y }, color);
	// Right
	DrawScreenLine({ halfLength + center.x,  halfLength + center.y }, { halfLength + center.x, -halfLength + center.y }, color);
	// Bottom
	DrawScreenLine({ halfLength + center.x, -halfLength + center.y }, { -halfLength + center.x, -halfLength + center.y }, color);
	// Left
	DrawScreenLine({ -halfLength + center.x, -halfLength + center.y }, { -halfLength + center.x,  halfLength + center.y }, color);
}

void Deku::Graphics::SimpleDraw::DrawScreenRect(const Deku::Math::Vector2& topLeft, const Deku::Math::Vector2& bottomRight, const Color color)
{
	// Top
	DrawScreenLine(topLeft, { bottomRight.x, topLeft.y }, color);
	// Right
	DrawScreenLine({ bottomRight.x, topLeft.y }, bottomRight, color);
	// Bot
	DrawScreenLine(bottomRight, { topLeft.x, bottomRight.y }, color);
	// Left
	DrawScreenLine({ topLeft.x, bottomRight.y }, topLeft, color);
}

void Deku::Graphics::SimpleDraw::DrawScreenCircle(const Deku::Math::Vector2& center, float radius, const Color color, bool filled)
{
	int sideCount = 12;
	float x = center.x;
	float y = center.y;

	if (!filled)
	{
		// Just like in mesh builder, slice it up into sections based on number of sides, then draw lines between the start and end for each slice
		const float sliceAngle = (2.0f * Math::Constants::Pi) / sideCount;
		for (uint32_t i = 0; i < sideCount; ++i)
		{
			const float theta = i * sliceAngle;
			const float thetaNext = theta + sliceAngle;
			const float x0 = x + (radius * sin(theta));
			const float y0 = y + (radius * cos(theta));
			const float x1 = x + (radius * sin(thetaNext));
			const float y1 = y + (radius * cos(thetaNext));

			DrawScreenLine({ x0, y0 }, { x1, y1 }, color);
		}
	}
	else
	{

	}
}


void Deku::Graphics::SimpleDraw::Render(const Camera & camera)
{
	sInstance->Render3D(camera);
	sInstance->Render2D(camera);
}