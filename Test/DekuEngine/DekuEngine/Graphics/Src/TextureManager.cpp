#include "Precompiled.h"

#include "TextureManager.h"



using namespace Deku::Graphics;

namespace
{
	std::unique_ptr<TextureManager> sTextureManager = nullptr;
}

void TextureManager::StaticInitialize()
{
	DEKUASSERT(sTextureManager == nullptr, "[SpriteRenderer] System already initialized!");

	sTextureManager = std::make_unique<TextureManager>();
	sTextureManager->Initialize();
}

void TextureManager::StaticTerminate()
{
	if (sTextureManager != nullptr)
	{
		sTextureManager->Terminate();
		sTextureManager.reset();
	}
}

TextureManager* TextureManager::Get()
{
	DEKUASSERT(sTextureManager != nullptr, "[TextureManager] System is not initialized!");
	return sTextureManager.get();
}

TextureManager::~TextureManager()
{

}

void Deku::Graphics::TextureManager::Initialize()
{

}

void Deku::Graphics::TextureManager::Terminate()
{

}

void Deku::Graphics::TextureManager::SetRootPath(std::wstring path)
{
	// std::move is a static cast to an r-value (&&) (steals the copy off the stack as opposed to deep copying for itself)
	mRootPath = std::move(path);
	if (mRootPath.back() != L'/')
	{
		mRootPath += L'/';
	}
}

TextureID Deku::Graphics::TextureManager::LoadTexture(std::wstring fileName)
{
	auto hash = std::hash<std::wstring>()(fileName);
	if (mInventory.find(hash) == mInventory.end())
	{
		std::wstring filePath = mRootPath + fileName;
		auto texture = std::make_unique<Texture>();
		texture->Initialize(filePath.c_str());
		mInventory.emplace(hash, std::move(texture));
	}
	std::wstring filePath = mRootPath + fileName;

	return hash;
}

const Texture* Deku::Graphics::TextureManager::GetTexture(TextureID textureId) const
{
	if (auto iter = mInventory.find(textureId); iter != mInventory.end())
	{
		return iter->second.get();
	}

	return nullptr;
}