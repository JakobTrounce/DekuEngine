#ifndef INCLUDED_DEKU_GRAPHICS_MESH_H 
#define INCLUDED_DEKU_GRAPHICS_MESH_H 

#include "VertexTypes.h"

namespace Deku{
namespace Graphics{

template<class T>
struct Mesh
{
	using VertexType = T;
	std::vector<T> mVertices;
	std::vector<uint32_t> mIndices;
};

using MeshPC = Mesh<VertexPC>;
using MeshPNX = Mesh<VertexPNX>;
using MeshPX = Mesh<VertexPX>;
using MeshPN = Mesh<VertexPN>;

} //Graphics
} //Deku


#endif // !INCLUDED_DEKU_GRAPHICS_MESH_H 
