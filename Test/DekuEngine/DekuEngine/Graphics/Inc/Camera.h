#ifndef INCLUDED_DEKU_GRAPHICS_CAMERA_H
#define INCLUDED_DEKU_GRAPHICS_CAMERA_H

namespace Deku {
namespace Graphics {

	Math::Matrix4 ComputeViewMatrix(Math::Vector3 right, Math::Vector3 up, Math::Vector3 look, Math::Vector3 position);
	Math::Matrix4 ComputePerspectiveMatrix(float n, float f, float fov, float aspectRatio);

//This assumes y axis is up direction
//Cannot look straight up or down
class Camera {

public:
	void SetPosition(const Math::Vector3& position) { mPosition = position; }
	void SetDirection(const Math::Vector3& direction) { mDirection = direction; }
	void SetTarget(const Math::Vector3& target) { mDirection = target; }

	void SetFov(float angle, bool rad = false) { mFov = !rad ? angle * Math::Constants::DegToRad : angle; }
	void SetNearPlane(float distance) { mNearPlane = distance; }
	void SetFarPlane(float distance) { mFarPlane = distance; }

	//3 degrees of freedom for translation
	void Walk(float distance) { mPosition.z += distance; }
	void Strafe(float distance) { mPosition.x += distance; }
	void Rise(float distance) { mPosition.y += distance; }

	//2 degrees of freedom for rotation
	void Yaw(float rotation) { mDirection.x += rotation; }		//Rotate along global Y axis
	void Pitch(float rotation) { mDirection.y += rotation; }	//Rotate along local  X axis

	const Math::Vector3& GetPosition() const { return mPosition; };
	const Math::Vector3& GetDirection()const { return mDirection; };

	float GetFoV()const { return mFov; };
	float GetNearPlane()const { return mNearPlane; };
	float GetFarPlane()const { return mFarPlane; };

	Math::Matrix4 GetViewMatrix() const;
	Math::Matrix4 GetPerspectiveMatrix(float aspectRatio = 0.0f) const;

private:
	Math::Vector3 mPosition{ 0.0f };
	Math::Vector3 mDirection{ 0.0f, 0.0f, 1.0f };

	float mFov{60.0f * Math::Constants::DegToRad }; //multiply by deg to rad in Math::Constants
	float mNearPlane{ 1.0f };
	float mFarPlane{ 1000.0f };

	Math::Matrix4 mTransformation{ Math::Matrix4::Identity() };
};
}
}

#endif // defined INCLUDED_DEKU_GRAPHICS_CAMERA_H