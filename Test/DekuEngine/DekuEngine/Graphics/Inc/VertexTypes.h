#ifndef INCLUDED_DEKU_GRAPHICS_VERTEX_TYPES_H
#define INCLUDED_DEKU_GRAPHICS_VERTEX_TYPES_H

#include "Common.h"
#include "Colors.h"

namespace Deku {
namespace Graphics {

//Vertex Element Flags
constexpr uint32_t VE_Position		= 0x1 << 0;
constexpr uint32_t VE_Normal		= 0x1 << 1;
constexpr uint32_t VE_Tangent		= 0x1 << 2;
constexpr uint32_t VE_Color			= 0x1 << 3;
constexpr uint32_t VE_Texcoord		= 0x1 << 4;
constexpr uint32_t VE_Blendindex	= 0x1 << 5;
constexpr uint32_t VE_BlendWidth	= 0x1 << 6;

#define VERTEX_FORMAT(fmt)\
	static constexpr uint32_t Format = fmt

// Position Color
struct VertexPC
{
	VERTEX_FORMAT(VE_Position | VE_Color);
	VertexPC() = default;
	VertexPC(const Math::Vector3& pos, const Color& col)
		: position(pos)
		, color(col)
	{}

	Math::Vector3 position;
	Color color;
};

// Position Normal TexCoord
struct VertexPNX
{
	VERTEX_FORMAT(VE_Position | VE_Normal | VE_Texcoord);
	Math::Vector3 position;
	Math::Vector3 normal;
	Math::Vector2 texCoord;
};

struct VertexPN
{
	VertexPN() = default;


	VERTEX_FORMAT(VE_Position | VE_Normal );
	Math::Vector3 position;
	Math::Vector3 normal;
};

struct VertexPX
{
	VERTEX_FORMAT(VE_Position | VE_Texcoord);
	Math::Vector3 position;
	Math::Vector2 texCoord;
};

} //Graphics
} //Deku

#endif // INCLUDED_DEKU_GRAPHICS_VERTEX_TYPES_H