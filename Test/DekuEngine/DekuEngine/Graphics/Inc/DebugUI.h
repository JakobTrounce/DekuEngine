//====================================================================================================
// Filename:	Gui.h
// Created by:	Peter Chan
//====================================================================================================

#ifndef INCLUDED_DEKU_GRAPHICS_DEBUGUI_H
#define INCLUDED_DEKU_GRAPHICS_DEBUGUI_H

namespace Deku::Graphics::DebugUI {

enum class Theme
{
	Classic,
	Dark,
	Light
};

void StaticInitialize(HWND window, bool docking = false, bool multiWin = false);
void StaticTerminate();
void BeginRender();
void EndRender();

void SetTheme(Theme theme);

} // namespace Deku::Graphics::DebugUI

#endif // #ifndef INCLUDED_VISHV_GRAPHICS_DEBUGUI_H