#ifndef INCLUDED_DEKU_GRAPHICS_GRAPHICSSYSTEM_H
#define INCLUDED_DEKU_GRAPHICS_GRAPHICSSYSTEM_H

namespace Deku {
namespace Graphics {
class  GraphicsSystem
{
public:
	// basic singleton functions for the graphics system
	static void StaticInitialize(HWND window, bool fullscreen);
	static void StaticTerminate();
	static GraphicsSystem* Get();

public:
	GraphicsSystem() = default;
	~GraphicsSystem();

	GraphicsSystem(const GraphicsSystem&) = delete;
	GraphicsSystem& operator=(const GraphicsSystem&) = delete;

	void Initialize(HWND window, bool fullscreen);
	void Terminate();

	void BeginRender(Color clearColor = Colors::White);
	void EndRender(bool vsynce = true);

	void ToggleFullscreen();
	
	uint32_t GetBackBufferWidth() const;
	uint32_t GetBackBufferHeight() const;

	ID3D11Device* GetDevice() { return mD3DDdevice; }
	ID3D11DeviceContext* GetContext() { return mImmediateContext; }

private:

	friend LRESULT CALLBACK GraphicsSystemMessageHandler(HWND window, UINT message, WPARAM wParam, LPARAM lParam);

	friend ID3D11Device* GetDevice();
	friend ID3D11DeviceContext* GetContext();

	// device pointer, used for memory management
	ID3D11Device* mD3DDdevice{ nullptr };
	//context pointer, used for draw calls
	ID3D11DeviceContext* mImmediateContext{ nullptr };

	//swap chain
	IDXGISwapChain* mSwapChain{ nullptr };
	//
	ID3D11RenderTargetView* mRenderTargetView{ nullptr };


	ID3D11Texture2D* mDepthStencilBuffer{nullptr};
	ID3D11DepthStencilView* mDepthStencilView{ nullptr };

	DXGI_SWAP_CHAIN_DESC mSwapChainDesc{};
	D3D11_VIEWPORT mViewport{};
};
}// namespace Graphics
} // namespace Deku





#endif // #ifndef INCLUDED_GRAPHICS_GRAPHICS_SYSTEM