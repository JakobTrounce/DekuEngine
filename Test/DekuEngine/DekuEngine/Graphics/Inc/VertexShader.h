#ifndef INCLUDED_DEKU_GRAPHICS_VERTEXSHADER_H
#define INCLUDED_DEKU_GRAPHICS_VERTEXSHADER_H

namespace Deku{
namespace Graphics{

class VertexShader
{
public:
#include "VertexTypes.h"
	void Initialize(std::string shaderName, uint32_t vertexFormat);
	void Terminate();

	void Bind();
	~VertexShader();
private:
	ID3D11VertexShader* mVertexShader{ nullptr };
	ID3D11InputLayout* mInputLayout {nullptr};

	ID3DBlob* shaderBlob{nullptr};
	ID3DBlob* errorBlob{nullptr};

	std::wstring ConvertString(std::string& s);

};




} // namespace graphics
} // namespace Deku




#endif // INCLUDED_DEKU_GRAPHICS_VERTEXSHADER_H