#ifndef INCLUDED_DEKU_GRAPHICS_SIMPLE_DRAW_H
#define INCLUDED_DEKU_GRAPHICS_SIMPLE_DRAW_H

#include "Camera.h"
#include "Colors.h"
#include "VertexTypes.h"

namespace Deku::Graphics::SimpleDraw 
{
	void StaticInitialize(uint32_t maxVertex = 10000);
	void StaticTerminate();
	   
	void DrawDebugGrid(int lineCount = 100);
	void DrawLine(const Deku::Math::Vector3& v0, const Deku::Math::Vector3& v1, const Color color = Colors::Magenta);

	void DrawSquare(const Deku::Math::Vector3& center, float sideLength, const Color color = Colors::Magenta);
	void DrawCircle(const Deku::Math::Vector3& center, float radius, int sideCount = 16, const Color color = Colors::Magenta);
	void DrawCube(const Deku::Math::Vector3& center, float sideLength, const Color color = Colors::Magenta);
	void DrawSphere(const Deku::Math::Vector3& center, float radius, uint32_t sliceCount = 16, uint32_t ringCount = 16, const Color color = Colors::Magenta);

	void DrawScreenLine(const Deku::Math::Vector2& v0, const Deku::Math::Vector2& v1, const Color color = Colors::Magenta);
	void DrawScreenSquare(const Deku::Math::Vector2& center, float sideLength, const Color color = Colors::Magenta);
	void DrawScreenRect(const Deku::Math::Vector2& topLeft, const Deku::Math::Vector2& bottomRight, const Color color = Colors::Magenta);
	//void DrawScreenCircle(const Deku::Math::Vector2& center, float radius, int sideCount = 16, const Color color = Colors::Magenta);
	void DrawScreenCircle(const Deku::Math::Vector2& center, float radius, const Color color = Colors::Magenta, bool filled = false);


	void Render(const Camera& camera);
}	// Deku::Graphics::SimpleDraw 


#endif	//defined INCLUDED_DEKU_GRAPHICS_SIMPLE_DRAW_H


