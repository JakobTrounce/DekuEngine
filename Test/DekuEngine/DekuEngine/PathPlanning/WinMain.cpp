
#include <XEngine.h>
#include <AI.h>


AI::Graph graph;

AI::AIWorld world;
AI::Agent ai(world);
AI::SteeringModule str(ai);

X::TextureId stone;

int rows(15);
int columns(15);

X::Math::Vector2 endPos{ 0,0 };
X::TextureId end;

bool endActive = false;


void Initialize()
{
	end = X::LoadTexture("stone.png");
	str.AddBehaviour<AI::SeekBehaviour>()->SetActive(false);


}

void Terminate()
{

	X::ClearAllTextures();
}

void DrawGraph()
{
	for (int x = 0; x < rows * 32; x += 32.0f)
	{
		for (int y = 0; y < columns * 32; y += 32.0f)
		{
			if (X::IsMousePressed(X::Mouse::LBUTTON))
			{
				if (X::GetMouseScreenX() >= x && X::GetMouseScreenX() <= x + 32.0f && X::GetMouseScreenY() >= y && X::GetMouseScreenY() <= y + 32.0f)
				{
					endPos = { (float)x+16.0f,(float)y+16.0f };
					endActive = true;
				}
			}
		}
	}

	for (int x = 0; x <= rows * 32; x += 32.0f)
	{
		for (int y = 0; y <= columns * 32; y += 32.0f)
		{
			X::DrawScreenLine(0.0f, (float)y, (float)x, (float)y, X::Math::Vector4::Magenta());
			X::DrawScreenLine((float)x, 0.0f, (float)x, (float)y, X::Math::Vector4::Magenta());
		}
	}
}
bool GameLoop(float deltaTime)
{
	DrawGraph();


	if (endActive)
	{
		X::DrawSprite(end, endPos);
	}

	return X::IsKeyPressed(X::Keys::ESCAPE);
}

int CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	X::Start();
	Initialize();
	X::Run(GameLoop);
	Terminate();
	X::Stop();
	return 0;

}