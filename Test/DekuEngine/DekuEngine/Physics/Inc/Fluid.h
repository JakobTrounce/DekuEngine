#ifndef INCLUDED_DEKU_PHYSICS_FLUID_H
#define INCLUDED_DEKU_PHYSICS_FLUID_H

namespace Deku::Physics
{
	class Fluid
	{
	public:

		Fluid(Deku::Math::Vector2 pos, uint32_t id)
			: mPosition(pos)
			, mId(id)
			, startPos(pos)
		{

		}

		

		const Deku::Math::Vector2 Position() const { return mPosition;  }
		void Position(const Deku::Math::Vector2& position) { mPosition = position; }

		const Deku::Math::Vector2 Velocity() const { return mVelocity; }
		void Velocity(const Deku::Math::Vector2& velocity) { mVelocity = velocity; }

		const double Density() const { return mDensity; }
		void Density(double density) { mDensity = density; }

		const double Temperature() const { return mTemperature; }
		void Temperature(double temperature) { mTemperature = temperature; }

		const double Pressure() const { return mPressure; }
		void Pressure(double pressure) { mPressure = pressure; }
		
		const double Mass() const { return mMass; }
		void Mass(double mass) { mMass = mass; }

		const double Viscosity() const { return mViscosity; }
		void Viscosity(double viscosity) { mViscosity = viscosity; }

		const uint32_t ID() const { return mId; }

		Deku::Math::Vector2 startPos;

	private:
		Deku::Math::Vector2 mPosition{ 0 };
		Deku::Math::Vector2 mVelocity{ 0 };
		double mDensity{ 1.0 };
		double mTemperature{ 1.0 };
		double mPressure{ 1.0 };
		double mMass{ 25.0 };
		double mViscosity{ 25.0 };
		uint32_t mId{0};
	};
}


#endif